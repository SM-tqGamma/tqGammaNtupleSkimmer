#!/bin/bash
source ~/.bashrc
echo "ntuple-skimmer --in $1 --out $2 --isdata $3 --dofake $4 --sysName $5 --dosys 1"
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
cd ../source && lsetup "asetup AnalysisBase,21.2.179,here"
cd ../build && source x86*/setup.sh
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8
ntuple-skimmer --in $1 --out $2 --isdata $3 --dofake $4 --sysName $5 --dosys 1
