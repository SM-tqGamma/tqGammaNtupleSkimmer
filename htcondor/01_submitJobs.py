import os,sys,click,time
import multiprocessing as mp
def getVariations():

    variations = [
        "CategoryReduction_JET_BJES_Response__1down",
        "CategoryReduction_JET_BJES_Response__1up",
        "CategoryReduction_JET_EffectiveNP_Detector1__1down",
        "CategoryReduction_JET_EffectiveNP_Detector1__1up",
        "CategoryReduction_JET_EffectiveNP_Detector2__1down",
        "CategoryReduction_JET_EffectiveNP_Detector2__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed1__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed1__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed2__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed2__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed3__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed3__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling1__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling1__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling2__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling2__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling3__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling3__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling4__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling4__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical1__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical1__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical2__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical2__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical3__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical3__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical4__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical4__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical5__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical5__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical6__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical6__1up",
        "CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
        "CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
        "CategoryReduction_JET_EtaIntercalibration_TotalStat__1down",
        "CategoryReduction_JET_EtaIntercalibration_TotalStat__1up",
        "CategoryReduction_JET_Flavor_Composition__1down",
        "CategoryReduction_JET_Flavor_Composition__1up",
        "CategoryReduction_JET_Flavor_Response__1down",
        "CategoryReduction_JET_Flavor_Response__1up",
        "CategoryReduction_JET_JER_DataVsMC_MC16__1down",
        "CategoryReduction_JET_JER_DataVsMC_MC16__1up",
        "CategoryReduction_JET_JER_EffectiveNP_1__1down",
        "CategoryReduction_JET_JER_EffectiveNP_1__1up",
        "CategoryReduction_JET_JER_EffectiveNP_2__1down",
        "CategoryReduction_JET_JER_EffectiveNP_2__1up",
        "CategoryReduction_JET_JER_EffectiveNP_3__1down",
        "CategoryReduction_JET_JER_EffectiveNP_3__1up",
        "CategoryReduction_JET_JER_EffectiveNP_4__1down",
        "CategoryReduction_JET_JER_EffectiveNP_4__1up",
        "CategoryReduction_JET_JER_EffectiveNP_5__1down",
        "CategoryReduction_JET_JER_EffectiveNP_5__1up",
        "CategoryReduction_JET_JER_EffectiveNP_6__1down",
        "CategoryReduction_JET_JER_EffectiveNP_6__1up",
        "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",
        "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",
        "CategoryReduction_JET_Pileup_OffsetMu__1down",
        "CategoryReduction_JET_Pileup_OffsetMu__1up",
        "CategoryReduction_JET_Pileup_OffsetNPV__1down",
        "CategoryReduction_JET_Pileup_OffsetNPV__1up",
        "CategoryReduction_JET_Pileup_PtTerm__1down",
        "CategoryReduction_JET_Pileup_PtTerm__1up",
        "CategoryReduction_JET_Pileup_RhoTopology__1down",
        "CategoryReduction_JET_Pileup_RhoTopology__1up",
        "CategoryReduction_JET_PunchThrough_MC16__1down",
        "CategoryReduction_JET_PunchThrough_MC16__1up",
        "CategoryReduction_JET_SingleParticle_HighPt__1down",
        "CategoryReduction_JET_SingleParticle_HighPt__1up",
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_AF2__1down",
        "EG_SCALE_AF2__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up",
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_ScaleDown",
        "MET_SoftTrk_ScaleUp",
        "MUON_ID__1down",
        "MUON_ID__1up",
        "MUON_MS__1down",
        "MUON_MS__1up",
        "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RESBIAS__1up",
        "MUON_SAGITTA_RHO__1down",
        "MUON_SAGITTA_RHO__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up",
        "nominal",
        "CategoryReduction_JET_JER_DataVsMC_AFII__1down",
        "CategoryReduction_JET_JER_DataVsMC_AFII__1up",
        "CategoryReduction_JET_PunchThrough_AFII__1down",
        "CategoryReduction_JET_PunchThrough_AFII__1up",
        "CategoryReduction_JET_RelativeNonClosure_AFII__1down",
        "CategoryReduction_JET_RelativeNonClosure_AFII__1up",]
    return variations

def sDict():
    sampleDict = {}
    sampleDict["tqGamma"] = [412147]
    sampleDict["ttbar"] = [410470]
    sampleDict["ttGamma"] = [410389]
    #sampleDict["ZJets"] = list(range(364114,364128))+list(range(364100,364114))+list(range(364128,364142))
    sampleDict["ZeeJets"] = list(range(364114,364128))
    sampleDict["ZmmJets"] = list(range(364100,364114))
    sampleDict["ZttJets"] = list(range(364128,364142))

    #sampleDict["WJets"] = list(range(364170,364184))+list(range(364156,364170))+list(range(364184,364198))
    sampleDict["WenJets"] = list(range(364170,364184))
    sampleDict["WmnJets"] = list(range(364156,364170))
    sampleDict["WtnJets"] = list(range(364184,364198))

    sampleDict["ZJetsGamma"] = list(range(366140,366145))+list(range(366145,366150))+list(range(366150,366155))
    sampleDict["WJetsGamma"] = list(range(364522,364526))+list(range(364527,364531))+list(range(364532,364536))
    sampleDict["tchan"] = [410658,410659]
    sampleDict["tW"] = [410646,410647]
    sampleDict["schan"] = [410644,410645]
    sampleDict["diboson"] = [364250,364253,364254,364255,363355,363356,363357,363358,363359,363360,363489]
    #sampleDict["ttGammaNew"] = [500800, 504554]

    sampleDict["tqGammaShower"] = [500537]
    sampleDict["ttGammaVar"] = [410395,410404,410405]
    sampleDict["ttbarVar"] = [410480,410482]
    sampleDict["SingleTopVar"] = [411032, 411033, 411034, 411035, 411036, 411037]
    sampleDict["tqgammaLO"] = [412089]
    return sampleDict


def jobSubmitter(jobConfigs):
    import htcondor, classad
    batch_name = jobConfigs[0]["sample"]+jobConfigs[0]["af2"]+"_"+jobConfigs[0]["camp"]+"_"+jobConfigs[0]["sysname"]
    if jobConfigs[0]["sample"] == "": batch_name = jobConfigs[0]["dsid"]+jobConfigs[0]["af2"]+"_"+jobConfigs[0]["camp"]+"_"+jobConfigs[0]["sysname"]
    job = htcondor.Submit({
        "executable": "job.sh",   # the program to run on the execute node
        "arguments": "$(ifile) $(ofile) $(isdata) $(dofake) $(sysname)", # arguments passed to the executable
        "output": "condorLogs/$(dsid)_$(camp)$(af2)_$(sysname).out", # anything the job prints to standard output will end up in this file
        "error": "condorLogs/$(dsid)_$(camp)$(af2)_$(sysname).err",  # anything the job prints to standard error will end up in this file
        "log": "condorLogs/$(dsid)_$(camp)$(af2)_$(sysname).log", # this file will contain a record of what happened to the job
        "request_cpus": "1", # how many CPU cores we want
        "request_memory": "8GB",  # how much memory we want
        "batch_name": batch_name,
    })

    collector = htcondor.Collector()
    WaitForNextBatch = True
    while WaitForNextBatch:

        jn = 0
        for schedd_ad in collector.locateAll(htcondor.DaemonTypes.Schedd):
            schedd = htcondor.Schedd(schedd_ad)
            ads = schedd.query("JobStatus==1", ['ClusterId', 'OWNER'])
            jn += len(ads)
        if jn < 1000: WaitForNextBatch=False
        if WaitForNextBatch:
            print(f"Waiting for queue to empty below 1000 idle jobs! Currently {jn} jobs are idle")
            time.sleep(5)
    schedd = htcondor.Schedd()
    successful =False
    while not successful:
        try:
            with schedd.transaction() as txn:
                submit_result = job.queue_with_itemdata(txn, itemdata = iter(jobConfigs))
            successful = True
            print("Submission successful")
        except:
            print("Submission not successful")
    return

def LocaljobSubmitter(jobConfig):
    print(f"job.sh {jobConfig['ifile']} {jobConfig['ofile']} {jobConfig['isdata']} {jobConfig['dofake']} {jobConfig['sysname']}")
    os.system(f"./job.sh {jobConfig['ifile']} {jobConfig['ofile']} {jobConfig['isdata']} {jobConfig['dofake']} {jobConfig['sysname']}")

@click.command()
@click.option("--campaign","-c", multiple=True,required=True, type=click.Choice(["mc16a", "mc16d", "mc16e"]), help="Specify which campaign you want to run on.")
@click.option("--dofake", is_flag=True)
@click.option("--isdata", is_flag=True)
@click.option("--dsid", "-d", multiple=True,default=[0])
@click.option("--isaf2", is_flag=True)
@click.option("--sysname", default="nominal")
@click.option("--samples", "-s",multiple=True ,default=[""])
@click.option("--local", is_flag=True)
@click.option("--nworkers", default=1)
def main(campaign, dofake, isdata, dsid, isaf2, sysname, samples, local, nworkers):
    ntupledir = os.environ["NTUPLEDIR"]
    if "ttGammaNew" in samples:
        ntupledir = os.environ["TTGAMMADIR"]
    odir = os.environ["ODIR"]
    if not os.path.exists(odir):
        print(f"Generating output directory: {odir}")
        os.system(f"mkdir -p {odir}")
    variations = getVariations()
    if not sysname in variations and not sysname == "all":
        print(f"{sysname} not in list of variations. Exiting.")
        sys.exit(1)
    sampleDict = sDict()
    if "all" in samples: samples = sampleDict.keys()
    for sample in samples:
        if not sample == "" and not sample in sampleDict.keys():
            print(f"{sample} not specified in the sample dictionary. Exiting")
            sys.exit(1)
        elif not sample == "":
            dsid = sampleDict[sample]
        if not isdata and dsid == (0,):
            print("Specify at least one dsid! Exiting")
            sys.exit(1)
        systs = [sysname]
        if sysname == "all": systs = variations
        for syst in systs:
            print("############################")
            print(f"Processing {syst}")
            for camp in campaign:
                jobConfigs = []
                subdir = [camp]
                if isdata:
                    if "mc16a" == camp: subdir = ["data15", "data16"]
                    elif "mc16d" == camp: subdir = ["data17"]
                    elif "mc16e" == camp: subdir = ["data18"]
                for sub in subdir:
                    #print(f"Searching for input in {ntupledir}/{sub}")
                    #print("########################################")
                    for cursample in os.listdir(f"{ntupledir}/{sub}"):
                        if not os.path.isdir(f"{ntupledir}/{sub}/{cursample}"): continue
                        if not isaf2 and "a875" in cursample: continue
                        elif isaf2 and not "a875" in cursample: continue
                        for cdsid in dsid:
                            if not isdata and not str(cdsid) in cursample: continue
                            #print(f"Found input directory: {ntupledir}/{sub}/{cursample}")
                            #print("----------------------------------------")
                            for rfile in os.listdir(f"{ntupledir}/{sub}/{cursample}"):
                                jobConfig = {}
                                jobConfig["ifile"] = ""
                                jobConfig["ofile"] = ""
                                jobConfig["sysname"] = ""
                                jobConfig["isdata"] = "0"
                                jobConfig["dofake"] = "0"
                                jobConfig["dsid"] = str(cdsid)
                                jobConfig["af2"] = ""
                                jobConfig["sample"] = sample
                                jobConfig["camp"] = camp

                                if not rfile.endswith(".root"): continue
                                ifile = f"{ntupledir}/{sub}/{cursample}/{rfile}"
                                #print(f"Found inputfile: {ifile}")
                                ofile = f"{odir}/{sub}"
                                if dofake: ofile = ofile.replace("data","lfake")
                                if not isdata: ofile+="/"+str(cdsid)
                                if isaf2: ofile+="_af2"
                                if not os.path.exists(ofile):
                                    #print(f"Generating output subdirectory: {ofile}")
                                    os.system(f"mkdir -p {ofile}")
                                ofile += "/"+syst
                                if not os.path.exists(ofile):
                                    #print(f"Generating output subdirectory: {ofile}")
                                    os.system(f"mkdir -p {ofile}")
                                ofile +="/"+rfile
                                #print(f"Output will be saved at {ofile}")
                                jobConfig["ifile"] = ifile
                                jobConfig["ofile"] = ofile
                                jobConfig["sysname"] = syst
                                if isdata:
                                    jobConfig["dsid"] = sub
                                    jobConfig["isdata"] = "1"
                                if dofake: jobConfig["dofake"] = "1"
                                if isaf2: jobConfig["af2"] = "_af2"
                                jobConfigs.append(jobConfig)
                                #print("########################################")
                print(f"Submitting {camp}")
                if not local: jobSubmitter(jobConfigs)
                else: 
                    p = mp.Pool(nworkers)
                    p.map(LocaljobSubmitter,jobConfigs)
                    

if __name__ == "__main__":
    main()
