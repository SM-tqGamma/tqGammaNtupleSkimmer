source /cvmfs/sft.cern.ch/lcg/views/LCG_96bpython3/x86_64-centos7-gcc9-opt/setup.sh
export MNTDIR=      # Directory of MiniNtuples
export NNDIR=       # Directory of Ntuples with NN response
export HISTDIR=     # Directory where output histograms are stored
export PATH=$(pwd)/../include:$PATH
