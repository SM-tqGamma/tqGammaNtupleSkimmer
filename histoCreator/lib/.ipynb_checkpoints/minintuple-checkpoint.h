//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug  4 20:35:55 2021 by ROOT version 6.18/04
// from TTree nominal/Mini Ntuples
// found on file: /ceph/groups/e4/users/bwendland/tqgammaMNT_July21/mc16a/412147/nominal/group.phys-top.25406334._000001.output.root
//////////////////////////////////////////////////////////

#ifndef minintuple_h
#define minintuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
// Header file for the classes stored in the TTree if any.
#include "vector"
using namespace std;

class minintuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   bool            m_isData;
   bool            m_isNom;
   // Declaration of leaf types
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_globalLeptonTriggerSF;
   Float_t         weight_bTagSF_DL1r_60;
   Float_t         weight_bTagSF_DL1r_70;
   Float_t         weight_bTagSF_DL1r_77;
   Float_t         weight_bTagSF_DL1r_85;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_jvt;
   vector<float>   *mc_generator_weights;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e120_lhloose;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_mu26_ivarmedium;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Int_t           runYear;
   Float_t         lumi;
   Float_t         met_met;
   Float_t         met_phi;
   Float_t         transMass;
   Float_t         transMassWph;
   Float_t         transMassWb;
   Float_t         HT;
   Float_t         efakeweight;
   Float_t         efakeweightErr;
   Float_t         hfakeweight;
   Float_t         hfakeweightErr;
   Char_t          event_in_overlap;
   Char_t          truthJetMatchedB;
   Char_t          truthJetMatchedD;
   Float_t         lep1_pt;
   Float_t         lep1_eta;
   Float_t         lep1_phi;
   Float_t         lep1_e;
   Float_t         lep1_pz;
   Float_t         lep1_d0sig;
   Float_t         lep1_z0sintheta;
   Float_t         lep1_topoetcone20;
   Float_t         lep1_ptvarcone30;
   Int_t           lep1_trueType;
   Int_t           lep1_trueOrigin;
   Int_t           lep1_id;
   Int_t           lep1_mc_pid;
   Float_t         lep1_mc_charge;
   Float_t         lep1_mc_pt;
   Float_t         lep1_mc_eta;
   Float_t         lep1_mc_phi;
   Float_t         lep2_pt;
   Float_t         lep2_eta;
   Float_t         lep2_phi;
   Float_t         lep2_e;
   Float_t         lep2_pz;
   Float_t         lep2_d0sig;
   Float_t         lep2_z0sintheta;
   Float_t         lep2_topoetcone20;
   Float_t         lep2_ptvarcone30;
   Int_t           lep2_trueType;
   Int_t           lep2_trueOrigin;
   Int_t           lep2_id;
   Int_t           lep2_mc_pid;
   Float_t         lep2_mc_charge;
   Float_t         lep2_mc_pt;
   Float_t         lep2_mc_eta;
   Float_t         lep2_mc_phi;
   Float_t         nu1_pt;
   Float_t         nu1_eta;
   Float_t         nu1_phi;
   Float_t         nu1_e;
   Float_t         nu1_pz;
   UInt_t          nlep;
   UInt_t          nlep_raw;
   Float_t         ph_pt;
   Float_t         phintop_pt;
   Float_t         phintop_eta;
   Float_t         phintop_phi;
   Float_t         phintop_e;
   Float_t         ph_eta;
   Float_t         ph_phi;
   Float_t         ph_e;
   Float_t         ph_topoetcone20;
   Float_t         ph_topoetcone30;
   Float_t         ph_topoetcone40;
   Float_t         ph_ptcone20;
   Float_t         ph_ptcone30;
   Float_t         ph_ptcone40;
   UInt_t          ph_isoFCT;
   UInt_t          ph_isoFCL;
   UInt_t          ph_isTight;
   UInt_t          ph_isLoose;
   UInt_t          ph_author;
   Int_t           ph_conversionType;
   Float_t         ph_caloEta;
   UInt_t          ph_isEM_Tight;
   Int_t           ph_truthType;
   Int_t           ph_truthOrigin;
   Int_t           ph_type;
   Float_t         ph_sf_id;
   Float_t         ph_sf_iso;
   Int_t           ph_mc_pid;
   Float_t         ph_mc_pt;
   Float_t         ph_mc_eta;
   Float_t         ph_mc_phi;
   Float_t         ph_mcel_dr;
   Float_t         ph_mcel_pt;
   Float_t         ph_mcel_eta;
   Float_t         ph_mcel_phi;
   Float_t         ph_mc_production;
   UInt_t          nph;
   UInt_t          nph_raw;
   Float_t         lj_pt;
   Float_t         lj_eta;
   Float_t         lj_phi;
   Float_t         lj_e;
   Float_t         lj_DL1r;
   Float_t         lj_jvt;
   Int_t           lj_passfjvt;
   Int_t           lj_tagWeightBin_DL1r_Continuous;
   Int_t           lj_truthflav;
   Int_t           lj_isBjet;
   Float_t         slj_pt;
   Float_t         slj_eta;
   Float_t         slj_phi;
   Float_t         slj_e;
   Float_t         slj_DL1r;
   Float_t         slj_jvt;
   Int_t           slj_passfjvt;
   Int_t           slj_tagWeightBin_DL1r_Continuous;
   Int_t           slj_truthflav;
   Int_t           slj_isBjet;
   Float_t         lbj_pt;
   Float_t         lbj_eta;
   Float_t         lbj_phi;
   Float_t         lbj_e;
   Float_t         lbj_DL1r;
   Float_t         lbj_jvt;
   Int_t           lbj_passfjvt;
   Int_t           lbj_tagWeightBin_DL1r_Continuous;
   Int_t           lbj_truthflav;
   Int_t           lbj_isBjet;
   Float_t         slbj_pt;
   Float_t         slbj_eta;
   Float_t         slbj_phi;
   Float_t         slbj_e;
   Float_t         slbj_DL1r;
   Float_t         slbj_jvt;
   Int_t           slbj_passfjvt;
   Int_t           slbj_tagWeightBin_DL1r_Continuous;
   Int_t           slbj_truthflav;
   Int_t           slbj_isBjet;
   Float_t         fj_pt;
   Float_t         fj_eta;
   Float_t         fj_phi;
   Float_t         fj_e;
   Float_t         fj_DL1r;
   Float_t         fj_jvt;
   Int_t           fj_passfjvt;
   Int_t           fj_tagWeightBin_DL1r_Continuous;
   Int_t           fj_truthflav;
   Int_t           fj_isBjet;
   UInt_t          njets;
   UInt_t          nbjets;
   UInt_t          nfjets;
   UInt_t          njets_raw;
   UInt_t          nbjets_DL1r_60;
   UInt_t          nbjets_DL1r_70;
   UInt_t          nbjets_DL1r_77;
   UInt_t          nbjets_DL1r_85;
   UInt_t          fjet_flag;
   Float_t         Wbsn_pt;
   Float_t         Wbsn_eta;
   Float_t         Wbsn_phi;
   Float_t         Wbsn_e;
   Float_t         Wbsn_pz;
   Float_t         Wbsn_m;
   Float_t         Wbsn_rap;
   Float_t         Wbsn_dr;
   Float_t         Wbsn_dphi;
   Float_t         Wbsn_deta;
   Float_t         Wbsn_ctheta;
   Float_t         top_pt;
   Float_t         top_eta;
   Float_t         top_phi;
   Float_t         top_e;
   Float_t         top_pz;
   Float_t         top_m;
   Float_t         top_rap;
   Float_t         top_dr;
   Float_t         top_dphi;
   Float_t         top_deta;
   Float_t         top_ctheta;
   Float_t         topph_pt;
   Float_t         topph_eta;
   Float_t         topph_phi;
   Float_t         topph_e;
   Float_t         topph_pz;
   Float_t         topph_m;
   Float_t         topph_rap;
   Float_t         topph_dr;
   Float_t         topph_dphi;
   Float_t         topph_deta;
   Float_t         topph_ctheta;
   Float_t         topfj_pt;
   Float_t         topfj_eta;
   Float_t         topfj_phi;
   Float_t         topfj_e;
   Float_t         topfj_pz;
   Float_t         topfj_m;
   Float_t         topfj_rap;
   Float_t         topfj_dr;
   Float_t         topfj_dphi;
   Float_t         topfj_deta;
   Float_t         topfj_ctheta;
   Float_t         Wph_pt;
   Float_t         Wph_eta;
   Float_t         Wph_phi;
   Float_t         Wph_e;
   Float_t         Wph_pz;
   Float_t         Wph_m;
   Float_t         Wph_rap;
   Float_t         Wph_dr;
   Float_t         Wph_dphi;
   Float_t         Wph_deta;
   Float_t         Wph_ctheta;
   Float_t         lepph_pt;
   Float_t         lepph_eta;
   Float_t         lepph_phi;
   Float_t         lepph_e;
   Float_t         lepph_pz;
   Float_t         lepph_m;
   Float_t         lepph_rap;
   Float_t         lepph_dr;
   Float_t         lepph_dphi;
   Float_t         lepph_deta;
   Float_t         lepph_ctheta;
   Float_t         fjph_pt;
   Float_t         fjph_eta;
   Float_t         fjph_phi;
   Float_t         fjph_e;
   Float_t         fjph_pz;
   Float_t         fjph_m;
   Float_t         fjph_rap;
   Float_t         fjph_dr;
   Float_t         fjph_dphi;
   Float_t         fjph_deta;
   Float_t         fjph_ctheta;
   Float_t         bph_pt;
   Float_t         bph_eta;
   Float_t         bph_phi;
   Float_t         bph_e;
   Float_t         bph_pz;
   Float_t         bph_m;
   Float_t         bph_rap;
   Float_t         bph_dr;
   Float_t         bph_dphi;
   Float_t         bph_deta;
   Float_t         bph_ctheta;
   Float_t         nuph_pt;
   Float_t         nuph_eta;
   Float_t         nuph_phi;
   Float_t         nuph_e;
   Float_t         nuph_pz;
   Float_t         nuph_m;
   Float_t         nuph_rap;
   Float_t         nuph_dr;
   Float_t         nuph_dphi;
   Float_t         nuph_deta;
   Float_t         nuph_ctheta;
   Float_t         bnu_pt;
   Float_t         bnu_eta;
   Float_t         bnu_phi;
   Float_t         bnu_e;
   Float_t         bnu_pz;
   Float_t         bnu_m;
   Float_t         bnu_rap;
   Float_t         bnu_dr;
   Float_t         bnu_dphi;
   Float_t         bnu_deta;
   Float_t         bnu_ctheta;
   Float_t         blep_pt;
   Float_t         blep_eta;
   Float_t         blep_phi;
   Float_t         blep_e;
   Float_t         blep_pz;
   Float_t         blep_m;
   Float_t         blep_rap;
   Float_t         blep_dr;
   Float_t         blep_dphi;
   Float_t         blep_deta;
   Float_t         blep_ctheta;
   Float_t         bfj_pt;
   Float_t         bfj_eta;
   Float_t         bfj_phi;
   Float_t         bfj_e;
   Float_t         bfj_pz;
   Float_t         bfj_m;
   Float_t         bfj_rap;
   Float_t         bfj_dr;
   Float_t         bfj_dphi;
   Float_t         bfj_deta;
   Float_t         bfj_ctheta;
   Float_t         lfj_pt;
   Float_t         lfj_eta;
   Float_t         lfj_phi;
   Float_t         lfj_e;
   Float_t         lfj_pz;
   Float_t         lfj_m;
   Float_t         lfj_rap;
   Float_t         lfj_dr;
   Float_t         lfj_dphi;
   Float_t         lfj_deta;
   Float_t         lfj_ctheta;
   Float_t         nufj_pt;
   Float_t         nufj_eta;
   Float_t         nufj_phi;
   Float_t         nufj_e;
   Float_t         nufj_pz;
   Float_t         nufj_m;
   Float_t         nufj_rap;
   Float_t         nufj_dr;
   Float_t         nufj_dphi;
   Float_t         nufj_deta;
   Float_t         nufj_ctheta;
   Float_t         lep1lep2_pt;
   Float_t         lep1lep2_eta;
   Float_t         lep1lep2_phi;
   Float_t         lep1lep2_e;
   Float_t         lep1lep2_pz;
   Float_t         lep1lep2_m;
   Float_t         lep1lep2_rap;
   Float_t         lep1lep2_dr;
   Float_t         lep1lep2_dphi;
   Float_t         lep1lep2_deta;
   Float_t         lep1lep2_ctheta;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_UP;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_photonSF_ID_UP;
   Float_t         weight_photonSF_ID_DOWN;
   Float_t         weight_photonSF_effIso;
   Float_t         weight_photonSF_effIso_UP;
   Float_t         weight_photonSF_effIso_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;


   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_60;   //!
   TBranch        *b_weight_bTagSF_DL1r_70;   //!
   TBranch        *b_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_weight_bTagSF_DL1r_85;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_runYear;   //!
   TBranch        *b_lumi;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_transMass;   //!
   TBranch        *b_transMassWph;   //!
   TBranch        *b_transMassWb;   //!
   TBranch        *b_HT;   //!
   TBranch        *b_efakeweight;   //!
   TBranch        *b_efakeweightErr;   //!
   TBranch        *b_hfakeweight;   //!
   TBranch        *b_hfakeweightErr;   //!
   TBranch        *b_event_in_overlap;   //!
   TBranch        *b_truthJetMatchedB;   //!
   TBranch        *b_truthJetMatchedD;   //!
   TBranch        *b_lep1_pt;   //!
   TBranch        *b_lep1_eta;   //!
   TBranch        *b_lep1_phi;   //!
   TBranch        *b_lep1_e;   //!
   TBranch        *b_lep1_pz;   //!
   TBranch        *b_lep1_d0sig;   //!
   TBranch        *b_lep1_z0sintheta;   //!
   TBranch        *b_lep1_topoetcone20;   //!
   TBranch        *b_lep1_ptvarcone30;   //!
   TBranch        *b_lep1_trueType;   //!
   TBranch        *b_lep1_trueOrigin;   //!
   TBranch        *b_lep1_id;   //!
   TBranch        *b_lep1_mc_pid;   //!
   TBranch        *b_lep1_mc_charge;   //!
   TBranch        *b_lep1_mc_pt;   //!
   TBranch        *b_lep1_mc_eta;   //!
   TBranch        *b_lep1_mc_phi;   //!
   TBranch        *b_lep2_pt;   //!
   TBranch        *b_lep2_eta;   //!
   TBranch        *b_lep2_phi;   //!
   TBranch        *b_lep2_e;   //!
   TBranch        *b_lep2_pz;   //!
   TBranch        *b_lep2_d0sig;   //!
   TBranch        *b_lep2_z0sintheta;   //!
   TBranch        *b_lep2_topoetcone20;   //!
   TBranch        *b_lep2_ptvarcone30;   //!
   TBranch        *b_lep2_trueType;   //!
   TBranch        *b_lep2_trueOrigin;   //!
   TBranch        *b_lep2_id;   //!
   TBranch        *b_lep2_mc_pid;   //!
   TBranch        *b_lep2_mc_charge;   //!
   TBranch        *b_lep2_mc_pt;   //!
   TBranch        *b_lep2_mc_eta;   //!
   TBranch        *b_lep2_mc_phi;   //!
   TBranch        *b_nu1_pt;   //!
   TBranch        *b_nu1_eta;   //!
   TBranch        *b_nu1_phi;   //!
   TBranch        *b_nu1_e;   //!
   TBranch        *b_nu1_pz;   //!
   TBranch        *b_nlep;   //!
   TBranch        *b_nlep_raw;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_phintop_pt;   //!
   TBranch        *b_phintop_eta;   //!
   TBranch        *b_phintop_phi;   //!
   TBranch        *b_phintop_e;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_isoFCT;   //!
   TBranch        *b_ph_isoFCL;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isLoose;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_conversionType;   //!
   TBranch        *b_ph_caloEta;   //!
   TBranch        *b_ph_isEM_Tight;   //!
   TBranch        *b_ph_truthType;   //!
   TBranch        *b_ph_truthOrigin;   //!
   TBranch        *b_ph_type;   //!
   TBranch        *b_ph_sf_id;   //!
   TBranch        *b_ph_sf_iso;   //!
   TBranch        *b_ph_mc_pid;   //!
   TBranch        *b_ph_mc_pt;   //!
   TBranch        *b_ph_mc_eta;   //!
   TBranch        *b_ph_mc_phi;   //!
   TBranch        *b_ph_mcel_dr;   //!
   TBranch        *b_ph_mcel_pt;   //!
   TBranch        *b_ph_mcel_eta;   //!
   TBranch        *b_ph_mcel_phi;   //!
   TBranch        *b_ph_mc_production;   //!
   TBranch        *b_nph;   //!
   TBranch        *b_nph_raw;   //!
   TBranch        *b_lj_pt;   //!
   TBranch        *b_lj_eta;   //!
   TBranch        *b_lj_phi;   //!
   TBranch        *b_lj_e;   //!
   TBranch        *b_lj_DL1r;   //!
   TBranch        *b_lj_jvt;   //!
   TBranch        *b_lj_passfjvt;   //!
   TBranch        *b_lj_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_lj_truthflav;   //!
   TBranch        *b_lj_isBjet;   //!
   TBranch        *b_slj_pt;   //!
   TBranch        *b_slj_eta;   //!
   TBranch        *b_slj_phi;   //!
   TBranch        *b_slj_e;   //!
   TBranch        *b_slj_DL1r;   //!
   TBranch        *b_slj_jvt;   //!
   TBranch        *b_slj_passfjvt;   //!
   TBranch        *b_slj_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_slj_truthflav;   //!
   TBranch        *b_slj_isBjet;   //!
   TBranch        *b_lbj_pt;   //!
   TBranch        *b_lbj_eta;   //!
   TBranch        *b_lbj_phi;   //!
   TBranch        *b_lbj_e;   //!
   TBranch        *b_lbj_DL1r;   //!
   TBranch        *b_lbj_jvt;   //!
   TBranch        *b_lbj_passfjvt;   //!
   TBranch        *b_lbj_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_lbj_truthflav;   //!
   TBranch        *b_lbj_isBjet;   //!
   TBranch        *b_slbj_pt;   //!
   TBranch        *b_slbj_eta;   //!
   TBranch        *b_slbj_phi;   //!
   TBranch        *b_slbj_e;   //!
   TBranch        *b_slbj_DL1r;   //!
   TBranch        *b_slbj_jvt;   //!
   TBranch        *b_slbj_passfjvt;   //!
   TBranch        *b_slbj_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_slbj_truthflav;   //!
   TBranch        *b_slbj_isBjet;   //!
   TBranch        *b_fj_pt;   //!
   TBranch        *b_fj_eta;   //!
   TBranch        *b_fj_phi;   //!
   TBranch        *b_fj_e;   //!
   TBranch        *b_fj_DL1r;   //!
   TBranch        *b_fj_jvt;   //!
   TBranch        *b_fj_passfjvt;   //!
   TBranch        *b_fj_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_fj_truthflav;   //!
   TBranch        *b_fj_isBjet;   //!
   TBranch        *b_njets;   //!
   TBranch        *b_nbjets;   //!
   TBranch        *b_nfjets;   //!
   TBranch        *b_njets_raw;   //!
   TBranch        *b_nbjets_DL1r_60;   //!
   TBranch        *b_nbjets_DL1r_70;   //!
   TBranch        *b_nbjets_DL1r_77;   //!
   TBranch        *b_nbjets_DL1r_85;   //!
   TBranch        *b_fjet_flag;   //!
   TBranch        *b_Wbsn_pt;   //!
   TBranch        *b_Wbsn_eta;   //!
   TBranch        *b_Wbsn_phi;   //!
   TBranch        *b_Wbsn_e;   //!
   TBranch        *b_Wbsn_pz;   //!
   TBranch        *b_Wbsn_m;   //!
   TBranch        *b_Wbsn_rap;   //!
   TBranch        *b_Wbsn_dr;   //!
   TBranch        *b_Wbsn_dphi;   //!
   TBranch        *b_Wbsn_deta;   //!
   TBranch        *b_Wbsn_ctheta;   //!
   TBranch        *b_top_pt;   //!
   TBranch        *b_top_eta;   //!
   TBranch        *b_top_phi;   //!
   TBranch        *b_top_e;   //!
   TBranch        *b_top_pz;   //!
   TBranch        *b_top_m;   //!
   TBranch        *b_top_rap;   //!
   TBranch        *b_top_dr;   //!
   TBranch        *b_top_dphi;   //!
   TBranch        *b_top_deta;   //!
   TBranch        *b_top_ctheta;   //!
   TBranch        *b_topph_pt;   //!
   TBranch        *b_topph_eta;   //!
   TBranch        *b_topph_phi;   //!
   TBranch        *b_topph_e;   //!
   TBranch        *b_topph_pz;   //!
   TBranch        *b_topph_m;   //!
   TBranch        *b_topph_rap;   //!
   TBranch        *b_topph_dr;   //!
   TBranch        *b_topph_dphi;   //!
   TBranch        *b_topph_deta;   //!
   TBranch        *b_topph_ctheta;   //!
   TBranch        *b_topfj_pt;   //!
   TBranch        *b_topfj_eta;   //!
   TBranch        *b_topfj_phi;   //!
   TBranch        *b_topfj_e;   //!
   TBranch        *b_topfj_pz;   //!
   TBranch        *b_topfj_m;   //!
   TBranch        *b_topfj_rap;   //!
   TBranch        *b_topfj_dr;   //!
   TBranch        *b_topfj_dphi;   //!
   TBranch        *b_topfj_deta;   //!
   TBranch        *b_topfj_ctheta;   //!
   TBranch        *b_Wph_pt;   //!
   TBranch        *b_Wph_eta;   //!
   TBranch        *b_Wph_phi;   //!
   TBranch        *b_Wph_e;   //!
   TBranch        *b_Wph_pz;   //!
   TBranch        *b_Wph_m;   //!
   TBranch        *b_Wph_rap;   //!
   TBranch        *b_Wph_dr;   //!
   TBranch        *b_Wph_dphi;   //!
   TBranch        *b_Wph_deta;   //!
   TBranch        *b_Wph_ctheta;   //!
   TBranch        *b_lepph_pt;   //!
   TBranch        *b_lepph_eta;   //!
   TBranch        *b_lepph_phi;   //!
   TBranch        *b_lepph_e;   //!
   TBranch        *b_lepph_pz;   //!
   TBranch        *b_lepph_m;   //!
   TBranch        *b_lepph_rap;   //!
   TBranch        *b_lepph_dr;   //!
   TBranch        *b_lepph_dphi;   //!
   TBranch        *b_lepph_deta;   //!
   TBranch        *b_lepph_ctheta;   //!
   TBranch        *b_fjph_pt;   //!
   TBranch        *b_fjph_eta;   //!
   TBranch        *b_fjph_phi;   //!
   TBranch        *b_fjph_e;   //!
   TBranch        *b_fjph_pz;   //!
   TBranch        *b_fjph_m;   //!
   TBranch        *b_fjph_rap;   //!
   TBranch        *b_fjph_dr;   //!
   TBranch        *b_fjph_dphi;   //!
   TBranch        *b_fjph_deta;   //!
   TBranch        *b_fjph_ctheta;   //!
   TBranch        *b_bph_pt;   //!
   TBranch        *b_bph_eta;   //!
   TBranch        *b_bph_phi;   //!
   TBranch        *b_bph_e;   //!
   TBranch        *b_bph_pz;   //!
   TBranch        *b_bph_m;   //!
   TBranch        *b_bph_rap;   //!
   TBranch        *b_bph_dr;   //!
   TBranch        *b_bph_dphi;   //!
   TBranch        *b_bph_deta;   //!
   TBranch        *b_bph_ctheta;   //!
   TBranch        *b_nuph_pt;   //!
   TBranch        *b_nuph_eta;   //!
   TBranch        *b_nuph_phi;   //!
   TBranch        *b_nuph_e;   //!
   TBranch        *b_nuph_pz;   //!
   TBranch        *b_nuph_m;   //!
   TBranch        *b_nuph_rap;   //!
   TBranch        *b_nuph_dr;   //!
   TBranch        *b_nuph_dphi;   //!
   TBranch        *b_nuph_deta;   //!
   TBranch        *b_nuph_ctheta;   //!
   TBranch        *b_bnu_pt;   //!
   TBranch        *b_bnu_eta;   //!
   TBranch        *b_bnu_phi;   //!
   TBranch        *b_bnu_e;   //!
   TBranch        *b_bnu_pz;   //!
   TBranch        *b_bnu_m;   //!
   TBranch        *b_bnu_rap;   //!
   TBranch        *b_bnu_dr;   //!
   TBranch        *b_bnu_dphi;   //!
   TBranch        *b_bnu_deta;   //!
   TBranch        *b_bnu_ctheta;   //!
   TBranch        *b_blep_pt;   //!
   TBranch        *b_blep_eta;   //!
   TBranch        *b_blep_phi;   //!
   TBranch        *b_blep_e;   //!
   TBranch        *b_blep_pz;   //!
   TBranch        *b_blep_m;   //!
   TBranch        *b_blep_rap;   //!
   TBranch        *b_blep_dr;   //!
   TBranch        *b_blep_dphi;   //!
   TBranch        *b_blep_deta;   //!
   TBranch        *b_blep_ctheta;   //!
   TBranch        *b_bfj_pt;   //!
   TBranch        *b_bfj_eta;   //!
   TBranch        *b_bfj_phi;   //!
   TBranch        *b_bfj_e;   //!
   TBranch        *b_bfj_pz;   //!
   TBranch        *b_bfj_m;   //!
   TBranch        *b_bfj_rap;   //!
   TBranch        *b_bfj_dr;   //!
   TBranch        *b_bfj_dphi;   //!
   TBranch        *b_bfj_deta;   //!
   TBranch        *b_bfj_ctheta;   //!
   TBranch        *b_lfj_pt;   //!
   TBranch        *b_lfj_eta;   //!
   TBranch        *b_lfj_phi;   //!
   TBranch        *b_lfj_e;   //!
   TBranch        *b_lfj_pz;   //!
   TBranch        *b_lfj_m;   //!
   TBranch        *b_lfj_rap;   //!
   TBranch        *b_lfj_dr;   //!
   TBranch        *b_lfj_dphi;   //!
   TBranch        *b_lfj_deta;   //!
   TBranch        *b_lfj_ctheta;   //!
   TBranch        *b_nufj_pt;   //!
   TBranch        *b_nufj_eta;   //!
   TBranch        *b_nufj_phi;   //!
   TBranch        *b_nufj_e;   //!
   TBranch        *b_nufj_pz;   //!
   TBranch        *b_nufj_m;   //!
   TBranch        *b_nufj_rap;   //!
   TBranch        *b_nufj_dr;   //!
   TBranch        *b_nufj_dphi;   //!
   TBranch        *b_nufj_deta;   //!
   TBranch        *b_nufj_ctheta;   //!
   TBranch        *b_lep1lep2_pt;   //!
   TBranch        *b_lep1lep2_eta;   //!
   TBranch        *b_lep1lep2_phi;   //!
   TBranch        *b_lep1lep2_e;   //!
   TBranch        *b_lep1lep2_pz;   //!
   TBranch        *b_lep1lep2_m;   //!
   TBranch        *b_lep1lep2_rap;   //!
   TBranch        *b_lep1lep2_dr;   //!
   TBranch        *b_lep1lep2_dphi;   //!
   TBranch        *b_lep1lep2_deta;   //!
   TBranch        *b_lep1lep2_ctheta;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_photonSF_ID_UP;   //!
   TBranch        *b_weight_photonSF_ID_DOWN;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_weight_photonSF_effIso_UP;   //!
   TBranch        *b_weight_photonSF_effIso_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;   //!

   minintuple(TTree *tree=0, bool isData=false, bool isNom=false);
   virtual ~minintuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef minintuple_cxx
minintuple::minintuple(TTree *tree, bool isData, bool isNom) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/ceph/groups/e4/users/bwendland/tqgammaMNT_July21/mc16a/412147/nominal/group.phys-top.25406334._000001.output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/ceph/groups/e4/users/bwendland/tqgammaMNT_July21/mc16a/412147/nominal/group.phys-top.25406334._000001.output.root");
      }
      f->GetObject("nominal",tree);

   }
   m_isData=isData;
   Init(tree);
}

minintuple::~minintuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t minintuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t minintuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void minintuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_down = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   if(!m_isData){
     fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
     fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
     fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
     fChain->SetBranchAddress("weight_photonSF", &weight_photonSF, &b_weight_photonSF);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_60", &weight_bTagSF_DL1r_60, &b_weight_bTagSF_DL1r_60);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70", &weight_bTagSF_DL1r_70, &b_weight_bTagSF_DL1r_70);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_85", &weight_bTagSF_DL1r_85, &b_weight_bTagSF_DL1r_85);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
     fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
     if(m_isNom) fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   }
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("runYear", &runYear, &b_runYear);
   fChain->SetBranchAddress("lumi", &lumi, &b_lumi);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("transMass", &transMass, &b_transMass);
   fChain->SetBranchAddress("transMassWph", &transMassWph, &b_transMassWph);
   fChain->SetBranchAddress("transMassWb", &transMassWb, &b_transMassWb);
   fChain->SetBranchAddress("HT", &HT, &b_HT);
   fChain->SetBranchAddress("efakeweight", &efakeweight, &b_efakeweight);
   fChain->SetBranchAddress("efakeweightErr", &efakeweightErr, &b_efakeweightErr);
   fChain->SetBranchAddress("hfakeweight", &hfakeweight, &b_hfakeweight);
   fChain->SetBranchAddress("hfakeweightErr", &hfakeweightErr, &b_hfakeweightErr);
   fChain->SetBranchAddress("event_in_overlap", &event_in_overlap, &b_event_in_overlap);
   fChain->SetBranchAddress("truthJetMatchedB", &truthJetMatchedB, &b_truthJetMatchedB);
   fChain->SetBranchAddress("truthJetMatchedD", &truthJetMatchedD, &b_truthJetMatchedD);
   fChain->SetBranchAddress("lep1_pt", &lep1_pt, &b_lep1_pt);
   fChain->SetBranchAddress("lep1_eta", &lep1_eta, &b_lep1_eta);
   fChain->SetBranchAddress("lep1_phi", &lep1_phi, &b_lep1_phi);
   fChain->SetBranchAddress("lep1_e", &lep1_e, &b_lep1_e);
   fChain->SetBranchAddress("lep1_pz", &lep1_pz, &b_lep1_pz);
   fChain->SetBranchAddress("lep1_d0sig", &lep1_d0sig, &b_lep1_d0sig);
   fChain->SetBranchAddress("lep1_z0sintheta", &lep1_z0sintheta, &b_lep1_z0sintheta);
   fChain->SetBranchAddress("lep1_topoetcone20", &lep1_topoetcone20, &b_lep1_topoetcone20);
   fChain->SetBranchAddress("lep1_ptvarcone30", &lep1_ptvarcone30, &b_lep1_ptvarcone30);
   fChain->SetBranchAddress("lep1_trueType", &lep1_trueType, &b_lep1_trueType);
   fChain->SetBranchAddress("lep1_trueOrigin", &lep1_trueOrigin, &b_lep1_trueOrigin);
   fChain->SetBranchAddress("lep1_id", &lep1_id, &b_lep1_id);
   fChain->SetBranchAddress("lep1_mc_pid", &lep1_mc_pid, &b_lep1_mc_pid);
   fChain->SetBranchAddress("lep1_mc_charge", &lep1_mc_charge, &b_lep1_mc_charge);
   fChain->SetBranchAddress("lep1_mc_pt", &lep1_mc_pt, &b_lep1_mc_pt);
   fChain->SetBranchAddress("lep1_mc_eta", &lep1_mc_eta, &b_lep1_mc_eta);
   fChain->SetBranchAddress("lep1_mc_phi", &lep1_mc_phi, &b_lep1_mc_phi);
   fChain->SetBranchAddress("lep2_pt", &lep2_pt, &b_lep2_pt);
   fChain->SetBranchAddress("lep2_eta", &lep2_eta, &b_lep2_eta);
   fChain->SetBranchAddress("lep2_phi", &lep2_phi, &b_lep2_phi);
   fChain->SetBranchAddress("lep2_e", &lep2_e, &b_lep2_e);
   fChain->SetBranchAddress("lep2_pz", &lep2_pz, &b_lep2_pz);
   fChain->SetBranchAddress("lep2_d0sig", &lep2_d0sig, &b_lep2_d0sig);
   fChain->SetBranchAddress("lep2_z0sintheta", &lep2_z0sintheta, &b_lep2_z0sintheta);
   fChain->SetBranchAddress("lep2_topoetcone20", &lep2_topoetcone20, &b_lep2_topoetcone20);
   fChain->SetBranchAddress("lep2_ptvarcone30", &lep2_ptvarcone30, &b_lep2_ptvarcone30);
   fChain->SetBranchAddress("lep2_trueType", &lep2_trueType, &b_lep2_trueType);
   fChain->SetBranchAddress("lep2_trueOrigin", &lep2_trueOrigin, &b_lep2_trueOrigin);
   fChain->SetBranchAddress("lep2_id", &lep2_id, &b_lep2_id);
   fChain->SetBranchAddress("lep2_mc_pid", &lep2_mc_pid, &b_lep2_mc_pid);
   fChain->SetBranchAddress("lep2_mc_charge", &lep2_mc_charge, &b_lep2_mc_charge);
   fChain->SetBranchAddress("lep2_mc_pt", &lep2_mc_pt, &b_lep2_mc_pt);
   fChain->SetBranchAddress("lep2_mc_eta", &lep2_mc_eta, &b_lep2_mc_eta);
   fChain->SetBranchAddress("lep2_mc_phi", &lep2_mc_phi, &b_lep2_mc_phi);
   fChain->SetBranchAddress("nu1_pt", &nu1_pt, &b_nu1_pt);
   fChain->SetBranchAddress("nu1_eta", &nu1_eta, &b_nu1_eta);
   fChain->SetBranchAddress("nu1_phi", &nu1_phi, &b_nu1_phi);
   fChain->SetBranchAddress("nu1_e", &nu1_e, &b_nu1_e);
   fChain->SetBranchAddress("nu1_pz", &nu1_pz, &b_nu1_pz);
   fChain->SetBranchAddress("nlep", &nlep, &b_nlep);
   fChain->SetBranchAddress("nlep_raw", &nlep_raw, &b_nlep_raw);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("phintop_pt", &phintop_pt, &b_phintop_pt);
   fChain->SetBranchAddress("phintop_eta", &phintop_eta, &b_phintop_eta);
   fChain->SetBranchAddress("phintop_phi", &phintop_phi, &b_phintop_phi);
   fChain->SetBranchAddress("phintop_e", &phintop_e, &b_phintop_e);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
   fChain->SetBranchAddress("ph_isoFCT", &ph_isoFCT, &b_ph_isoFCT);
   fChain->SetBranchAddress("ph_isoFCL", &ph_isoFCL, &b_ph_isoFCL);
   fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
   fChain->SetBranchAddress("ph_isLoose", &ph_isLoose, &b_ph_isLoose);
   fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
   fChain->SetBranchAddress("ph_conversionType", &ph_conversionType, &b_ph_conversionType);
   fChain->SetBranchAddress("ph_caloEta", &ph_caloEta, &b_ph_caloEta);
   fChain->SetBranchAddress("ph_isEM_Tight", &ph_isEM_Tight, &b_ph_isEM_Tight);
   fChain->SetBranchAddress("ph_truthType", &ph_truthType, &b_ph_truthType);
   fChain->SetBranchAddress("ph_truthOrigin", &ph_truthOrigin, &b_ph_truthOrigin);
   fChain->SetBranchAddress("ph_type", &ph_type, &b_ph_type);
   fChain->SetBranchAddress("ph_sf_id", &ph_sf_id, &b_ph_sf_id);
   fChain->SetBranchAddress("ph_sf_iso", &ph_sf_iso, &b_ph_sf_iso);
   fChain->SetBranchAddress("ph_mc_pid", &ph_mc_pid, &b_ph_mc_pid);
   fChain->SetBranchAddress("ph_mc_pt", &ph_mc_pt, &b_ph_mc_pt);
   fChain->SetBranchAddress("ph_mc_eta", &ph_mc_eta, &b_ph_mc_eta);
   fChain->SetBranchAddress("ph_mc_phi", &ph_mc_phi, &b_ph_mc_phi);
   fChain->SetBranchAddress("ph_mcel_dr", &ph_mcel_dr, &b_ph_mcel_dr);
   fChain->SetBranchAddress("ph_mcel_pt", &ph_mcel_pt, &b_ph_mcel_pt);
   fChain->SetBranchAddress("ph_mcel_eta", &ph_mcel_eta, &b_ph_mcel_eta);
   fChain->SetBranchAddress("ph_mcel_phi", &ph_mcel_phi, &b_ph_mcel_phi);
   fChain->SetBranchAddress("ph_mc_production", &ph_mc_production, &b_ph_mc_production);
   fChain->SetBranchAddress("nph", &nph, &b_nph);
   fChain->SetBranchAddress("nph_raw", &nph_raw, &b_nph_raw);
   fChain->SetBranchAddress("lj_pt", &lj_pt, &b_lj_pt);
   fChain->SetBranchAddress("lj_eta", &lj_eta, &b_lj_eta);
   fChain->SetBranchAddress("lj_phi", &lj_phi, &b_lj_phi);
   fChain->SetBranchAddress("lj_e", &lj_e, &b_lj_e);
   fChain->SetBranchAddress("lj_DL1r", &lj_DL1r, &b_lj_DL1r);
   fChain->SetBranchAddress("lj_jvt", &lj_jvt, &b_lj_jvt);
   fChain->SetBranchAddress("lj_passfjvt", &lj_passfjvt, &b_lj_passfjvt);
   fChain->SetBranchAddress("lj_tagWeightBin_DL1r_Continuous", &lj_tagWeightBin_DL1r_Continuous, &b_lj_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("lj_truthflav", &lj_truthflav, &b_lj_truthflav);
   fChain->SetBranchAddress("lj_isBjet", &lj_isBjet, &b_lj_isBjet);
   fChain->SetBranchAddress("slj_pt", &slj_pt, &b_slj_pt);
   fChain->SetBranchAddress("slj_eta", &slj_eta, &b_slj_eta);
   fChain->SetBranchAddress("slj_phi", &slj_phi, &b_slj_phi);
   fChain->SetBranchAddress("slj_e", &slj_e, &b_slj_e);
   fChain->SetBranchAddress("slj_DL1r", &slj_DL1r, &b_slj_DL1r);
   fChain->SetBranchAddress("slj_jvt", &slj_jvt, &b_slj_jvt);
   fChain->SetBranchAddress("slj_passfjvt", &slj_passfjvt, &b_slj_passfjvt);
   fChain->SetBranchAddress("slj_tagWeightBin_DL1r_Continuous", &slj_tagWeightBin_DL1r_Continuous, &b_slj_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("slj_truthflav", &slj_truthflav, &b_slj_truthflav);
   fChain->SetBranchAddress("slj_isBjet", &slj_isBjet, &b_slj_isBjet);
   fChain->SetBranchAddress("lbj_pt", &lbj_pt, &b_lbj_pt);
   fChain->SetBranchAddress("lbj_eta", &lbj_eta, &b_lbj_eta);
   fChain->SetBranchAddress("lbj_phi", &lbj_phi, &b_lbj_phi);
   fChain->SetBranchAddress("lbj_e", &lbj_e, &b_lbj_e);
   fChain->SetBranchAddress("lbj_DL1r", &lbj_DL1r, &b_lbj_DL1r);
   fChain->SetBranchAddress("lbj_jvt", &lbj_jvt, &b_lbj_jvt);
   fChain->SetBranchAddress("lbj_passfjvt", &lbj_passfjvt, &b_lbj_passfjvt);
   fChain->SetBranchAddress("lbj_tagWeightBin_DL1r_Continuous", &lbj_tagWeightBin_DL1r_Continuous, &b_lbj_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("lbj_truthflav", &lbj_truthflav, &b_lbj_truthflav);
   fChain->SetBranchAddress("lbj_isBjet", &lbj_isBjet, &b_lbj_isBjet);
   fChain->SetBranchAddress("slbj_pt", &slbj_pt, &b_slbj_pt);
   fChain->SetBranchAddress("slbj_eta", &slbj_eta, &b_slbj_eta);
   fChain->SetBranchAddress("slbj_phi", &slbj_phi, &b_slbj_phi);
   fChain->SetBranchAddress("slbj_e", &slbj_e, &b_slbj_e);
   fChain->SetBranchAddress("slbj_DL1r", &slbj_DL1r, &b_slbj_DL1r);
   fChain->SetBranchAddress("slbj_jvt", &slbj_jvt, &b_slbj_jvt);
   fChain->SetBranchAddress("slbj_passfjvt", &slbj_passfjvt, &b_slbj_passfjvt);
   fChain->SetBranchAddress("slbj_tagWeightBin_DL1r_Continuous", &slbj_tagWeightBin_DL1r_Continuous, &b_slbj_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("slbj_truthflav", &slbj_truthflav, &b_slbj_truthflav);
   fChain->SetBranchAddress("slbj_isBjet", &slbj_isBjet, &b_slbj_isBjet);
   fChain->SetBranchAddress("fj_pt", &fj_pt, &b_fj_pt);
   fChain->SetBranchAddress("fj_eta", &fj_eta, &b_fj_eta);
   fChain->SetBranchAddress("fj_phi", &fj_phi, &b_fj_phi);
   fChain->SetBranchAddress("fj_e", &fj_e, &b_fj_e);
   fChain->SetBranchAddress("fj_DL1r", &fj_DL1r, &b_fj_DL1r);
   fChain->SetBranchAddress("fj_jvt", &fj_jvt, &b_fj_jvt);
   fChain->SetBranchAddress("fj_passfjvt", &fj_passfjvt, &b_fj_passfjvt);
   fChain->SetBranchAddress("fj_tagWeightBin_DL1r_Continuous", &fj_tagWeightBin_DL1r_Continuous, &b_fj_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("fj_truthflav", &fj_truthflav, &b_fj_truthflav);
   fChain->SetBranchAddress("fj_isBjet", &fj_isBjet, &b_fj_isBjet);
   fChain->SetBranchAddress("njets", &njets, &b_njets);
   fChain->SetBranchAddress("nbjets", &nbjets, &b_nbjets);
   fChain->SetBranchAddress("nfjets", &nfjets, &b_nfjets);
   fChain->SetBranchAddress("njets_raw", &njets_raw, &b_njets_raw);
   fChain->SetBranchAddress("nbjets_DL1r_60", &nbjets_DL1r_60, &b_nbjets_DL1r_60);
   fChain->SetBranchAddress("nbjets_DL1r_70", &nbjets_DL1r_70, &b_nbjets_DL1r_70);
   fChain->SetBranchAddress("nbjets_DL1r_77", &nbjets_DL1r_77, &b_nbjets_DL1r_77);
   fChain->SetBranchAddress("nbjets_DL1r_85", &nbjets_DL1r_85, &b_nbjets_DL1r_85);
   fChain->SetBranchAddress("fjet_flag", &fjet_flag, &b_fjet_flag);
   fChain->SetBranchAddress("Wbsn_pt", &Wbsn_pt, &b_Wbsn_pt);
   fChain->SetBranchAddress("Wbsn_eta", &Wbsn_eta, &b_Wbsn_eta);
   fChain->SetBranchAddress("Wbsn_phi", &Wbsn_phi, &b_Wbsn_phi);
   fChain->SetBranchAddress("Wbsn_e", &Wbsn_e, &b_Wbsn_e);
   fChain->SetBranchAddress("Wbsn_pz", &Wbsn_pz, &b_Wbsn_pz);
   fChain->SetBranchAddress("Wbsn_m", &Wbsn_m, &b_Wbsn_m);
   fChain->SetBranchAddress("Wbsn_rap", &Wbsn_rap, &b_Wbsn_rap);
   fChain->SetBranchAddress("Wbsn_dr", &Wbsn_dr, &b_Wbsn_dr);
   fChain->SetBranchAddress("Wbsn_dphi", &Wbsn_dphi, &b_Wbsn_dphi);
   fChain->SetBranchAddress("Wbsn_deta", &Wbsn_deta, &b_Wbsn_deta);
   fChain->SetBranchAddress("Wbsn_ctheta", &Wbsn_ctheta, &b_Wbsn_ctheta);
   fChain->SetBranchAddress("top_pt", &top_pt, &b_top_pt);
   fChain->SetBranchAddress("top_eta", &top_eta, &b_top_eta);
   fChain->SetBranchAddress("top_phi", &top_phi, &b_top_phi);
   fChain->SetBranchAddress("top_e", &top_e, &b_top_e);
   fChain->SetBranchAddress("top_pz", &top_pz, &b_top_pz);
   fChain->SetBranchAddress("top_m", &top_m, &b_top_m);
   fChain->SetBranchAddress("top_rap", &top_rap, &b_top_rap);
   fChain->SetBranchAddress("top_dr", &top_dr, &b_top_dr);
   fChain->SetBranchAddress("top_dphi", &top_dphi, &b_top_dphi);
   fChain->SetBranchAddress("top_deta", &top_deta, &b_top_deta);
   fChain->SetBranchAddress("top_ctheta", &top_ctheta, &b_top_ctheta);
   fChain->SetBranchAddress("topph_pt", &topph_pt, &b_topph_pt);
   fChain->SetBranchAddress("topph_eta", &topph_eta, &b_topph_eta);
   fChain->SetBranchAddress("topph_phi", &topph_phi, &b_topph_phi);
   fChain->SetBranchAddress("topph_e", &topph_e, &b_topph_e);
   fChain->SetBranchAddress("topph_pz", &topph_pz, &b_topph_pz);
   fChain->SetBranchAddress("topph_m", &topph_m, &b_topph_m);
   fChain->SetBranchAddress("topph_rap", &topph_rap, &b_topph_rap);
   fChain->SetBranchAddress("topph_dr", &topph_dr, &b_topph_dr);
   fChain->SetBranchAddress("topph_dphi", &topph_dphi, &b_topph_dphi);
   fChain->SetBranchAddress("topph_deta", &topph_deta, &b_topph_deta);
   fChain->SetBranchAddress("topph_ctheta", &topph_ctheta, &b_topph_ctheta);
   fChain->SetBranchAddress("topfj_pt", &topfj_pt, &b_topfj_pt);
   fChain->SetBranchAddress("topfj_eta", &topfj_eta, &b_topfj_eta);
   fChain->SetBranchAddress("topfj_phi", &topfj_phi, &b_topfj_phi);
   fChain->SetBranchAddress("topfj_e", &topfj_e, &b_topfj_e);
   fChain->SetBranchAddress("topfj_pz", &topfj_pz, &b_topfj_pz);
   fChain->SetBranchAddress("topfj_m", &topfj_m, &b_topfj_m);
   fChain->SetBranchAddress("topfj_rap", &topfj_rap, &b_topfj_rap);
   fChain->SetBranchAddress("topfj_dr", &topfj_dr, &b_topfj_dr);
   fChain->SetBranchAddress("topfj_dphi", &topfj_dphi, &b_topfj_dphi);
   fChain->SetBranchAddress("topfj_deta", &topfj_deta, &b_topfj_deta);
   fChain->SetBranchAddress("topfj_ctheta", &topfj_ctheta, &b_topfj_ctheta);
   fChain->SetBranchAddress("Wph_pt", &Wph_pt, &b_Wph_pt);
   fChain->SetBranchAddress("Wph_eta", &Wph_eta, &b_Wph_eta);
   fChain->SetBranchAddress("Wph_phi", &Wph_phi, &b_Wph_phi);
   fChain->SetBranchAddress("Wph_e", &Wph_e, &b_Wph_e);
   fChain->SetBranchAddress("Wph_pz", &Wph_pz, &b_Wph_pz);
   fChain->SetBranchAddress("Wph_m", &Wph_m, &b_Wph_m);
   fChain->SetBranchAddress("Wph_rap", &Wph_rap, &b_Wph_rap);
   fChain->SetBranchAddress("Wph_dr", &Wph_dr, &b_Wph_dr);
   fChain->SetBranchAddress("Wph_dphi", &Wph_dphi, &b_Wph_dphi);
   fChain->SetBranchAddress("Wph_deta", &Wph_deta, &b_Wph_deta);
   fChain->SetBranchAddress("Wph_ctheta", &Wph_ctheta, &b_Wph_ctheta);
   fChain->SetBranchAddress("lepph_pt", &lepph_pt, &b_lepph_pt);
   fChain->SetBranchAddress("lepph_eta", &lepph_eta, &b_lepph_eta);
   fChain->SetBranchAddress("lepph_phi", &lepph_phi, &b_lepph_phi);
   fChain->SetBranchAddress("lepph_e", &lepph_e, &b_lepph_e);
   fChain->SetBranchAddress("lepph_pz", &lepph_pz, &b_lepph_pz);
   fChain->SetBranchAddress("lepph_m", &lepph_m, &b_lepph_m);
   fChain->SetBranchAddress("lepph_rap", &lepph_rap, &b_lepph_rap);
   fChain->SetBranchAddress("lepph_dr", &lepph_dr, &b_lepph_dr);
   fChain->SetBranchAddress("lepph_dphi", &lepph_dphi, &b_lepph_dphi);
   fChain->SetBranchAddress("lepph_deta", &lepph_deta, &b_lepph_deta);
   fChain->SetBranchAddress("lepph_ctheta", &lepph_ctheta, &b_lepph_ctheta);
   fChain->SetBranchAddress("fjph_pt", &fjph_pt, &b_fjph_pt);
   fChain->SetBranchAddress("fjph_eta", &fjph_eta, &b_fjph_eta);
   fChain->SetBranchAddress("fjph_phi", &fjph_phi, &b_fjph_phi);
   fChain->SetBranchAddress("fjph_e", &fjph_e, &b_fjph_e);
   fChain->SetBranchAddress("fjph_pz", &fjph_pz, &b_fjph_pz);
   fChain->SetBranchAddress("fjph_m", &fjph_m, &b_fjph_m);
   fChain->SetBranchAddress("fjph_rap", &fjph_rap, &b_fjph_rap);
   fChain->SetBranchAddress("fjph_dr", &fjph_dr, &b_fjph_dr);
   fChain->SetBranchAddress("fjph_dphi", &fjph_dphi, &b_fjph_dphi);
   fChain->SetBranchAddress("fjph_deta", &fjph_deta, &b_fjph_deta);
   fChain->SetBranchAddress("fjph_ctheta", &fjph_ctheta, &b_fjph_ctheta);
   fChain->SetBranchAddress("bph_pt", &bph_pt, &b_bph_pt);
   fChain->SetBranchAddress("bph_eta", &bph_eta, &b_bph_eta);
   fChain->SetBranchAddress("bph_phi", &bph_phi, &b_bph_phi);
   fChain->SetBranchAddress("bph_e", &bph_e, &b_bph_e);
   fChain->SetBranchAddress("bph_pz", &bph_pz, &b_bph_pz);
   fChain->SetBranchAddress("bph_m", &bph_m, &b_bph_m);
   fChain->SetBranchAddress("bph_rap", &bph_rap, &b_bph_rap);
   fChain->SetBranchAddress("bph_dr", &bph_dr, &b_bph_dr);
   fChain->SetBranchAddress("bph_dphi", &bph_dphi, &b_bph_dphi);
   fChain->SetBranchAddress("bph_deta", &bph_deta, &b_bph_deta);
   fChain->SetBranchAddress("bph_ctheta", &bph_ctheta, &b_bph_ctheta);
   fChain->SetBranchAddress("nuph_pt", &nuph_pt, &b_nuph_pt);
   fChain->SetBranchAddress("nuph_eta", &nuph_eta, &b_nuph_eta);
   fChain->SetBranchAddress("nuph_phi", &nuph_phi, &b_nuph_phi);
   fChain->SetBranchAddress("nuph_e", &nuph_e, &b_nuph_e);
   fChain->SetBranchAddress("nuph_pz", &nuph_pz, &b_nuph_pz);
   fChain->SetBranchAddress("nuph_m", &nuph_m, &b_nuph_m);
   fChain->SetBranchAddress("nuph_rap", &nuph_rap, &b_nuph_rap);
   fChain->SetBranchAddress("nuph_dr", &nuph_dr, &b_nuph_dr);
   fChain->SetBranchAddress("nuph_dphi", &nuph_dphi, &b_nuph_dphi);
   fChain->SetBranchAddress("nuph_deta", &nuph_deta, &b_nuph_deta);
   fChain->SetBranchAddress("nuph_ctheta", &nuph_ctheta, &b_nuph_ctheta);
   fChain->SetBranchAddress("bnu_pt", &bnu_pt, &b_bnu_pt);
   fChain->SetBranchAddress("bnu_eta", &bnu_eta, &b_bnu_eta);
   fChain->SetBranchAddress("bnu_phi", &bnu_phi, &b_bnu_phi);
   fChain->SetBranchAddress("bnu_e", &bnu_e, &b_bnu_e);
   fChain->SetBranchAddress("bnu_pz", &bnu_pz, &b_bnu_pz);
   fChain->SetBranchAddress("bnu_m", &bnu_m, &b_bnu_m);
   fChain->SetBranchAddress("bnu_rap", &bnu_rap, &b_bnu_rap);
   fChain->SetBranchAddress("bnu_dr", &bnu_dr, &b_bnu_dr);
   fChain->SetBranchAddress("bnu_dphi", &bnu_dphi, &b_bnu_dphi);
   fChain->SetBranchAddress("bnu_deta", &bnu_deta, &b_bnu_deta);
   fChain->SetBranchAddress("bnu_ctheta", &bnu_ctheta, &b_bnu_ctheta);
   fChain->SetBranchAddress("blep_pt", &blep_pt, &b_blep_pt);
   fChain->SetBranchAddress("blep_eta", &blep_eta, &b_blep_eta);
   fChain->SetBranchAddress("blep_phi", &blep_phi, &b_blep_phi);
   fChain->SetBranchAddress("blep_e", &blep_e, &b_blep_e);
   fChain->SetBranchAddress("blep_pz", &blep_pz, &b_blep_pz);
   fChain->SetBranchAddress("blep_m", &blep_m, &b_blep_m);
   fChain->SetBranchAddress("blep_rap", &blep_rap, &b_blep_rap);
   fChain->SetBranchAddress("blep_dr", &blep_dr, &b_blep_dr);
   fChain->SetBranchAddress("blep_dphi", &blep_dphi, &b_blep_dphi);
   fChain->SetBranchAddress("blep_deta", &blep_deta, &b_blep_deta);
   fChain->SetBranchAddress("blep_ctheta", &blep_ctheta, &b_blep_ctheta);
   fChain->SetBranchAddress("bfj_pt", &bfj_pt, &b_bfj_pt);
   fChain->SetBranchAddress("bfj_eta", &bfj_eta, &b_bfj_eta);
   fChain->SetBranchAddress("bfj_phi", &bfj_phi, &b_bfj_phi);
   fChain->SetBranchAddress("bfj_e", &bfj_e, &b_bfj_e);
   fChain->SetBranchAddress("bfj_pz", &bfj_pz, &b_bfj_pz);
   fChain->SetBranchAddress("bfj_m", &bfj_m, &b_bfj_m);
   fChain->SetBranchAddress("bfj_rap", &bfj_rap, &b_bfj_rap);
   fChain->SetBranchAddress("bfj_dr", &bfj_dr, &b_bfj_dr);
   fChain->SetBranchAddress("bfj_dphi", &bfj_dphi, &b_bfj_dphi);
   fChain->SetBranchAddress("bfj_deta", &bfj_deta, &b_bfj_deta);
   fChain->SetBranchAddress("bfj_ctheta", &bfj_ctheta, &b_bfj_ctheta);
   fChain->SetBranchAddress("lfj_pt", &lfj_pt, &b_lfj_pt);
   fChain->SetBranchAddress("lfj_eta", &lfj_eta, &b_lfj_eta);
   fChain->SetBranchAddress("lfj_phi", &lfj_phi, &b_lfj_phi);
   fChain->SetBranchAddress("lfj_e", &lfj_e, &b_lfj_e);
   fChain->SetBranchAddress("lfj_pz", &lfj_pz, &b_lfj_pz);
   fChain->SetBranchAddress("lfj_m", &lfj_m, &b_lfj_m);
   fChain->SetBranchAddress("lfj_rap", &lfj_rap, &b_lfj_rap);
   fChain->SetBranchAddress("lfj_dr", &lfj_dr, &b_lfj_dr);
   fChain->SetBranchAddress("lfj_dphi", &lfj_dphi, &b_lfj_dphi);
   fChain->SetBranchAddress("lfj_deta", &lfj_deta, &b_lfj_deta);
   fChain->SetBranchAddress("lfj_ctheta", &lfj_ctheta, &b_lfj_ctheta);
   fChain->SetBranchAddress("nufj_pt", &nufj_pt, &b_nufj_pt);
   fChain->SetBranchAddress("nufj_eta", &nufj_eta, &b_nufj_eta);
   fChain->SetBranchAddress("nufj_phi", &nufj_phi, &b_nufj_phi);
   fChain->SetBranchAddress("nufj_e", &nufj_e, &b_nufj_e);
   fChain->SetBranchAddress("nufj_pz", &nufj_pz, &b_nufj_pz);
   fChain->SetBranchAddress("nufj_m", &nufj_m, &b_nufj_m);
   fChain->SetBranchAddress("nufj_rap", &nufj_rap, &b_nufj_rap);
   fChain->SetBranchAddress("nufj_dr", &nufj_dr, &b_nufj_dr);
   fChain->SetBranchAddress("nufj_dphi", &nufj_dphi, &b_nufj_dphi);
   fChain->SetBranchAddress("nufj_deta", &nufj_deta, &b_nufj_deta);
   fChain->SetBranchAddress("nufj_ctheta", &nufj_ctheta, &b_nufj_ctheta);
   fChain->SetBranchAddress("lep1lep2_pt", &lep1lep2_pt, &b_lep1lep2_pt);
   fChain->SetBranchAddress("lep1lep2_eta", &lep1lep2_eta, &b_lep1lep2_eta);
   fChain->SetBranchAddress("lep1lep2_phi", &lep1lep2_phi, &b_lep1lep2_phi);
   fChain->SetBranchAddress("lep1lep2_e", &lep1lep2_e, &b_lep1lep2_e);
   fChain->SetBranchAddress("lep1lep2_pz", &lep1lep2_pz, &b_lep1lep2_pz);
   fChain->SetBranchAddress("lep1lep2_m", &lep1lep2_m, &b_lep1lep2_m);
   fChain->SetBranchAddress("lep1lep2_rap", &lep1lep2_rap, &b_lep1lep2_rap);
   fChain->SetBranchAddress("lep1lep2_dr", &lep1lep2_dr, &b_lep1lep2_dr);
   fChain->SetBranchAddress("lep1lep2_dphi", &lep1lep2_dphi, &b_lep1lep2_dphi);
   fChain->SetBranchAddress("lep1lep2_deta", &lep1lep2_deta, &b_lep1lep2_deta);
   fChain->SetBranchAddress("lep1lep2_ctheta", &lep1lep2_ctheta, &b_lep1lep2_ctheta);
   if(!m_isData && m_isNom){
     fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
     fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
     fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
     fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_UP", &weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_weight_globalLeptonTriggerSF_EL_Trigger_UP);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
     fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
     fChain->SetBranchAddress("weight_photonSF_ID_UP", &weight_photonSF_ID_UP, &b_weight_photonSF_ID_UP);
     fChain->SetBranchAddress("weight_photonSF_ID_DOWN", &weight_photonSF_ID_DOWN, &b_weight_photonSF_ID_DOWN);
     fChain->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso, &b_weight_photonSF_effIso);
     fChain->SetBranchAddress("weight_photonSF_effIso_UP", &weight_photonSF_effIso_UP, &b_weight_photonSF_effIso_UP);
     fChain->SetBranchAddress("weight_photonSF_effIso_DOWN", &weight_photonSF_effIso_DOWN, &b_weight_photonSF_effIso_DOWN);
     fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
     fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_up", &weight_bTagSF_DL1r_70_eigenvars_B_up, &b_weight_bTagSF_DL1r_70_eigenvars_B_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_up", &weight_bTagSF_DL1r_70_eigenvars_C_up, &b_weight_bTagSF_DL1r_70_eigenvars_C_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_up", &weight_bTagSF_DL1r_70_eigenvars_Light_up, &b_weight_bTagSF_DL1r_70_eigenvars_Light_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_down", &weight_bTagSF_DL1r_70_eigenvars_B_down, &b_weight_bTagSF_DL1r_70_eigenvars_B_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_down", &weight_bTagSF_DL1r_70_eigenvars_C_down, &b_weight_bTagSF_DL1r_70_eigenvars_C_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_down", &weight_bTagSF_DL1r_70_eigenvars_Light_down, &b_weight_bTagSF_DL1r_70_eigenvars_Light_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_up", &weight_bTagSF_DL1r_70_extrapolation_up, &b_weight_bTagSF_DL1r_70_extrapolation_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_down", &weight_bTagSF_DL1r_70_extrapolation_down, &b_weight_bTagSF_DL1r_70_extrapolation_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_up", &weight_bTagSF_DL1r_70_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_down", &weight_bTagSF_DL1r_70_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &weight_bTagSF_DL1r_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &weight_bTagSF_DL1r_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &weight_bTagSF_DL1r_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &weight_bTagSF_DL1r_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
     fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);
   }
   Notify();
}

Bool_t minintuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void minintuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t minintuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef minintuple_cxx
