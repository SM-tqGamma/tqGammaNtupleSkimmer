from __future__ import print_function
import pandas as pd

df = pd.read_csv("SFs_final_40.csv")

for index, row in df.iterrows():
    print('{"conv_eta%s_pt%s", ' %(row['etaBin'], row['ph_pt_bin']) , end="")
    print("std::vector<Float_t>{ %8.4f, %8.4f }}," %(row['SF_conv'], row['SF_conv_TotUnc']))
    print('{"unconv_eta%s_pt%s", ' %(row['etaBin'], row['ph_pt_bin']) ,end="")
    print("std::vector<Float_t>{ %8.4f, %8.4f }}," %(row['SF_unconv'], row['SF_unconv_TotUnc']))
