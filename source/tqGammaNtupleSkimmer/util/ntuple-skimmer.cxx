#include "tqGammaNtupleSkimmer/TqGammaEventSaver.h"

//#include <AsgTools/MessageCheck.h>
//#include "AsgTools/AsgMessaging.h"


#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

#include <TFile.h>
#include <TString.h>
#include <TSystem.h>

using namespace std;


int main(int argc, char* argv[]) {

  TString configFile = "/code/harish/tqGamma/source/tqGammaNtupleSkimmer/scripts/generic.conf";
  string samplePath = "";//410389.root";
  TString outName = "";
  int isData = 0;
  int doFake = 0;
  int doSys = 0;
  TString sysName = "";

  //gSystem->ExpandPathName(configFile);

  for (int i=0; i<argc; ++i) {
    string arg = argv[i];

    if(arg == "--in") {
      if(argc > i+1) {
	samplePath = argv[++i];
      }
      else {
	std::cout<<"Invalid input file"<<std::endl;
      }

    }

    if(arg == "--out") {
      if(argc > i+1) {
        outName = argv[++i];
      }
      else {
	std::cout<<"Invalid configFile name name"<<std::endl;
      }

    }


    if(arg == "--conf") {
      if(argc > i+1) {
        configFile = argv[++i];
      }
      else {
	std::cout<<"Invalid output name"<<std::endl;
      }

    }


    if(arg == "--isdata") {
      if(argc > i+1) {
        isData = stoi(argv[++i]);
      }
      else {
	std::cout<<"Invalid Data Type"<<std::endl;
      }

    }

    if(arg == "--dofake") {
      if(argc > i+1) {
        doFake = stoi(argv[++i]);
	if(doFake) {
	  isData = 1;
	}
      }
      else {
	std::cout<<"Invalid value for doFake"<<std::endl;
      }

    }


    if(arg == "--dosys") {
      if(argc > i+1) {
        doSys = stoi(argv[++i]);
      }
      else {
	std::cout<<"Invalid value for dosys"<<std::endl;
      }

    }

    if(arg == "--sysName") {
      if(argc > i+1) {
	sysName = argv[++i];
      }
      else {
	std::cout<<"Invalid systematic name"<<std::endl;
      }

    }



  }

  if(samplePath == "") {
    std::cout << "Input is invalid"<<std::endl;
    return EXIT_FAILURE;
  }

  TqGammaEventSaver eventsaver;

  if( !eventsaver.initialize(configFile, samplePath, outName, isData, doFake, doSys, sysName).isSuccess() ) return EXIT_FAILURE;
  if( !eventsaver.executeEventLoop().isSuccess() ) return EXIT_FAILURE;
  return EXIT_SUCCESS;
}
