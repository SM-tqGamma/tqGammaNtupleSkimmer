flist=/data_ceph/harish/v3_tqGamma/mc16a/*_root

count=1
for file in ${flist[@]}
do
fname=$(basename $file _root)
fAll=($file/*.root)
cname="${fAll[0]}"
if (( $count % 10 == 0 ))
then
    ntuple-skimmer --in "$file/*.root" --dosys 1 --conf $cname >& logs/$fname.txt
else
    ntuple-skimmer --in "$file/*.root" --dosys 1 --conf $cname >& logs/$fname.txt &
fi
count=`expr $count + 1`
done
