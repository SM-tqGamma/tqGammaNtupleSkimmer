#include "tqGammaNtupleSkimmer/TqGammaEventSaver.h"

void TqGammaEventSaver::DefineOutputTree(bool isNominal) {


  if (!m_isData) {
    m_outTree->Branch("weight_mc", &m_inTree.weight_mc);
    m_outTree->Branch("weight_pileup", &m_inTree.weight_pileup);
    m_outTree->Branch("weight_leptonSF", &m_inTree.weight_leptonSF);
    m_outTree->Branch("weight_photonSF", &m_inTree.weight_photonSF);
    m_outTree->Branch("weight_globalLeptonTriggerSF", &m_inTree.weight_globalLeptonTriggerSF);
    m_outTree->Branch("weight_bTagSF_DL1r_60", &m_inTree.weight_bTagSF_DL1r_60);
    m_outTree->Branch("weight_bTagSF_DL1r_70", &m_inTree.weight_bTagSF_DL1r_70);
    m_outTree->Branch("weight_bTagSF_DL1r_77", &m_inTree.weight_bTagSF_DL1r_77);
    m_outTree->Branch("weight_bTagSF_DL1r_85", &m_inTree.weight_bTagSF_DL1r_85);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous", &m_inTree.weight_bTagSF_DL1r_Continuous);
    m_outTree->Branch("weight_jvt", &m_inTree.weight_jvt);
    for(auto& hfakeEnt:m_hFakeWeightOutputMap){
      //cout << hfakeEnt.first << endl;
      m_outTree->Branch(TString(hfakeEnt.first), &hfakeEnt.second);
    }
    for(auto& efakeEnt:m_eFakeWeightOutputMap){
      //cout << efakeEnt.first << endl;
      m_outTree->Branch(TString(efakeEnt.first), &efakeEnt.second);
    }
  }

  if(isNominal and m_doSys) {
    //m_outTree->Branch("sumWeights_vector", &m_sumWeights_vector);
    m_outTree->Branch("mc_generator_weights", &m_inTree.mc_generator_weights);
  }


  if (m_doFake) {
    m_outTree->Branch("ASM_weight", &m_inTree.ASM_weight->at(0));
    m_outTree->Branch("lfakeweight", &m_lfakeweight);
    m_outTree->Branch("lfakeweight_wjetsnorm", &m_lfakeweight_wjetsnorm);
    m_outTree->Branch("lep1_isTight", &m_lep1_isTight);
  }


  m_outTree->Branch("HLT_mu20_iloose_L1MU15", &m_inTree.HLT_mu20_iloose_L1MU15);
  m_outTree->Branch("HLT_e60_lhmedium_nod0", &m_inTree.HLT_e60_lhmedium_nod0);
  m_outTree->Branch("HLT_mu50", &m_inTree.HLT_mu50);
  m_outTree->Branch("HLT_e60_lhmedium", &m_inTree.HLT_e60_lhmedium);
  m_outTree->Branch("HLT_e120_lhloose", &m_inTree.HLT_e120_lhloose);
  m_outTree->Branch("HLT_e24_lhmedium_L1EM20VH", &m_inTree.HLT_e24_lhmedium_L1EM20VH);
  m_outTree->Branch("HLT_mu26_ivarmedium", &m_inTree.HLT_mu26_ivarmedium);
  m_outTree->Branch("HLT_e140_lhloose_nod0", &m_inTree.HLT_e140_lhloose_nod0);
  m_outTree->Branch("HLT_e26_lhtight_nod0_ivarloose", &m_inTree.HLT_e26_lhtight_nod0_ivarloose);

  m_outTree->Branch("eventNumber", &m_inTree.eventNumber);
  m_outTree->Branch("runNumber", &m_inTree.runNumber);
  m_outTree->Branch("mcChannelNumber", &m_inTree.mcChannelNumber);
  m_outTree->Branch("mu", &m_inTree.mu);
  m_outTree->Branch("runYear", &m_runYear);
  m_outTree->Branch("lumi", &m_lumi);

  m_outTree->Branch("met_met", &m_inTree.met_met);
  m_outTree->Branch("met_phi", &m_inTree.met_phi);
  //m_outTree->Branch("scale_nom", &m_scale_nom);
  //m_outTree->Branch("sumWeights", &m_sumWeights);
  m_outTree->Branch("transMass", &m_transMass);
  m_outTree->Branch("transMassWph", &m_transMassWph);
  m_outTree->Branch("transMassWb", &m_transMassWb);
  m_outTree->Branch("HT", &m_HT);
  m_outTree->Branch("efakeweight", &m_efakeweight);
  m_outTree->Branch("efakeweightErr", &m_efakeweightErr);
  m_outTree->Branch("efakeweight_UP", &m_efakeweight_up);
  m_outTree->Branch("efakeweight_DOWN", &m_efakeweight_down);

  m_outTree->Branch("hfakeweight", &m_hfakeweight);
  m_outTree->Branch("hfakeweightErr", &m_hfakeweightErr);
  m_outTree->Branch("hfakeweight_UP", &m_hfakeweight_up);
  m_outTree->Branch("hfakeweight_DOWN", &m_hfakeweight_down);

  m_outTree->Branch("weight_photonSFown", &m_weight_photonSFown);
  m_outTree->Branch("weight_photonSFown_ID_up", &m_weight_photonSFown_ID_up);
  m_outTree->Branch("weight_photonSFown_ID_down", &m_weight_photonSFown_ID_down);
  m_outTree->Branch("weight_photonSFown_ISO_up", &m_weight_photonSFown_ISO_up);
  m_outTree->Branch("weight_photonSFown_ISO_down", &m_weight_photonSFown_ISO_down);

  m_outTree->Branch("event_in_overlap", &m_inTree.event_in_overlap);
  m_outTree->Branch("truthJetMatchedB", &m_inTree.truthJetMatchedB);
  m_outTree->Branch("truthJetMatchedD", &m_inTree.truthJetMatchedD);

  std::list<leptonObj*> lepList = {&lep1, &lep2, &nu1};
  lep1.name.assign("lep1");
  lep2.name.assign("lep2");
  nu1.name.assign("nu1");

  for ( auto & iLep : lepList ) {

    ATH_MSG_DEBUG("lep name: "<< iLep->name);

    m_outTree->Branch((iLep->name+"_pt").c_str(), &iLep->pt);
    m_outTree->Branch((iLep->name+"_eta").c_str(), &iLep->eta );
    m_outTree->Branch((iLep->name+"_phi").c_str(), &iLep->phi );
    m_outTree->Branch((iLep->name+"_e").c_str(), &iLep->e);
    m_outTree->Branch((iLep->name+"_pz").c_str(), &iLep->pz);

    if (iLep->name.find("lep") == std::string::npos)
      continue;

    m_outTree->Branch((iLep->name+"_d0sig").c_str(), &iLep->d0sig);
    m_outTree->Branch((iLep->name+"_z0sintheta").c_str(), &iLep->z0sintheta);
    m_outTree->Branch((iLep->name+"_topoetcone20").c_str(), &iLep->topoetcone20);
    m_outTree->Branch((iLep->name+"_ptvarcone30").c_str(), &iLep->ptvarcone30);
    m_outTree->Branch((iLep->name+"_trueType").c_str(), &iLep->trueType);
    m_outTree->Branch((iLep->name+"_trueOrigin").c_str(), &iLep->trueOrigin);
    m_outTree->Branch((iLep->name+"_id").c_str(), &iLep->id);
    m_outTree->Branch((iLep->name+"_mc_pid").c_str(), &iLep->mc_pid);
    m_outTree->Branch((iLep->name+"_mc_charge").c_str(), &iLep->mc_charge);
    m_outTree->Branch((iLep->name+"_mc_pt").c_str(), &iLep->mc_pt);
    m_outTree->Branch((iLep->name+"_mc_eta").c_str(), &iLep->mc_eta);
    m_outTree->Branch((iLep->name+"_mc_phi").c_str(), &iLep->mc_phi);
  }

  m_outTree->Branch("nlep", &m_nlep);
  m_outTree->Branch("nlep_raw", &m_nlep_raw);

  m_outTree->Branch("ph_pt", &photon1.pt);
  m_outTree->Branch("phintop_pt", &m_phintop_pt);
  m_outTree->Branch("phintop_eta", &m_phintop_eta);
  m_outTree->Branch("phintop_phi", &m_phintop_phi);
  m_outTree->Branch("phintop_e", &m_phintop_e);
  m_outTree->Branch("ph_eta", &photon1.eta);
  m_outTree->Branch("ph_phi", &photon1.phi);
  m_outTree->Branch("ph_e", &photon1.e);
  m_outTree->Branch("ph_topoetcone20", &photon1.topoetcone20);
  m_outTree->Branch("ph_topoetcone30", &photon1.topoetcone30);
  m_outTree->Branch("ph_topoetcone40", &photon1.topoetcone40);
  m_outTree->Branch("ph_ptcone20", &photon1.ptcone20);
  m_outTree->Branch("ph_ptcone30", &photon1.ptcone30);
  m_outTree->Branch("ph_ptcone40", &photon1.ptcone40);
  m_outTree->Branch("ph_isoFCT", &photon1.isoFCT);
  m_outTree->Branch("ph_isoFCL", &photon1.isoFCL);
  m_outTree->Branch("ph_isTight", &photon1.isTight);
  m_outTree->Branch("ph_isLoose", &photon1.isLoose);
  m_outTree->Branch("ph_author", &photon1.author);
  m_outTree->Branch("ph_conversionType", &photon1.conversionType);
  m_outTree->Branch("ph_caloEta", &photon1.caloEta);
  m_outTree->Branch("ph_isEM_Tight", &photon1.isEM_Tight);
  m_outTree->Branch("ph_truthType", &photon1.truthType);
  m_outTree->Branch("ph_truthOrigin", &photon1.truthOrigin);
  m_outTree->Branch("ph_type", &photon1.type);
  m_outTree->Branch("ph_sf_id", &photon1.sf_id);
  m_outTree->Branch("ph_sf_iso", &photon1.sf_iso);
  m_outTree->Branch("ph_mc_pid", &photon1.mc_pid);
  m_outTree->Branch("ph_mc_pt", &photon1.mc_pt);
  m_outTree->Branch("ph_mc_eta", &photon1.mc_eta);
  m_outTree->Branch("ph_mc_phi", &photon1.mc_phi);
  m_outTree->Branch("ph_mcel_dr", &photon1.mcel_dr);
  m_outTree->Branch("ph_mcel_pt", &photon1.mcel_pt);
  m_outTree->Branch("ph_mcel_eta", &photon1.mcel_eta);
  m_outTree->Branch("ph_mcel_phi", &photon1.mcel_phi);
  m_outTree->Branch("ph_mc_production", &photon1.mc_production);
  m_outTree->Branch("nph", &m_nph);
  m_outTree->Branch("nph_raw", &m_nph_raw);


  std::list<jetObj*> jetList = {&ljet, &sljet, &lbjet, &slbjet, &fjet};

  ljet.name.assign("lj");
  sljet.name.assign("slj");
  lbjet.name.assign("lbj");
  slbjet.name.assign("slbj");
  fjet.name.assign("fj");

  for( auto & iJet : jetList ) {
    ATH_MSG_DEBUG("jet name: "<< iJet->name);
    m_outTree->Branch((iJet->name+"_pt").c_str(), &iJet->pt);
    m_outTree->Branch((iJet->name+"_eta").c_str(), &iJet->eta);
    m_outTree->Branch((iJet->name+"_phi").c_str(), &iJet->phi);
    m_outTree->Branch((iJet->name+"_e").c_str(), &iJet->e);
    m_outTree->Branch((iJet->name+"_DL1r").c_str(), &iJet->DL1r);
    m_outTree->Branch((iJet->name+"_jvt").c_str(), &iJet->jvt);
    m_outTree->Branch((iJet->name+"_passfjvt").c_str(), &iJet->passfjvt);
    m_outTree->Branch((iJet->name+"_tagWeightBin_DL1r_Continuous").c_str(), &iJet->tagWeightBin_DL1r_Continuous);
    m_outTree->Branch((iJet->name+"_truthflav").c_str(), &iJet->truthflav);
    m_outTree->Branch((iJet->name+"_isBjet").c_str(), &iJet->isBjet);
  }

  m_outTree->Branch("njets", &m_njets);
  m_outTree->Branch("nbjets", &m_nbjets);
  m_outTree->Branch("nfjets", &m_nfjets);
  m_outTree->Branch("njets_raw", &m_njets_raw);
  m_outTree->Branch("nbjets_DL1r_60", &m_nbjets_DL1r_60);
  m_outTree->Branch("nbjets_DL1r_70", &m_nbjets_DL1r_70);
  m_outTree->Branch("nbjets_DL1r_77", &m_nbjets_DL1r_77);
  m_outTree->Branch("nbjets_DL1r_85", &m_nbjets_DL1r_85);
  m_outTree->Branch("fjet_flag", &m_fjet_flag);


  Wbsn.name.assign("Wbsn");
  topq.name.assign("top");
  topph.name.assign("topph");
  topfj.name.assign("topfj");
  Wph.name.assign("Wph");
  lepph.name.assign("lepph");
  fjph.name.assign("fjph");
  bph.name.assign("bph");
  nuph.name.assign("nuph");
  bnu.name.assign("bnu");
  blep.name.assign("blep");
  bfj.name.assign("bfj");
  lfj.name.assign("lfj");
  nufj.name.assign("nufj");
  lep1lep2.name.assign("lep1lep2");

  for ( auto & iRes : resList ) {

    ATH_MSG_DEBUG("Resonance name: "<< iRes->name);

    m_outTree->Branch((iRes->name+"_pt").c_str(), &iRes->pt);
    m_outTree->Branch((iRes->name+"_eta").c_str(), &iRes->eta );
    m_outTree->Branch((iRes->name+"_phi").c_str(), &iRes->phi );
    m_outTree->Branch((iRes->name+"_e").c_str(), &iRes->e);
    m_outTree->Branch((iRes->name+"_pz").c_str(), &iRes->pz);
    m_outTree->Branch((iRes->name+"_m").c_str(), &iRes->m);
    m_outTree->Branch((iRes->name+"_rap").c_str(), &iRes->rap);
    m_outTree->Branch((iRes->name+"_dr").c_str(), &iRes->dr);
    m_outTree->Branch((iRes->name+"_dphi").c_str(), &iRes->dphi );
    m_outTree->Branch((iRes->name+"_deta").c_str(), &iRes->deta );
    m_outTree->Branch((iRes->name+"_ctheta").c_str(), &iRes->ctheta);

  }

  // Add weights corresponding to systematic uncertainties to nominal tree
  if(isNominal and m_doSys and (!m_isData)) {
    m_outTree->Branch("weight_pileup_UP", &m_inTree.weight_pileup_UP);
    m_outTree->Branch("weight_pileup_DOWN", &m_inTree.weight_pileup_DOWN);
    m_outTree->Branch("weight_leptonSF_EL_SF_Trigger_UP", &m_inTree.weight_leptonSF_EL_SF_Trigger_UP);
    m_outTree->Branch("weight_leptonSF_EL_SF_Trigger_DOWN", &m_inTree.weight_leptonSF_EL_SF_Trigger_DOWN);
    m_outTree->Branch("weight_leptonSF_EL_SF_Reco_UP", &m_inTree.weight_leptonSF_EL_SF_Reco_UP);
    m_outTree->Branch("weight_leptonSF_EL_SF_Reco_DOWN", &m_inTree.weight_leptonSF_EL_SF_Reco_DOWN);
    m_outTree->Branch("weight_leptonSF_EL_SF_ID_UP", &m_inTree.weight_leptonSF_EL_SF_ID_UP);
    m_outTree->Branch("weight_leptonSF_EL_SF_ID_DOWN", &m_inTree.weight_leptonSF_EL_SF_ID_DOWN);
    m_outTree->Branch("weight_leptonSF_EL_SF_Isol_UP", &m_inTree.weight_leptonSF_EL_SF_Isol_UP);
    m_outTree->Branch("weight_leptonSF_EL_SF_Isol_DOWN", &m_inTree.weight_leptonSF_EL_SF_Isol_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_Trigger_STAT_UP", &m_inTree.weight_leptonSF_MU_SF_Trigger_STAT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &m_inTree.weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_Trigger_SYST_UP", &m_inTree.weight_leptonSF_MU_SF_Trigger_SYST_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &m_inTree.weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_STAT_UP", &m_inTree.weight_leptonSF_MU_SF_ID_STAT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_STAT_DOWN", &m_inTree.weight_leptonSF_MU_SF_ID_STAT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_SYST_UP", &m_inTree.weight_leptonSF_MU_SF_ID_SYST_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_SYST_DOWN", &m_inTree.weight_leptonSF_MU_SF_ID_SYST_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &m_inTree.weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &m_inTree.weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &m_inTree.weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &m_inTree.weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_Isol_STAT_UP", &m_inTree.weight_leptonSF_MU_SF_Isol_STAT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &m_inTree.weight_leptonSF_MU_SF_Isol_STAT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_Isol_SYST_UP", &m_inTree.weight_leptonSF_MU_SF_Isol_SYST_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &m_inTree.weight_leptonSF_MU_SF_Isol_SYST_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_TTVA_STAT_UP", &m_inTree.weight_leptonSF_MU_SF_TTVA_STAT_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &m_inTree.weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
    m_outTree->Branch("weight_leptonSF_MU_SF_TTVA_SYST_UP", &m_inTree.weight_leptonSF_MU_SF_TTVA_SYST_UP);
    m_outTree->Branch("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &m_inTree.weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
    m_outTree->Branch("weight_globalLeptonTriggerSF_EL_Trigger_UP", &m_inTree.weight_globalLeptonTriggerSF_EL_Trigger_UP);
    m_outTree->Branch("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &m_inTree.weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
    m_outTree->Branch("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &m_inTree.weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
    m_outTree->Branch("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &m_inTree.weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
    m_outTree->Branch("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &m_inTree.weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
    m_outTree->Branch("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &m_inTree.weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
    m_outTree->Branch("weight_photonSF_ID_UP", &m_inTree.weight_photonSF_ID_UP);
    m_outTree->Branch("weight_photonSF_ID_DOWN", &m_inTree.weight_photonSF_ID_DOWN);
    m_outTree->Branch("weight_photonSF_effIso", &m_inTree.weight_photonSF_effIso);
    m_outTree->Branch("weight_photonSF_effIso_UP", &m_inTree.weight_photonSF_effIso_UP);
    m_outTree->Branch("weight_photonSF_effIso_DOWN", &m_inTree.weight_photonSF_effIso_DOWN);
    m_outTree->Branch("weight_jvt_UP", &m_inTree.weight_jvt_UP);
    m_outTree->Branch("weight_jvt_DOWN", &m_inTree.weight_jvt_DOWN);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_B_up", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_B_up);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_C_up", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_C_up);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_Light_up", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_Light_up);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_B_down", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_B_down);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_C_down", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_C_down);
    m_outTree->Branch("weight_bTagSF_DL1r_70_eigenvars_Light_down", &m_inTree.weight_bTagSF_DL1r_70_eigenvars_Light_down);
    m_outTree->Branch("weight_bTagSF_DL1r_70_extrapolation_up", &m_inTree.weight_bTagSF_DL1r_70_extrapolation_up);
    m_outTree->Branch("weight_bTagSF_DL1r_70_extrapolation_down", &m_inTree.weight_bTagSF_DL1r_70_extrapolation_down);
    m_outTree->Branch("weight_bTagSF_DL1r_70_extrapolation_from_charm_up", &m_inTree.weight_bTagSF_DL1r_70_extrapolation_from_charm_up);
    m_outTree->Branch("weight_bTagSF_DL1r_70_extrapolation_from_charm_down", &m_inTree.weight_bTagSF_DL1r_70_extrapolation_from_charm_down);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
    m_outTree->Branch("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &m_inTree.weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);
  }


}
