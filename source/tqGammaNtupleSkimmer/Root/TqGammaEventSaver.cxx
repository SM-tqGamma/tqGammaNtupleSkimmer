#include "tqGammaNtupleSkimmer/TqGammaEventSaver.h"
#include "TopDataPreparation/SampleXsection.h"
#include "TTreeReader.h"
#include "TH2D.h"
using namespace std;

TqGammaEventSaver::TqGammaEventSaver(const string& name) :
  asg::AsgMessaging(name)
{
  //StatusCode::enableFailure();
}

TqGammaEventSaver::~TqGammaEventSaver() {

}


StatusCode TqGammaEventSaver::initialize(const TString& configFile, const std::string& samplePath, const TString& outName, bool isData, bool doFake, bool doSys, const TString& sysName) {

  StatusCode sc = StatusCode::SUCCESS;

  m_samplePath = samplePath;
  m_outName = outName;

  ATH_MSG_VERBOSE("Reading conf file " << configFile);
  m_isData = isData;
  m_doFake = doFake;
  m_doSys = doSys;
  m_sysName = sysName;
  if(m_isData) m_doSys = false;
  if (m_doFake and (not m_isData) ) {
    ATH_MSG_FATAL("Fake weights are not available for MC!!! Check isData flag if you are running on Data");
    return StatusCode::FAILURE;
  }

  if(m_samplePath != "" && m_samplePath.find(".root") != string::npos) {
    ATH_MSG_INFO("Reading root file " << m_samplePath);
  }
  else {
    ATH_MSG_FATAL("Unable to read input file!!");
    return StatusCode::FAILURE;
  }

  m_treeVec = CxxUtils::make_unique<vector<string>>();

  if(m_doSys && m_sysName == "") {
    TFile* Rootfile = TFile::Open(configFile, "READ");
    TIter nextkey( Rootfile->GetListOfKeys() );
    TKey *key;
    while((key = (TKey*)nextkey())) {

      TObject *obj = key->ReadObj();
      if( obj->IsA()->InheritsFrom( TTree::Class())) {
	TTree* tree_data =  (TTree*)obj;
	m_treeVec->push_back(tree_data->GetName());
      }
    }
    Rootfile->Close();
    delete Rootfile;
  }
  else {
    if(m_doFake) {
      m_treeVec->push_back("nominal_Loose");
      vector<TString> selection = {"1j1b70", "1j0b701b85"};
      vector<TString> channel = {"_el", "_mu"};
      vector<TString> suffix = {"", "_var"};
      for(TString sel : selection){
        for(TString chan : channel){
          for(TString suf : suffix){
            TFile * lfakefile = new TFile(m_lfakepath+sel+chan+suf+".root");
            TH2F * rHist = (TH2F*)lfakefile->Get("real_pt_mt_2D_Tight");
            TH2F * fHist = (TH2F*)lfakefile->Get("lfake_pt_mt_2D_Tight");
            rHist->SetDirectory(0);
            fHist->SetDirectory(0);
            m_lfakeweight_maps.insert({sel+chan+"_real"+suf, rHist});
            m_lfakeweight_maps.insert({sel+chan+"_fake"+suf, fHist});
            lfakefile->Close();
          }
        }
      }
    }
    else if(m_sysName != ""){
      m_treeVec->push_back(m_sysName.Data());
    }
    else {
    m_treeVec->push_back("nominal");
    }
  }
  InitHFakeWeightFromFile();
  InitEFakeWeightFromFile();


  return sc;
}



StatusCode TqGammaEventSaver::GetSumWeightsXSec() {
  StatusCode sc = StatusCode::SUCCESS;

  TChain nomchain("nominal");
  nomchain.Add(m_samplePath.c_str());

  if(nomchain.GetEntries() == 0) {
    return sc;
  }
  nomchain.GetEntry(0);

  if(m_isData) {
    m_dsid = std::to_string((int) nomchain.GetLeaf("runNumber")->GetValue(0));
    return sc;
  }

  m_dsid = std::to_string((int) nomchain.GetLeaf("mcChannelNumber")->GetValue(0));

  SampleXsection tdp;
  string xsec_file = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC16-13TeV.data";
  if (!tdp.readFromFile(xsec_file.c_str())) {
    ATH_MSG_FATAL("TopDataPreparation - could not read file\n" << xsec_file << "\n" << "Maybe check it exists, first?\n" );
    return StatusCode::FAILURE;
  }

  m_xsec = tdp.getXsection(m_dsid.Atoi());
  ATH_MSG_INFO("Sample X-section: "<<m_xsec);

  TChain mychain("sumWeights");
  mychain.Add(m_samplePath.c_str());
  TTreeReader reader(&mychain);
  TTreeReaderValue<Float_t> total_events_weighted_reader{reader, "totalEventsWeighted"};
  TTreeReaderValue<ULong64_t> total_events_reader(reader, "totalEvents");

  while (reader.Next()) {
    m_sumWeights += *total_events_weighted_reader;
    m_tot_events_processed += *total_events_reader;
  }
  ATH_MSG_INFO("Sample sum_Weights: "<<m_sumWeights);
  m_scale_nom = m_xsec / m_sumWeights;

  MCLumiHistMap.insert({"nominal", TH1F("MCLumi_nominal","MCLumi_nominal",1,0,1)});
  MCLumiHistMap["nominal"].Fill(0.5, 1./m_scale_nom);
  SetWeightMaps();
  //if(!m_doSys) return sc;

  reader.Restart();
  TTreeReaderValue<std::vector<Float_t> > total_events_weighted_vector_reader{reader, "totalEventsWeighted_mc_generator_weights"};
  TTreeReaderValue<std::vector<std::string> > names_mc_generator_weights_vector_reader{reader, "names_mc_generator_weights"};
  while (reader.Next()) {
    m_sumWeights_vector.resize(total_events_weighted_vector_reader->size());
    m_WeightNames_vector.resize(total_events_weighted_vector_reader->size());
    for (size_t i = 0; i < total_events_weighted_vector_reader->size(); ++i) {
      m_sumWeights_vector.at(i) += total_events_weighted_vector_reader->at(i);
      m_WeightNames_vector.at(i) = names_mc_generator_weights_vector_reader->at(i);
    }
  }
  for (size_t i = 0; i < m_sumWeights_vector.size(); ++i){
    MCLumiHistMap.insert({"weight_"+to_string(i), TH1F(TString("MCLumi_"+to_string(i)),TString("MCLumi_"+to_string(i)),1,0,1)});
    MCLumiHistMap["weight_"+to_string(i)].Fill(0.5, m_sumWeights_vector.at(i)/m_xsec);
  }

  return sc;
}

void TqGammaEventSaver::SetWeightMaps() {
  ATH_MSG_DEBUG("Setting fake photon weight maps: \n");
  m_eFakeWeightMap = {
    {"eta03_conv0", vector<Float_t>{  1.1627,   0.0187}},
    {"eta03_conv1", vector<Float_t>{  2.6482,   0.0879}},
    {"eta03_conv2", vector<Float_t>{  1.8245,   0.0532}},
    {"eta03_conv3", vector<Float_t>{  1.0585,   0.0168}},
    {"eta03_conv4", vector<Float_t>{  1.3017,   0.0954}},
    {"eta03_conv5", vector<Float_t>{  1.7116,   0.2519}},
    {"eta06_conv0", vector<Float_t>{  1.0165,   0.0181}},
    {"eta06_conv1", vector<Float_t>{  1.8978,   0.0610}},
    {"eta06_conv2", vector<Float_t>{  1.5318,   0.0585}},
    {"eta06_conv3", vector<Float_t>{  1.0075,   0.0159}},
    {"eta06_conv4", vector<Float_t>{  1.1365,   0.0842}},
    {"eta06_conv5", vector<Float_t>{  1.7491,   0.1791}},
    {"eta10_conv0", vector<Float_t>{  1.0152,   0.0134}},
    {"eta10_conv1", vector<Float_t>{  1.8025,   0.0610}},
    {"eta10_conv2", vector<Float_t>{  1.0979,   0.0357}},
    {"eta10_conv3", vector<Float_t>{  0.9963,   0.0118}},
    {"eta10_conv4", vector<Float_t>{  0.8777,   0.0623}},
    {"eta10_conv5", vector<Float_t>{  1.1909,   0.0685}},
    {"eta137_conv0", vector<Float_t>{  1.0436,   0.0149}},
    {"eta137_conv1", vector<Float_t>{  1.4042,   0.0448}},
    {"eta137_conv2", vector<Float_t>{  0.9369,   0.0246}},
    {"eta137_conv3", vector<Float_t>{  0.9314,   0.0116}},
    {"eta137_conv4", vector<Float_t>{  0.9954,   0.0353}},
    {"eta137_conv5", vector<Float_t>{  1.1918,   0.0450}},
    {"eta181_conv0", vector<Float_t>{  1.0847,   0.0139}},
    {"eta181_conv1", vector<Float_t>{  1.0650,   0.0368}},
    {"eta181_conv2", vector<Float_t>{  0.9547,   0.0241}},
    {"eta181_conv3", vector<Float_t>{  0.9292,   0.0112}},
    {"eta181_conv4", vector<Float_t>{  0.9496,   0.0353}},
    {"eta181_conv5", vector<Float_t>{  0.9060,   0.0246}},
    {"eta237_conv0", vector<Float_t>{  1.0865,   0.0113}},
    {"eta237_conv1", vector<Float_t>{  1.1393,   0.0242}},
    {"eta237_conv2", vector<Float_t>{  0.9647,   0.0297}},
    {"eta237_conv3", vector<Float_t>{  0.9878,   0.0065}},
    {"eta237_conv4", vector<Float_t>{  0.8353,   0.0659}},
    {"eta237_conv5", vector<Float_t>{  0.7979,   0.0359}},
  };

  m_hFakeWeightMap = {
    {"conv_etaA_pt0", std::vector<Float_t>{   1.6057,   0.3340 }},
    {"unconv_etaA_pt0", std::vector<Float_t>{   1.2202,   0.2859 }},
    {"conv_etaB_pt0", std::vector<Float_t>{   1.5684,   0.3685 }},
    {"unconv_etaB_pt0", std::vector<Float_t>{   1.1802,   0.1471 }},
    {"conv_etaC_pt0", std::vector<Float_t>{   1.5085,   0.2343 }},
    {"unconv_etaC_pt0", std::vector<Float_t>{   1.6465,   0.3038 }},
    {"conv_etaD_pt0", std::vector<Float_t>{   0.9155,   0.2769 }},
    {"unconv_etaD_pt0", std::vector<Float_t>{   1.3611,   0.3925 }},
    {"conv_etaE_pt0", std::vector<Float_t>{   0.9637,   0.8668 }},
    {"unconv_etaE_pt0", std::vector<Float_t>{   1.4133,   0.6211 }},
    {"conv_etaF_pt0", std::vector<Float_t>{   1.0627,   0.3770 }},
    {"unconv_etaF_pt0", std::vector<Float_t>{   1.4133,   0.6211 }},
    {"conv_etaA_pt1", std::vector<Float_t>{   0.8406,   0.1419 }},
    {"unconv_etaA_pt1", std::vector<Float_t>{   0.6235,   0.1194 }},
    {"conv_etaB_pt1", std::vector<Float_t>{   0.8132,   0.1738 }},
    {"unconv_etaB_pt1", std::vector<Float_t>{   0.9823,   0.2004 }},
    {"conv_etaC_pt1", std::vector<Float_t>{   0.9210,   0.2096 }},
    {"unconv_etaC_pt1", std::vector<Float_t>{   1.0619,   0.3135 }},
    {"conv_etaD_pt1", std::vector<Float_t>{   1.0159,   0.1220 }},
    {"unconv_etaD_pt1", std::vector<Float_t>{   1.2015,   0.2205 }},
    {"conv_etaE_pt1", std::vector<Float_t>{   0.8010,   0.1638 }},
    {"unconv_etaE_pt1", std::vector<Float_t>{   1.5704,   0.4038 }},
    {"conv_etaF_pt1", std::vector<Float_t>{   0.4893,   0.3372 }},
    {"unconv_etaF_pt1", std::vector<Float_t>{   1.5704,   0.4038 }},
  };

}


void TqGammaEventSaver::GetLFakeWeight(){

  double lPt = lep1.pt/1000.;
  double mTW = m_transMass/1000.;

  TString selection = "1j1b70";
  TString channel = "_mu";
  if(abs(lep1.id) == 11) channel = "_el";
  if(m_nbjets_DL1r_70 == 0) selection = "1j0b701b85";

  TH2F *  FEffHist_2D = m_lfakeweight_maps[selection+channel+"_fake"];
  TH2F *  REffHist_2D = m_lfakeweight_maps[selection+channel+"_real"];

  TH2F *  FEffHist_2D_var = m_lfakeweight_maps[selection+channel+"_fake_var"];
  TH2F *  REffHist_2D_var = m_lfakeweight_maps[selection+channel+"_real_var"];

  if(lPt > 100.) lPt = 95.;
  if(mTW > 100.) mTW = 95.;
  int r_2D_bin = REffHist_2D->FindBin(lPt, mTW);
  if(abs(lep1.id) == 13 && lPt > 60.) lPt = 57.;
  if(abs(lep1.id) == 13 && mTW > 25.) mTW = 24.;

  int f_2D_bin = FEffHist_2D->FindBin(lPt, mTW);
  int f_2D_bin_var = FEffHist_2D_var->FindBin(lPt, mTW);
  double r_2D = REffHist_2D->GetBinContent(r_2D_bin);
  double r_2D_var = REffHist_2D_var->GetBinContent(r_2D_bin);

  double f_2D = FEffHist_2D->GetBinContent(f_2D_bin);
  double f_2D_var = FEffHist_2D_var->GetBinContent(f_2D_bin_var);

  if(abs(lep1.id) == 11) m_lep1_isTight = m_inTree.el_isTight->at(0);
  else m_lep1_isTight = m_inTree.mu_isTight->at(0);
  if(m_lep1_isTight){
    m_lfakeweight = f_2D/(r_2D-f_2D)*(r_2D-1);
    m_lfakeweight_wjetsnorm = f_2D_var/(r_2D_var-f_2D_var)*(r_2D_var-1);}
  else {
    m_lfakeweight =  f_2D/(r_2D-f_2D)*r_2D;
    m_lfakeweight_wjetsnorm =  f_2D_var/(r_2D_var-f_2D_var)*r_2D_var;
  }
}


StatusCode TqGammaEventSaver::finalize() {
  StatusCode sc = StatusCode::SUCCESS;
  ATH_MSG_INFO("Return code:0  --- Exiting Successfully !!!");
  return sc;
}

StatusCode TqGammaEventSaver::executeEventLoop() {

  StatusCode sc = StatusCode::SUCCESS;
  ATH_CHECK( GetSumWeightsXSec() ); // Get X-section and set DSID for output filename
  ATH_MSG_INFO("DSID: "<< m_dsid);
  if(m_outName == "") m_outName = m_dsid+".root";
  m_outputFile = TFile::Open(m_outName, "RECREATE");
  m_outputFile->cd();

  vector<string>::iterator treeName = m_treeVec->begin();

  for (; treeName != m_treeVec->end(); ++treeName) {

    if(*treeName == "sumWeights" || *treeName == "AnalysisTracking" || *treeName == "truth")  continue;
    ATH_MSG_INFO("Reading tree: "<< *treeName);

    if(!m_inTree.ReadFiles(m_samplePath, *treeName)) {
      ATH_MSG_FATAL("Error reading input sample file.");
      return StatusCode::FAILURE;
    }

    bool isNominal = false;
    if(*treeName == "nominal" and (!m_isData)) {
      isNominal = true;
    };

    m_inTree.Init(m_isData, m_doFake, isNominal);
    unsigned int nEntries = m_inTree.fChain->GetEntries();

    ATH_MSG_INFO("Entries:: "<< nEntries);

    if(nEntries ==0 ) { ATH_MSG_WARNING("Zero entries found in the input tree"); return StatusCode::SUCCESS; }


    m_inTree.GetEntry(0);

    if (m_doFake and *treeName == "nominal_Loose") {
      m_outTree = new TTree("nominal", "Mini Ntuples");
    }
    else {
      m_outTree = new TTree((*treeName).c_str(), "Mini Ntuples");
    }

    DefineOutputTree(isNominal);

    for (unsigned int iEvent = 0; iEvent < nEntries; iEvent++) {

      m_inTree.GetEntry(iEvent);
      ATH_CHECK( FillOutput(iEvent) );

      if((iEvent == nEntries-1) or (iEvent%10000 == 0)) {
	ATH_MSG_INFO("Events " << iEvent+1 << "/" << nEntries << " done.");
      }


    } // nEntries loop

    m_outTree->AutoSave();
    m_outTree->Write();

  } // m_treeVec loop
  for(auto& hist: MCLumiHistMap){
		 hist.second.Write();
  }
  if(!m_isData){
    m_weightTree = new TTree("MCweights", "MCweights");
    m_weightTree->Branch("weightNames", &m_WeightNames_vector);
    m_weightTree->Fill();
    m_weightTree->Write();
  }
  m_outputFile->Close();
  ATH_MSG_INFO("Output is saved at " << m_outName);
  return sc;
}


StatusCode TqGammaEventSaver::FillOutput (unsigned int iEvent) {


  StatusCode sc = StatusCode::SUCCESS;
  ATH_MSG_DEBUG("Processing Event: "<< iEvent);

  ClearAll();

  ATH_MSG_DEBUG("m_isData: "<< m_isData);

  if(!m_isData) {
    if (     m_inTree.runNumber == 310000) {
      m_runYear = 2018;
      m_lumi = 58450.1;
    }
    else if (m_inTree.runNumber == 300000) {
      m_runYear = 2017;
      m_lumi = 44307.4;
    }
    else if (m_inTree.runNumber == 284500) {
      m_runYear = 2016;
      m_lumi = 36207.66;
    }
    else {
      ATH_MSG_WARNING("unknown runNumber: "<< m_inTree.runNumber);
      m_runYear = 2014;
      m_lumi = 0.0;
    }
  }

  m_nlep_raw = m_inTree.el_pt->size() + m_inTree.mu_pt->size();

  for (std::size_t i = 0; i< m_inTree.el_pt->size(); ++i) {

    Float_t pt = m_inTree.el_pt->at(i);
    Float_t eta = m_inTree.el_eta->at(i);
    Float_t d0sig = m_inTree.el_d0sig->at(i);
    Float_t z0sintheta = m_inTree.el_delta_z0_sintheta->at(i);
    //ATH_MSG_INFO("pt: "<< pt << "\t eta: "<< eta);
    if (pt < 27e3)
      continue;
    if ( fabs(eta) > 2.47 || ( fabs(eta) > 1.37 && fabs(eta) < 1.52 )  )
      continue;
    if (fabs(d0sig) > 5)
      continue;
    if(fabs(z0sintheta) > 0.5)
      continue;


    leptonObj lep;
    lep.pt = pt;
    lep.eta = eta;
    lep.phi = m_inTree.el_phi->at(i);
    lep.e = m_inTree.el_e->at(i);
    lep.d0sig = d0sig;
    lep.z0sintheta = z0sintheta;
    lep.charge = m_inTree.el_charge->at(i);
    lep.id = lep.charge*-11;
    lep.topoetcone20 = m_inTree.el_topoetcone20->at(i);
    lep.ptvarcone30 = m_inTree.el_ptvarcone20->at(i); // ptvarcone20 is filled instead of ptvarcone30

    if(!m_isData) {
      lep.trueType = m_inTree.el_true_type->at(i);
      lep.trueOrigin = m_inTree.el_true_origin->at(i);
      lep.mc_pid = m_inTree.el_mc_pid->at(i);
      lep.mc_charge = m_inTree.el_mc_charge->at(i);
      lep.mc_pt = m_inTree.el_mc_pt->at(i);
      lep.mc_eta = m_inTree.el_mc_eta->at(i);
      lep.mc_phi = m_inTree.el_mc_phi->at(i);
    }

    leptons.push_back(lep);

  } // electrons loop


  for (std::size_t i = 0; i< m_inTree.mu_pt->size(); ++i) {

    Float_t pt = m_inTree.mu_pt->at(i);
    Float_t eta = m_inTree.mu_eta->at(i);
    Float_t d0sig = m_inTree.mu_d0sig->at(i);
    Float_t z0sintheta = m_inTree.mu_delta_z0_sintheta->at(i);

    if (pt < 27e3)
      continue;
    if ( fabs(eta) > 2.5 )
      continue;
    if (fabs(d0sig) > 3)
      continue;
    if(fabs(z0sintheta) > 0.5)
      continue;


    leptonObj lep;
    lep.pt = pt;
    lep.eta = eta;
    lep.phi = m_inTree.mu_phi->at(i);
    lep.e = m_inTree.mu_e->at(i);
    lep.d0sig = d0sig;
    lep.z0sintheta = z0sintheta;
    lep.charge = m_inTree.mu_charge->at(i);
    lep.id = lep.charge*-13;
    lep.topoetcone20 = m_inTree.mu_topoetcone20->at(i);
    lep.ptvarcone30 = m_inTree.mu_ptvarcone30->at(i);

    if(!m_isData) {
      lep.trueType = m_inTree.mu_true_type->at(i);
      lep.trueOrigin = m_inTree.mu_true_origin->at(i);
    }

    leptons.push_back(lep);

  } //muons

  std::sort(leptons.begin(), leptons.end(), greater<leptonObj>());

  m_nlep = leptons.size();


  if ( m_nlep > 0 ) {
    lep1.copy(leptons[0]);
    nu1.pt = m_inTree.met_met;
    nu1.phi = m_inTree.met_phi;
    lep1_fv.SetCoordinates(lep1.pt, lep1.eta, lep1.phi, lep1.e);
    lep1.pz = lep1_fv.Pz();
    GetNeutrinoPz();
    m_transMass = sqrt(2*lep1.pt*m_inTree.met_met*( 1 - cos(lep1.phi-m_inTree.met_phi) ));
  }


  if( m_nlep > 1 ) {
    lep2.copy(leptons[1]);
    lep2_fv.SetCoordinates(lep2.pt, lep2.eta, lep2.phi, lep2.e);
    lep2.pz = lep2_fv.Pz();
  }

  m_nph_raw = m_inTree.ph_pt->size();
  m_weight_photonSFown = 1.0;
  m_weight_photonSFown_ID_up = 1.0;
  m_weight_photonSFown_ID_down = 1.0;
  m_weight_photonSFown_ISO_up = 1.0;
  m_weight_photonSFown_ISO_down = 1.0;

  for (std::size_t i = 0; i< m_inTree.ph_pt->size(); ++i) {

    Float_t pt = m_inTree.ph_pt->at(i);
    Float_t eta = m_inTree.ph_eta->at(i);
    int isoFCT = ((int) m_inTree.ph_isoFCT->at(i) );
    //    float topoetcone40 = m_inTree.ph_topoetcone40->at(i);
    //float ptcone20 = m_inTree.ph_ptcone20->at(i);
    //int isoFCT = (int) ( (topoetcone40 < (0.022*pt + 2450)) && (ptcone20 < 0.05*pt) );

    int isTight = ((int) m_inTree.ph_isTight->at(i) ) ;

    if (pt < 20e3)
      continue;
    if ( fabs(eta) > 2.37 || ( fabs(eta) > 1.37 && fabs(eta) < 1.52 )  )
      continue;

    if ( !(isoFCT && isTight) )
      continue;

    photonObj ph;

    ph.pt = pt;
    ph.eta = eta;
    ph.phi = m_inTree.ph_phi->at(i);
    ph.e = m_inTree.ph_e->at(i);
    ph.topoetcone20 = m_inTree.ph_topoetcone20->at(i);
    ph.topoetcone30 = m_inTree.ph_topoetcone30->at(i);
    ph.topoetcone40 = m_inTree.ph_topoetcone40->at(i);
    ph.ptcone20 = m_inTree.ph_ptcone20->at(i);
    ph.ptcone30 = m_inTree.ph_ptcone30->at(i);
    ph.ptcone40 = m_inTree.ph_ptcone40->at(i);
    ph.isoFCT = ((int) m_inTree.ph_isoFCT->at(i) );
    ph.isoFCL = ((int) m_inTree.ph_isoFCL->at(i) );
    ph.isTight = ((int) m_inTree.ph_isTight->at(i) ) ;
    ph.isLoose = ((int) m_inTree.ph_isLoose->at(i) );
    ph.author = m_inTree.ph_author->at(i);
    ph.conversionType =m_inTree.ph_conversionType->at(i);
    ph.caloEta =m_inTree.ph_caloEta->at(i);
    ph.isEM_Tight =m_inTree.ph_isEM_Tight->at(i);
    ph.truthType =m_inTree.ph_truthType->at(i);
    ph.truthOrigin =m_inTree.ph_truthOrigin->at(i);
    ph.sf_id = m_inTree.ph_SF_ID->at(i);
    ph.sf_iso = m_inTree.ph_SF_iso->at(i);
    m_weight_photonSFown *= m_inTree.ph_SF_ID->at(i)*m_inTree.ph_SF_iso->at(i);
    m_weight_photonSFown_ID_up *= (m_inTree.ph_SF_ID->at(i)+m_inTree.ph_SF_ID_unc->at(i))*m_inTree.ph_SF_iso->at(i);
    m_weight_photonSFown_ID_down *= (m_inTree.ph_SF_ID->at(i)-m_inTree.ph_SF_ID_unc->at(i))*m_inTree.ph_SF_iso->at(i);
    m_weight_photonSFown_ISO_up *= m_inTree.ph_SF_ID->at(i)*(m_inTree.ph_SF_iso->at(i)+m_inTree.ph_SF_iso_unc->at(i));
    m_weight_photonSFown_ISO_down *= m_inTree.ph_SF_ID->at(i)*(m_inTree.ph_SF_iso->at(i)-m_inTree.ph_SF_iso_unc->at(i));

    ph.mc_pid = m_inTree.ph_mc_pid->at(i);
    ph.mc_pt = m_inTree.ph_mc_pt->at(i);
    ph.mc_eta = m_inTree.ph_mc_eta->at(i);
    ph.mc_phi = m_inTree.ph_mc_phi->at(i);
    ph.mcel_dr = m_inTree.ph_mcel_dr->at(i);
    ph.mcel_pt = m_inTree.ph_mcel_pt->at(i);
    ph.mcel_eta = m_inTree.ph_mcel_eta->at(i);
    ph.mcel_phi = m_inTree.ph_mcel_phi->at(i);
    ph.mc_production = m_inTree.ph_mc_production->at(i);
    ph.type = GetPhotonType(i);
    photons.push_back(ph);
  }

  std::sort(photons.begin(), photons.end(), greater<photonObj>());

  m_nph = photons.size();
  if( m_nph > 0 ) {
    photon1.copy(photons[0]);
    ResetHFakeWeightMap();
    ResetEFakeWeightMap();
    m_efakeweight = 1.0;
    m_efakeweightErr = 0.0;
    m_hfakeweight = 1.0;
    m_hfakeweightErr = 0.0;

    if (photon1.type == 4) {
      GetHadronFakeWeight(photon1.eta, photon1.conversionType, photon1.pt);
    }
    else if (photon1.type > 0) {
      GetElectronFakeWeight(photon1.eta, photon1.conversionType);
    }

    m_efakeweight_up = (m_efakeweight+m_efakeweightErr);
    m_efakeweight_down = (m_efakeweight-m_efakeweightErr);
    m_hfakeweight_up = (m_hfakeweight+m_hfakeweightErr);
    m_hfakeweight_down = (m_hfakeweight-m_hfakeweightErr);

    photon1_fv.SetCoordinates(photon1.pt, photon1.eta, photon1.phi, photon1.e);
    if(m_nlep > 0 ) {
      m_transMassWph = GetTransMass3D(lep1.pt, lep1.phi, m_inTree.met_met, m_inTree.met_phi, photon1.pt, photon1.phi);
    }

  }


  m_njets_raw = m_inTree.jet_pt->size();

  for(std::size_t i = 0; i< m_inTree.jet_pt->size(); ++i) {
    Float_t pt = m_inTree.jet_pt->at(i);
    Float_t eta = m_inTree.jet_eta->at(i);
    Float_t jvt = m_inTree.jet_jvt->at(i);

    m_HT += pt;
    if (pt < 25e3)
      continue;

    jetObj jet;
    jet.pt = pt;
    jet.eta = eta;
    jet.phi = m_inTree.jet_phi->at(i);
    jet.e = m_inTree.jet_e->at(i);
    jet.DL1r = m_inTree.jet_DL1r->at(i);
    jet.jvt = jvt;
    jet.passfjvt = ((int) m_inTree.jet_passfjvt->at(i) );
    jet.tagWeightBin_DL1r_Continuous = m_inTree.jet_tagWeightBin_DL1r_Continuous->at(i);
    /* int bin = 0;
    if (jet.DL1r > 4.565)
      bin = 5;
    else if (jet.DL1r > 3.245)
      bin = 4;
    else if (jet.DL1r > 2.195)
      bin = 3;
    else if (jet.DL1r > 0.665)
      bin = 2;
    else
      bin = 1;

      jet.tagWeightBin_DL1r_Continuous = bin; */

    if(!m_isData)  jet.truthflav = m_inTree.jet_truthflav->at(i);

    jet.isBjet = ( (int) m_inTree.jet_isbtagged_DL1r_70->at(i) ) ;

    if (fabs(eta) < 2.5 ) {

      jets.push_back(jet);

      if(( (int) m_inTree.jet_isbtagged_DL1r_60->at(i) ))
	m_nbjets_DL1r_60 += 1;

      if(( (int) m_inTree.jet_isbtagged_DL1r_70->at(i) )) {
	m_nbjets_DL1r_70 += 1;
	bjets.push_back(jet);
      }

      if(((int) m_inTree.jet_isbtagged_DL1r_77->at(i) ))
	m_nbjets_DL1r_77 += 1;

      if(((int)m_inTree.jet_isbtagged_DL1r_85->at(i))) {
	m_nbjets_DL1r_85 += 1;
	loosebjets.push_back(jet);
      }

    } // end of eta < 2.5

    else if ( jet.passfjvt ) {
      fjets.push_back(jet);
    }

  }

  m_njets = jets.size();
  m_nfjets = fjets.size();

  std::sort(jets.begin(),jets.end(), greater<jetObj>());


  if (m_njets > 0) {
    ljet.copy(jets[0]);
  }

  if (m_njets > 1) {
    sljet.copy(jets[1]);
  }

  m_nbjets = bjets.size();

  std::sort(bjets.begin(), bjets.end(), greater<jetObj>());


  if (m_nbjets > 0) {
    lbjet.copy(bjets[0]);
    b1_fv.SetCoordinates(lbjet.pt, lbjet.eta, lbjet.phi, lbjet.e);
    if(m_nlep>0) m_transMassWb = GetTransMass3D(lep1.pt, lep1.phi, m_inTree.met_met, m_inTree.met_phi, lbjet.pt, lbjet.phi);
  }

  std::sort(loosebjets.begin(), loosebjets.end(), greater<jetObj>());

  if ((m_nbjets == 0) && (m_nbjets_DL1r_85 > 0)) {
    lbjet.copy(loosebjets[0]);
    b1_fv.SetCoordinates(lbjet.pt, lbjet.eta, lbjet.phi, lbjet.e);
    if(m_nlep>0) m_transMassWb = GetTransMass3D(lep1.pt, lep1.phi, m_inTree.met_met, m_inTree.met_phi, lbjet.pt, lbjet.phi);

  }

  if (m_nbjets > 1) {
    slbjet.copy(bjets[1]);
  }

  std::sort(fjets.begin(), fjets.end(), greater<jetObj>());
  if (m_nfjets > 0) {
    fjet.copy(fjets[0]);
    fj_fv.SetCoordinates(fjet.pt, fjet.eta, fjet.phi, fjet.e);

    if ( fjet.pt >= ljet.pt ) {
      m_fjet_flag = 1;
    }
    else if ( ljet.isBjet and fjet.pt >= sljet.pt ) {
      m_fjet_flag = 1;
    }

  }


  GetTopQuarkInfo();

  if(m_doFake && lep1.id != 0) GetLFakeWeight();
  m_outputFile->cd();
/*
  if( m_nlep >0 && m_nph>0){
      if(!m_doSys && m_nbjets_DL1r_85>0 )
        m_outTree->Fill();
      else if(m_nbjets_DL1r_85>0)
        m_outTree->Fill();
  }*/


  if (m_doSys) {
    if (m_nlep >0 && m_nph>0 && m_nbjets_DL1r_85>0)
      m_outTree->Fill();
  }
  else {
    if (m_nlep >0 && m_nph>0 && m_nbjets_DL1r_85>0) // should be a flag in future
    //if (m_nbjets_DL1r_85>0) // should be a flag in future

      m_outTree->Fill();
  }

  return sc;


}



unsigned int  TqGammaEventSaver::GetPhotonType (unsigned int i) {

  if(m_isData) {
    return 0;
  }

  Float_t max_deltaR_recoPhoton_truthElectron = 0.05;
  vector <int> list_hadronic_fake_truth_origin{23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 42};
  int hadronic_truth_type = 17;
  int pdgID_electron = 11;
  int pdgID_photon   = 22;
  bool in_list_of_had_fakes = false;

  if(abs(m_inTree.ph_mc_pid->at(i)) == pdgID_electron) return 1;

  else if(abs(m_inTree.ph_mc_pid->at(i)) == pdgID_photon && fabs(m_inTree.ph_mcel_dr->at(i)) >= 0 && fabs(m_inTree.ph_mcel_dr->at(i)) < max_deltaR_recoPhoton_truthElectron) {

    if(fabs(m_inTree.ph_pt->at(i) - m_inTree.ph_mc_pt->at(i)) /m_inTree.ph_pt->at(i) < 0.1) return 3;

    else return 2;

  }


  for (unsigned int hf(0); hf < list_hadronic_fake_truth_origin.size(); hf++){

    if(m_inTree.ph_truthOrigin->at(i) == list_hadronic_fake_truth_origin.at(hf)) {
      in_list_of_had_fakes = true;
      break;
    }

  }


  if(in_list_of_had_fakes || m_inTree.ph_truthType->at(i) ==hadronic_truth_type) return 4;

  else return 0;

}

Float_t TqGammaEventSaver::GetTransMass3D(Float_t lep1_pt, Float_t lep1_phi, Float_t met_met, Float_t met_phi, Float_t ph_pt, Float_t ph_phi){
  Float_t tmWph2 = 2*lep1_pt*met_met*( 1 - cos(lep1_phi-met_phi) );
  tmWph2 += 2*lep1_pt*ph_pt*( 1 - cos(lep1_phi-ph_phi) );
  tmWph2 += 2*met_met*ph_pt*( 1 - cos(met_phi-ph_phi) );
  return sqrt(tmWph2);
}

void TqGammaEventSaver::GetNeutrinoPz() {
  Float_t m_W = 80.379e3;

  Float_t delta_phi = lep1.phi - nu1.phi ;
  Float_t mu = pow(m_W, 2)/2 + cos(delta_phi)*lep1.pt*nu1.pt;
  Float_t disc = ( pow(mu, 2) * pow(lep1.pz, 2) / pow(lep1.pt, 4) ) - (( pow(lep1.e, 2) * pow(nu1.pt, 2) - pow(mu, 2) ) / pow(lep1.pt, 2) );

  if (disc>=0) {
    Float_t nu_pz_A = mu*lep1.pz / pow(lep1.pt, 2) + sqrt(disc);
    Float_t nu_pz_B = mu*lep1.pz / pow(lep1.pt, 2) - sqrt(disc);

    if (abs(nu_pz_A) < abs(nu_pz_B))
      nu1.pz = nu_pz_A;
    else
      nu1.pz = nu_pz_B;
  }

  else if (disc<0) {
    Float_t scaled_mu = sqrt( pow(lep1.pt, 2) * pow(lep1.e, 2) * pow(nu1.pt, 2) )/ ( pow(lep1.pz, 2) + pow(lep1.pt, 2) );
    ATH_MSG_VERBOSE("scaled_mu: "<< scaled_mu);
    if( pow(m_W,2) > (2*1.05*lep1.pt*nu1.pt*(1-cos(delta_phi))) ) {
      ATH_MSG_WARNING("Error in Neutrino Reconsturction: "<< pow(m_W, 2)<<"\t"<<(2*lep1.pt*nu1.pt*(1-cos(delta_phi))));
     }
    nu1.pz = (lep1.pz*nu1.pt)/lep1.pt;
  }


  nu1_fv.SetPxPyPzE(nu1.pt*cos(nu1.phi), nu1.pt*sin(nu1.phi), nu1.pz, sqrt( pow(nu1.pt,2)+ pow(nu1.pz, 2) ) );
  nu1.pt = nu1_fv.Pt();
  nu1.eta = nu1_fv.Eta();
  nu1.e = nu1_fv.E();

}


void TqGammaEventSaver::GetTopQuarkInfo() {

  using namespace ROOT::Math::VectorUtil;

  //if (m_nlep ==0 || m_nph==0 || m_nbjets ==0) return;

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > W1_fv, top1_fv, phintop_fv;
  W1_fv = lep1_fv + nu1_fv;
  //m_mT = W1_fv.Mt();
  top1_fv = W1_fv + b1_fv;
  phintop_fv = boost(photon1_fv, top1_fv.BoostToCM());
  m_phintop_pt = phintop_fv.Pt();
  m_phintop_eta = phintop_fv.Eta();
  m_phintop_phi = phintop_fv.Phi();
  m_phintop_e = phintop_fv.E();

  Wbsn.setValues(lep1_fv, nu1_fv);
  topq.setValues(W1_fv, b1_fv);


  topph.setValues(top1_fv, photon1_fv);
  topfj.setValues(top1_fv, fj_fv);
  Wph.setValues(W1_fv, photon1_fv);
  lepph.setValues(lep1_fv, photon1_fv);
  fjph.setValues(fj_fv, photon1_fv);
  bph.setValues(b1_fv, photon1_fv);
  nuph.setValues(nu1_fv, photon1_fv);
  bnu.setValues(b1_fv, nu1_fv);
  blep.setValues(b1_fv, lep1_fv);
  bfj.setValues(b1_fv, fj_fv);
  lfj.setValues(lep1_fv, fj_fv);
  nufj.setValues(nu1_fv, fj_fv);
  lep1lep2.setValues(lep1_fv, lep2_fv);

}

void TqGammaEventSaver::GetElectronFakeWeight(Float_t pheta, int convtype) {
  std::string fname = "";
  if(fabs(pheta) < 0.3)
    fname = "eta03";
  else if (fabs(pheta)<0.6)
    fname = "eta06";
  else if (fabs(pheta)<1.0)
    fname = "eta10";
  else if (fabs(pheta)<1.37)
    fname = "eta137";
  else if (fabs(pheta)<1.81)
    fname = "eta181";
  else if (fabs(pheta)<2.37)
    fname = "eta237";
  else
    ATH_MSG_WARNING("Bad Photon found");

  fname += "_conv"+std::to_string(convtype);
  //m_efakeweight = m_eFakeWeightMap[fname][0];
  //m_efakeweightErr = m_eFakeWeightMap[fname][1];
  //cout << "efakeWeight_"+fname << endl;
  m_efakeweight = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0);
  m_efakeweightErr = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(3);

  m_eFakeWeightOutputMap["efakeWeight_"+fname] = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0);
  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_STAT_UP"]   = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)+m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(1);
  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_STAT_DOWN"] = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)-m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(1);

  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_SYST_UP"]   = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)+m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(2);
  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_SYST_DOWN"] = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)-m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(2);

  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_TOT_UP"]    = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)+m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(3);
  m_eFakeWeightOutputMap["efakeWeight_"+fname+"_TOT_DOWN"]  = m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(0)-m_eFakeWeightMapFromFile["efakeWeight_"+fname].at(3);

}

void TqGammaEventSaver::GetHadronFakeWeight(Float_t pheta, int convtype, Float_t phpt) {

  std::string fname = "";
  if(convtype==0)
    fname = "unconv_";
  else if(convtype>0)
    fname = "conv_";
  else
    ATH_MSG_WARNING("Bad Photon found");
  std::string hfname = fname;
  if(fabs(pheta) < 0.3){
    fname += "etaA";
    hfname += "etaBin03";
  }
  else if (fabs(pheta)<0.6){
    fname += "etaB";
    hfname += "etaBin06";
  }
  else if (fabs(pheta)<1.0){
    fname += "etaC";
    hfname += "etaBin100";
  }
  else if (fabs(pheta)<1.37){
    fname += "etaD";
    hfname += "etaBin137";
  }
  else if (fabs(pheta)<1.81){
    fname += "etaE";
    if(convtype==0) hfname += "etaBin237";
    else hfname += "etaBin181";

  }
  else if (fabs(pheta)<2.37){
    fname += "etaF";
    hfname += "etaBin237";
  }
  else
    ATH_MSG_WARNING("Bad Photon found");

  if(phpt < 40e3){
    fname += "_pt0";
    hfname += "_20";
  }
  else{
    fname += "_pt1";
    hfname += "_40";
  }

  //m_hfakeweight = m_hFakeWeightMap[fname][0];
  //m_hfakeweightErr = m_hFakeWeightMap[fname][1];

  m_hfakeweight =m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0);
  m_hfakeweightErr =m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(3);

  m_hFakeWeightOutputMap["hfakeWeight_"+hfname] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0);
  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_STAT_UP"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)+m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(1);
  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_STAT_DOWN"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)-m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(1);

  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_SYST_UP"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)+m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(2);
  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_SYST_DOWN"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)-m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(2);

  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_TOT_UP"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)+m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(3);
  m_hFakeWeightOutputMap["hfakeWeight_"+hfname+"_TOT_DOWN"] = m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(0)-m_hFakeWeightMapFromFile["hfakeWeight_"+hfname].at(3);

}

void TqGammaEventSaver::ResetHFakeWeightMap(){
  for(auto& hfakeEnt:m_hFakeWeightOutputMap){
    m_hFakeWeightOutputMap[hfakeEnt.first] = 1.0;
  }
}

void TqGammaEventSaver::ResetEFakeWeightMap(){
  for(auto& efakeEnt:m_eFakeWeightOutputMap){
    m_eFakeWeightOutputMap[efakeEnt.first] = 1.0;
  }
}

void TqGammaEventSaver::InitHFakeWeightFromFile(){

  ifstream SFFile("/net/e4-nfs-home.e4.physik.tu-dortmund.de/home/bwendland/SgTopGamma/NewRepo/tqGammaNtupleSkimmer/source/tqGammaNtupleSkimmer/input/SFs_final.csv");
  std::string line;
  if (SFFile.is_open()){
    while(getline(SFFile, line)){
      TString cur_line = TString(line);
      if(cur_line.Contains("SF")) continue;
      TObjArray *tx = cur_line.Tokenize(",");
      float SF_nom = ((TObjString *)(tx->At(1)))->String().Atof();
      float SF_statUnc = ((TObjString *)(tx->At(2)))->String().Atof();
      float SF_systUnc = ((TObjString *)(tx->At(3)))->String().Atof();
      float SF_totUnc = ((TObjString *)(tx->At(4)))->String().Atof();
      int pt = ((TObjString *)(tx->At(10)))->String().Atoi()/1000;
      float eta = ((TObjString *)(tx->At(9)))->String().Atof();
      TString convStr = ((TObjString *)(tx->At(11)))->String();

      string etaBin = "";
      if(eta < 0.6) etaBin = "_etaBin03";
      else if(eta < 1.0) etaBin = "_etaBin06";
      else if(eta < 1.37) etaBin = "_etaBin100";
      else if(eta < 1.80) etaBin = "_etaBin137";
      else if(eta < 2.30) etaBin = "_etaBin181";
      else etaBin = "_etaBin237";

      //cout << eta << " " << etaBin << endl;
      vector<float> curVec = {SF_nom, SF_statUnc, SF_systUnc, SF_totUnc};
      m_hFakeWeightMapFromFile.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt), {SF_nom, SF_statUnc, SF_systUnc, SF_totUnc}});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt), 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_STAT_UP", 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_SYST_UP", 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_TOT_UP", 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_STAT_DOWN", 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_SYST_DOWN", 1.0});
      m_hFakeWeightOutputMap.insert({(TString("hfakeWeight_")+convStr).Data()+etaBin+"_"+to_string(pt)+"_TOT_DOWN", 1.0});

      //cout << "SF for "<<convStr <<"erted "<<eta << " "<<pt << ": " << SF_nom << " +/- "<<SF_statUnc << " +/- " << SF_systUnc <<endl;
    }
    SFFile.close();
  }
}

void TqGammaEventSaver::InitEFakeWeightFromFile(){

  ifstream SFFile("/net/e4-nfs-home.e4.physik.tu-dortmund.de/home/bwendland/SgTopGamma/NewRepo/tqGammaNtupleSkimmer/source/tqGammaNtupleSkimmer/input/SFs_efake.txt");
  std::string line;
  if (SFFile.is_open()){
    while(getline(SFFile, line)){
      TString cur_line = TString(line);
      if(cur_line.Contains("SF")) continue;
      TObjArray *tx = cur_line.Tokenize(" ");
      TString binStr = ((TObjString *)(tx->At(0)))->String();
      float SF_nom = ((TObjString *)(tx->At(1)))->String().Atof();
      float SF_statUnc = ((TObjString *)(tx->At(2)))->String().Atof();
      float SF_systUnc = ((TObjString *)(tx->At(3)))->String().Atof();
      float SF_totUnc = ((TObjString *)(tx->At(4)))->String().Atof();
      //cout << (TString("efakeWeight_")+binStr).Data() << endl;
      vector<float> curVec = {SF_nom, SF_statUnc, SF_systUnc, SF_totUnc};
      m_eFakeWeightMapFromFile.insert({(TString("efakeWeight_")+binStr).Data(), {SF_nom, SF_statUnc, SF_systUnc, SF_totUnc}});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_STAT_UP")).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_STAT_DOWN")).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_SYST_UP")).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_SYST_DOWN")).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_TOT_UP")).Data(), 1.0});
      m_eFakeWeightOutputMap.insert({(TString("efakeWeight_")+binStr+TString("_TOT_DOWN")).Data(), 1.0});

      //cout << "SF for "<<convStr <<"erted "<<eta << " "<<pt << ": " << SF_nom << " +/- "<<SF_statUnc << " +/- " << SF_systUnc <<endl;
    }
    SFFile.close();
  }
}



void TqGammaEventSaver::ClearAll() {

  leptons.clear();
  photons.clear();
  jets.clear();
  bjets.clear();
  loosebjets.clear();
  fjets.clear();

  lep1.reset();
  lep2.reset();
  photon1.reset();
  ljet.reset();
  sljet.reset();
  lbjet.reset();
  slbjet.reset();
  fjet.reset();

  m_transMass = 0.0;
  m_transMassWph = 0.0;
  m_transMassWb = 0.0;
  m_mT = 0.0;
  m_HT = 0.0;
  m_efakeweight = 1.0;
  m_efakeweightErr = 0.0;
  m_hfakeweight = 1.0;
  m_hfakeweightErr = 0.0;
  m_nlep = 0;
  m_nlep_raw = 0;

  m_nph = 0;
  m_nph_raw = 0;

  m_njets = 0;
  m_phintop_pt = 0.0;
  m_phintop_eta = 0.0;
  m_phintop_phi = 0.0;
  m_phintop_e = 0.0;
  m_njets_raw = 0;
  m_nbjets = 0;
  m_nfjets = 0;
  m_nbjets_DL1r_60 = 0;
  m_nbjets_DL1r_70 = 0;
  m_nbjets_DL1r_77 = 0;
  m_nbjets_DL1r_85 = 0;
  m_fjet_flag = 0;

  lep1_fv.SetCoordinates(0,0,0,0);
  lep2_fv.SetCoordinates(0,0,0,0);
  nu1_fv.SetCoordinates(0,0,0,0);
  photon1_fv.SetCoordinates(0,0,0,0);
  b1_fv.SetCoordinates(0,0,0,0);
  fj_fv.SetCoordinates(0,0,0,0);

  for ( auto & iRes : resList ) {
    iRes->reset();
  }

}
