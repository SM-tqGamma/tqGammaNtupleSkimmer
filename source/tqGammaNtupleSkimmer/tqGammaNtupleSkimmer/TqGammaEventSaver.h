#ifndef TQGAMMAEVENTSAVER_H
#define TQGAMMAEVENTSAVER_H

/******************************************
 *@Package: TqGammaNtupleSkimming
 *@Class:   TqGammaEventSaver
 *@Author:  Harish Potti
 *
 *****************************************/


#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <memory>
#include <list>
#include "CxxUtils/make_unique.h"


#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH1D.h>
#include <TTree.h>
#include <TObject.h>
#include <TSystem.h>
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "Math/GenVector/VectorUtil.h"

#include "AsgTools/MessageCheck.h"
#include "AsgTools/AsgMessaging.h"


#include "tqGammaNtupleSkimmer/TqGammaNtuple.h"
#include "tqGammaNtupleSkimmer/ParticleUtils.h"

//class leptonObj;
class TqGammaEventSaver: public asg::AsgMessaging {

 public:

  TqGammaEventSaver(const std::string& name = "TqGammaEventSaver");
  ~TqGammaEventSaver();

  StatusCode initialize(const TString& configFile, const std::string& m_samplePath, const TString& outName, bool isData, bool doFake, bool doSys, const TString& sysName);

  StatusCode finalize();

  StatusCode executeEventLoop();

 private:

  std::string m_samplePath = "";
  bool m_isData = false;
  bool m_doFake = false;
  std::unique_ptr<std::vector<std::string>> m_treeVec;
  bool m_doSys = false;
  TString m_sysName = "";


  TFile* m_outputFile;
  TTree* m_outTree;
  TTree* m_weightTree;
  TString m_dsid = "";
  TString m_outName = "";

  TString m_lfakepath ="/net/e4-nfs-home.e4.physik.tu-dortmund.de/home/bwendland/SgTopGamma/NewRepo/tqGammaNtupleSkimmer/source/tqGammaNtupleSkimmer/input/lfakemaps/pt_mt_2D_";
  std::map<TString, TH2F*> m_lfakeweight_maps;

  TqGammaNtuple m_inTree;

  std::map<std::string, TH1F>  MCLumiHistMap;

  StatusCode GetSumWeightsXSec();
  void SetWeightMaps();
  void DefineOutputTree(bool isNominal);
  StatusCode FillOutput(unsigned int iEvent);
  void ClearAll();
  unsigned int GetPhotonType(unsigned int i);
  Float_t GetTransMass3D(Float_t, Float_t, Float_t, Float_t, Float_t, Float_t);
  void GetLFakeWeight();
  void GetNeutrinoPz();
  void GetTopQuarkInfo();
  void GetElectronFakeWeight(Float_t, int );
  void GetHadronFakeWeight(Float_t, int, Float_t);
  void InitHFakeWeightFromFile();
  void InitEFakeWeightFromFile();
  void ResetHFakeWeightMap();
  void ResetEFakeWeightMap();


  Float_t m_xsec = 0.0;
  Float_t m_sumWeights = 0.0;
  std::vector<Float_t> m_sumWeights_vector{};
  std::vector<std::string> m_WeightNames_vector{};
  ULong64_t m_tot_events_processed = 0;
  Float_t m_scale_nom = 0.0;
  std::map<std::string, std::vector<Float_t>> m_hFakeWeightMap;
  std::map<std::string, std::vector<float>> m_hFakeWeightMapFromFile;
  std::map<std::string, float> m_hFakeWeightOutputMap;
  std::map<std::string, std::vector<Float_t>> m_eFakeWeightMap;
  std::map<std::string, std::vector<Float_t>> m_eFakeWeightMapFromFile;
  std::map<std::string, float> m_eFakeWeightOutputMap;
  Int_t m_runYear = 2014;
  Float_t m_lumi = 0.0;
  Float_t m_transMass = 0.0;
  Float_t m_transMassWph =0.0;
  Float_t m_transMassWb = 0.0;
  Float_t m_mT = 0.0;
  Float_t m_HT = 0.0;
  Float_t m_efakeweight = 1.0;
  Float_t m_efakeweightErr = 0.0;
  Float_t m_efakeweight_up = 1.0;
  Float_t m_efakeweight_down = 1.0;

  Float_t m_hfakeweight = 1.0;
  Float_t m_hfakeweightErr = 0.0;
  Float_t m_hfakeweight_up = 1.0;
  Float_t m_hfakeweight_down = 1.0;

  Float_t m_weight_photonSFown = 1.0;
  Float_t m_weight_photonSFown_ID_up = 1.0;
  Float_t m_weight_photonSFown_ID_down = 1.0;
  Float_t m_weight_photonSFown_ISO_up = 1.0;
  Float_t m_weight_photonSFown_ISO_down = 1.0;

  UInt_t m_nlep = 0;
  UInt_t m_nlep_raw = 0;

  UInt_t m_nph = 0;
  UInt_t m_nph_raw = 0;
  Float_t m_phintop_pt = 0.0;
  Float_t m_phintop_eta = 0.0;
  Float_t m_phintop_phi = 0.0;
  Float_t m_phintop_e = 0.0;

  UInt_t m_njets = 0;
  UInt_t m_njets_raw = 0;
  UInt_t m_nbjets = 0;
  UInt_t m_nfjets = 0;
  UInt_t m_nbjets_DL1r_60 = 0;
  UInt_t m_nbjets_DL1r_70 = 0;
  UInt_t m_nbjets_DL1r_77 = 0;
  UInt_t m_nbjets_DL1r_85 = 0;
  UInt_t m_fjet_flag = 0;

  Float_t m_lfakeweight = 1.0;
  Float_t m_lfakeweight_wjetsnorm = 0.0;
  UInt_t m_lep1_isTight = 0;

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > lep1_fv;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > lep2_fv;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > nu1_fv;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > photon1_fv;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > b1_fv;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > fj_fv;

  std::vector<leptonObj> leptons;
  std::vector<photonObj> photons;
  std::vector<jetObj>  jets;
  std::vector<jetObj> bjets, loosebjets;
  std::vector<jetObj> fjets;

  leptonObj lep1, lep2, nu1;
  photonObj photon1;
  jetObj  ljet, sljet, lbjet, slbjet, fjet;
  resObj Wbsn, topq;
  resObj topph, topfj, Wph, lepph, fjph, bph, nuph;
  resObj bnu, blep, bfj;
  resObj lfj, nufj;
  resObj lep1lep2;

  std::list<resObj*> resList = {&Wbsn, &topq, &topph, &topfj, &Wph, &lepph, &fjph,
				&bph, &nuph, &bnu, &blep, &bfj, &lfj, &nufj, &lep1lep2};


};





#endif
