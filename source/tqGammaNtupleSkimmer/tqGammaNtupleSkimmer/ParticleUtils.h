#ifndef PARTICLEUTILS_H
#define PARTICLEUTILS_H

#include "Math/GenVector/GenVector_exception.h"

class leptonObj {

 public:
  Float_t pt, eta, phi, e, pz;
  Float_t d0sig, z0sintheta,topoetcone20,ptvarcone30;
  Int_t id, charge,trueType,trueOrigin;
  Int_t mc_pid;
  Float_t mc_charge, mc_pt, mc_eta, mc_phi;
  std::string name;

 leptonObj():
  pt(-1.0), eta(-999), phi(-999), e(-1.0), pz(0.0), 
    d0sig(-999), z0sintheta(-999), topoetcone20(-999), ptvarcone30(-999),
    id(0),charge(-999),trueType(-1),trueOrigin(-1),
    mc_pid(0), mc_charge(-999), mc_pt(-0.1), mc_eta(-999), mc_phi(-999),
    name("testLep")
    { 
        
    }

  ~leptonObj(){}
  bool operator > (const leptonObj& lep) const {
    return (pt > lep.pt);
  }

  void print() {
    std::cout<<"pt: "<< pt << "\t eta: "<< eta;
  }

  void copy(leptonObj & lep) {
    pt = lep.pt;
    eta = lep.eta;
    phi = lep.phi;
    e = lep.e;
    pz = lep.pz;
    d0sig = lep.d0sig;
    z0sintheta = lep.z0sintheta;
    topoetcone20 = lep.topoetcone20;
    ptvarcone30 = lep.ptvarcone30;
    id = lep.id;
    charge = lep.charge;
    trueType = lep.trueType;
    trueOrigin = lep.trueOrigin;
    mc_pid = lep.mc_pid;
    mc_charge = lep.mc_charge;
    mc_pt = lep.mc_pt;
    mc_eta = lep.mc_eta;
    mc_phi = lep.mc_phi;
  }

  void reset() {
    pt = -1.0; eta = -999; phi= -999; e = -1.0; pz = 0.0; 
    d0sig = -999;  z0sintheta = -999; topoetcone20 = -999; ptvarcone30 = -999;
    id = 0; charge = -999; trueType = -1; trueOrigin = -1;
    mc_pid=0; mc_charge=-999; mc_pt=-0.1; mc_eta = -999; mc_phi = -999;
  }

  /*  void setValues (ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> & fv) {
    pt = fv.Pt();
    eta = fv.Eta();
    phi = fv.Phi();
    e = fv.E();
    pz = fv.Pz();
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> zero(0,0,0,0);
    m = ROOT::Math::VectorUtil::InvariantMass(fv, zero);
    } */



}; // end lepObj



class photonObj {
 public:
  Float_t pt, eta, phi, e;
  Float_t topoetcone20, topoetcone30, topoetcone40;
  Float_t ptcone20, ptcone30, ptcone40, caloEta;
  UInt_t isoFCT,isoFCL,isTight,isLoose,author, isEM_Tight;
  Int_t conversionType, truthType, truthOrigin, type;
  Float_t sf_id, sf_iso;
  Int_t mc_pid;
  Float_t mc_pt, mc_eta, mc_phi;
  Float_t mcel_dr, mcel_pt, mcel_eta, mcel_phi, mc_production;


 photonObj():
  pt(-1.0), eta(-999), phi(-999), e(-1.0), 
    topoetcone20(-999),topoetcone30(-999),topoetcone40(-999),
    ptcone20(-999), ptcone30(-999), ptcone40(-999),caloEta(-999),
    isoFCT(0),isoFCL(0),isTight(0),isLoose(0),author(0), isEM_Tight(0),
    conversionType(-1), truthType(-1), truthOrigin(-1), type(-1),
    sf_id(1.0),sf_iso(1.0),
    mc_pid(0),mc_pt(0.0), mc_eta(-999), mc_phi(-999),
    mcel_dr(-0.1),mcel_pt(-0.1),mcel_eta(-999),mcel_phi(-999),mc_production(-999)
    {}

  ~photonObj() {}

  bool operator > (const photonObj& ph) const
  {
    return (pt > ph.pt);
  }

  void copy(photonObj &ph) {
    pt = ph.pt;  
    eta = ph.eta;  
    phi = ph.phi;  
    e = ph.e;
    topoetcone20 = ph.topoetcone20; 
    topoetcone30 = ph.topoetcone30; 
    topoetcone40 = ph.topoetcone40;
    ptcone20 = ph.ptcone20;  
    ptcone30 = ph.ptcone30;  
    ptcone40 = ph.ptcone40; 
    caloEta = ph.caloEta;
    isoFCT = ph.isoFCT; 
    isoFCL = ph.isoFCL; 
    isTight = ph.isTight; 
    isLoose = ph.isLoose; 
    author = ph.author;  
    isEM_Tight = ph.isEM_Tight;
    conversionType = ph.conversionType;  
    truthType = ph.truthType;  
    truthOrigin = ph.truthOrigin;  
    type = ph.type;
    sf_id = ph.sf_id; 
    sf_iso = ph.sf_iso;
    mc_pid = ph.mc_pid;
    mc_pt = ph.mc_pt;
    mc_eta = ph.mc_eta;
    mc_phi = ph.mc_phi;
    mcel_dr = ph.mcel_dr;
    mcel_pt = ph.mcel_pt;
    mcel_eta = ph.mcel_eta;
    mcel_phi = ph.mcel_phi;
    mc_production = ph.mc_production;
  }
  
  void reset() {
    pt = -1.0;  eta = -999;  phi = -999;  e = -1.0; 
    topoetcone20 = -999; topoetcone30 = -999; topoetcone40 = -999; 
    ptcone20 = -999;  ptcone30 = -999;  ptcone40 = -999; caloEta = -999; 
    isoFCT = 0; isoFCL = 0; isTight = 0; isLoose = 0; author = 0;  isEM_Tight = 0; 
    conversionType = -1;  truthType = -1;  truthOrigin = -1;  type = -1; 
    sf_id = 1.0; sf_iso = 1.0;
    mc_pid = 0; mc_pt=0; mc_eta=-999; mc_phi= -999;
    mcel_dr= -0.1; mcel_pt = -0.1; mcel_eta=-999; mcel_phi=-999; mc_production = -999;
  }

}; //end photonObj



class jetObj {
 public:
  Float_t pt, eta, phi, e;
  Float_t DL1r,jvt;
  Int_t tagWeightBin_DL1r_Continuous, passfjvt, truthflav, isBjet;
  std::string name;

 jetObj():
  pt(-1.0), eta(-999), phi(-999), e(-1.0),
    DL1r(-999),jvt(-999),tagWeightBin_DL1r_Continuous(-999),
    passfjvt(-999), truthflav(-1), isBjet(-999),
    name("testJet")
    {}
  ~jetObj() {}
  bool operator > (const jetObj& jet) const
  {
    return (pt > jet.pt);
  }

  void copy (jetObj& jet) {
    pt = jet.pt; 
    eta = jet.eta; 
    phi = jet.phi; 
    e = jet.e;
    DL1r = jet.DL1r; 
    jvt = jet.jvt; 
    tagWeightBin_DL1r_Continuous = jet.tagWeightBin_DL1r_Continuous;
    passfjvt = jet.passfjvt;  
    truthflav = jet.truthflav;  
    isBjet = jet.isBjet;

  }

  void reset() {
    pt = -1.0;  eta = -999;  phi = -999;  e = -1.0; 
    DL1r = -999; jvt = -999; tagWeightBin_DL1r_Continuous = -999; 
    passfjvt = -999;  truthflav = -1;  isBjet = -999;

  }

}; //end jetObj


class resObj {

 public:
  Float_t pt, eta, phi, pz, e, m, dr, dphi, deta, ctheta,rap;
  std::string name;

 resObj():
  pt(-1.0), eta(-999), phi(-999), pz(0.0),e(-1.0), m(-1.0),
    dr(-1.0), dphi(-999),deta(-999), ctheta(-999),rap(-999),
    name("")
    {}
  
  ~resObj() {}

  void setValues(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> & fv1, 
		 ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> & fv2) {
    using namespace ROOT::Math::VectorUtil;
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> resfv = fv1+fv2;
    pt = resfv.Pt();
    eta = resfv.Eta();
    phi = resfv.Phi();
    pz = resfv.Pz();
    e = resfv.E();
    m = InvariantMass(fv1, fv2);
    dr = DeltaR(fv1, fv2);
    dphi = DeltaPhi(fv1, fv2);
    deta = fabs(fv1.Eta() - fv2.Eta());
    ctheta = CosTheta(fv1, fv2);
    rap = 0.5*log((e+ pz)/(e-pz));
  }

  void reset() {
    pt = -1.0; eta = -999; phi = -999; pz = 0.0;e = -1.0; m = -1.0;
    dr = -1.0; dphi = -999;deta = -999; ctheta = -999; rap=-999;
  }

};

#endif
