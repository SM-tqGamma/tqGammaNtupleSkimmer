#ifndef TQGAMMANTUPLE_H
#define TQGAMMANTUPLE_H


#include <iostream>
#include <vector>
#include <string>
#include "CxxUtils/make_unique.h"

#include <TChain.h>
#include <TTree.h>
#include <TFile.h>
using namespace std;
class TqGammaNtuple {

 public:

  TqGammaNtuple(){}
  ~TqGammaNtuple(){}

  bool ReadFiles(const std::string& samplePath, const std::string& treeName) {
    fChain = CxxUtils::make_unique<TChain>(treeName.c_str());
    fChain->Add(samplePath.c_str());
    //std::cout<< "entries: "<<fChain->GetEntries()<<std::endl;
    return true;
  }
  //void Init() ;

  Int_t GetEntry(Long64_t entry) {
    return fChain->GetEntry(entry,0);
  }

  std::unique_ptr<TChain> fChain = 0;
  Int_t fCurrent;

  // Declaration of leaf types
  vector<float>   *ASM_weight;
  vector<float>   *ASM_weight_Syst_0;
  vector<string>  *ASM_weight_Systname_0;
    vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_globalLeptonTriggerSF;
   Float_t         weight_oldTriggerSF;
   Float_t         weight_bTagSF_DL1r_60;
   Float_t         weight_bTagSF_DL1r_70;
   Float_t         weight_bTagSF_DL1r_77;
   Float_t         weight_bTagSF_DL1r_85;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_UP;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_oldTriggerSF_EL_Trigger_UP;
   Float_t         weight_oldTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_photonSF_ID_UP;
   Float_t         weight_photonSF_ID_DOWN;
   Float_t         weight_photonSF_effIso;
   Float_t         weight_photonSF_effIso_UP;
   Float_t         weight_photonSF_effIso_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_firstEgMotherTruthType;
   vector<int>     *el_true_firstEgMotherTruthOrigin;
   vector<int>     *el_true_firstEgMotherPdgId;
   vector<char>    *el_true_isPrompt;
   vector<char>    *el_true_isChargeFl;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<char>    *mu_true_isPrompt;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *ph_iso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<int>     *jet_truthflavExtended;
   vector<char>    *jet_isbtagged_DL1r_60;
   vector<char>    *jet_isbtagged_DL1r_70;
   vector<char>    *jet_isbtagged_DL1r_77;
   vector<char>    *jet_isbtagged_DL1r_85;
   vector<int>     *jet_tagWeightBin_DL1r_Continuous;
   vector<float>   *jet_DL1;
   vector<float>   *jet_DL1r;
   vector<float>   *jet_DL1rmu;
   vector<float>   *jet_DL1_pu;
   vector<float>   *jet_DL1_pc;
   vector<float>   *jet_DL1_pb;
   vector<float>   *jet_DL1r_pu;
   vector<float>   *jet_DL1r_pc;
   vector<float>   *jet_DL1r_pb;
   vector<float>   *jet_DL1rmu_pu;
   vector<float>   *jet_DL1rmu_pc;
   vector<float>   *jet_DL1rmu_pb;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           triggermatch_el_no_jet;
   Int_t           triggermatch_el_gamma;
   Int_t           triggermatch_mu_gamma;
   Int_t           sel_ejets;
   Int_t           sel_mujets;
   Int_t           sel_zee_OS;
   Int_t           sel_zeg;
   Int_t           sel_radZ_eeg_OS;
   Int_t           sel_radZ_mumug_OS;
   Char_t          HLT_mu50;
   Char_t          HLT_mu26_ivarmedium;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   Char_t          HLT_e120_lhloose;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_topoetcone40;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptcone40;
   vector<char>    *ph_isoFCTCO;
   vector<char>    *ph_isoFCT;
   vector<char>    *ph_isoFCL;
   vector<char>    *ph_isTight;
   vector<char>    *ph_isLoose;
   vector<char>    *ph_isTight_daod;
   vector<char>    *ph_isHFake;
   vector<float>   *ph_rhad1;
   vector<float>   *ph_rhad;
   vector<float>   *ph_reta;
   vector<float>   *ph_weta2;
   vector<float>   *ph_rphi;
   vector<float>   *ph_ws3;
   vector<float>   *ph_wstot;
   vector<float>   *ph_fracm;
   vector<float>   *ph_deltaE;
   vector<float>   *ph_eratio;
   vector<float>   *ph_emaxs1;
   vector<float>   *ph_f1;
   vector<float>   *ph_e277;
   vector<unsigned int> *ph_OQ;
   vector<unsigned int> *ph_author;
   vector<int>     *ph_conversionType;
   vector<float>   *ph_caloEta;
   vector<unsigned int> *ph_isEM_Tight;
   vector<float>   *ph_SF_ID;
   vector<float>   *ph_SF_ID_unc;
   vector<float>   *ph_SF_iso;
   vector<float>   *ph_SF_iso_unc;
   vector<int>     *ph_truthType;
   vector<int>     *ph_truthOrigin;
   vector<int>     *ph_mc_pid;
   vector<float>   *ph_mc_pt;
   vector<float>   *ph_mc_eta;
   vector<float>   *ph_mc_phi;
   vector<float>   *ph_mcel_dr;
   vector<float>   *ph_mcel_pt;
   vector<float>   *ph_mcel_eta;
   vector<float>   *ph_mcel_phi;
   vector<int>     *ph_mc_parent_pid;
   vector<int>     *ph_mc_production;
   vector<char>    *el_isoGradient;
   vector<int>     *el_mc_pid;
   vector<float>   *el_mc_charge;
   vector<float>   *el_mc_pt;
   vector<float>   *el_mc_eta;
   vector<float>   *el_mc_phi;
   Char_t          event_in_overlap;
   Char_t          truthJetMatchedB;
   Char_t          truthJetMatchedD;
   // isTight flag for lepton fake estimation
   vector<char>    *mu_isTight;
   vector<char>    *el_isTight;

   // List of branches
   TBranch        *b_ASM_weight;   //!
   TBranch        *b_ASM_weight_Syst_0;   //!
   TBranch        *b_ASM_weight_Systname_0;   //!
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_weight_oldTriggerSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_60;   //!
   TBranch        *b_weight_bTagSF_DL1r_70;   //!
   TBranch        *b_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_weight_bTagSF_DL1r_85;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_photonSF_ID_UP;   //!
   TBranch        *b_weight_photonSF_ID_DOWN;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_weight_photonSF_effIso_UP;   //!
   TBranch        *b_weight_photonSF_effIso_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_firstEgMotherTruthType;   //!
   TBranch        *b_el_true_firstEgMotherTruthOrigin;   //!
   TBranch        *b_el_true_firstEgMotherPdgId;   //!
   TBranch        *b_el_true_isPrompt;   //!
   TBranch        *b_el_true_isChargeFl;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_iso;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_truthflavExtended;   //!
   TBranch        *b_jet_isbtagged_DL1r_60;   //!
   TBranch        *b_jet_isbtagged_DL1r_70;   //!
   TBranch        *b_jet_isbtagged_DL1r_77;   //!
   TBranch        *b_jet_isbtagged_DL1r_85;   //!
   TBranch        *b_jet_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_jet_DL1;   //!
   TBranch        *b_jet_DL1r;   //!
   TBranch        *b_jet_DL1rmu;   //!
   TBranch        *b_jet_DL1_pu;   //!
   TBranch        *b_jet_DL1_pc;   //!
   TBranch        *b_jet_DL1_pb;   //!
   TBranch        *b_jet_DL1r_pu;   //!
   TBranch        *b_jet_DL1r_pc;   //!
   TBranch        *b_jet_DL1r_pb;   //!
   TBranch        *b_jet_DL1rmu_pu;   //!
   TBranch        *b_jet_DL1rmu_pc;   //!
   TBranch        *b_jet_DL1rmu_pb;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_triggermatch_el_no_jet;   //!
   TBranch        *b_triggermatch_el_gamma;   //!
   TBranch        *b_triggermatch_mu_gamma;   //!
   TBranch        *b_sel_ejets;   //!
   TBranch        *b_sel_mujets;   //!
   TBranch        *b_sel_zee_OS;   //!
   TBranch        *b_sel_zeg;   //!
   TBranch        *b_sel_radZ_eeg_OS;   //!
   TBranch        *b_sel_radZ_mumug_OS;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_isoFCTCO;   //!
   TBranch        *b_ph_isoFCT;   //!
   TBranch        *b_ph_isoFCL;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isLoose;   //!
   TBranch        *b_ph_isTight_daod;   //!
   TBranch        *b_ph_isHFake;   //!
   TBranch        *b_ph_rhad1;   //!
   TBranch        *b_ph_rhad;   //!
   TBranch        *b_ph_reta;   //!
   TBranch        *b_ph_weta2;   //!
   TBranch        *b_ph_rphi;   //!
   TBranch        *b_ph_ws3;   //!
   TBranch        *b_ph_wstot;   //!
   TBranch        *b_ph_fracm;   //!
   TBranch        *b_ph_deltaE;   //!
   TBranch        *b_ph_eratio;   //!
   TBranch        *b_ph_emaxs1;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_e277;   //!
   TBranch        *b_ph_OQ;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_conversionType;   //!
   TBranch        *b_ph_caloEta;   //!
   TBranch        *b_ph_isEM_Tight;   //!
   TBranch        *b_ph_SF_ID;   //!
   TBranch        *b_ph_SF_ID_unc;   //!
   TBranch        *b_ph_SF_iso;   //!
   TBranch        *b_ph_SF_iso_unc;   //!
   TBranch        *b_ph_truthType;   //!
   TBranch        *b_ph_truthOrigin;   //!
   TBranch        *b_ph_mc_pid;   //!
   TBranch        *b_ph_mc_pt;   //!
   TBranch        *b_ph_mc_eta;   //!
   TBranch        *b_ph_mc_phi;   //!
   TBranch        *b_ph_mcel_dr;   //!
   TBranch        *b_ph_mcel_pt;   //!
   TBranch        *b_ph_mcel_eta;   //!
   TBranch        *b_ph_mcel_phi;   //!
   TBranch        *b_ph_mc_parent_pid;   //!
   TBranch        *b_ph_mc_production;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_mc_pid;   //!
   TBranch        *b_el_mc_charge;   //!
   TBranch        *b_el_mc_pt;   //!
   TBranch        *b_el_mc_eta;   //!
   TBranch        *b_el_mc_phi;   //!
   TBranch        *b_event_in_overlap;   //!
   TBranch        *b_truthJetMatchedB; //!
   TBranch        *b_truthJetMatchedD; //!
   TBranch        *b_mu_isTight; //!
   TBranch        *b_el_isTight; //!

   //};
//#endif

//#ifdef InputNtuple_cxx

   void Init(bool isData = false, bool doFake = false, bool isNominal = false)
{
  ASM_weight = 0;
  ASM_weight_Syst_0 = 0;
  ASM_weight_Systname_0 = 0;
   mc_generator_weights = 0;
   weight_bTagSF_DL1r_60_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_60_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_60_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_firstEgMotherTruthType = 0;
   el_true_firstEgMotherTruthOrigin = 0;
   el_true_firstEgMotherPdgId = 0;
   el_true_isPrompt = 0;
   el_true_isChargeFl = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_e = 0;
   ph_iso = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c10 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_truthflavExtended = 0;
   jet_isbtagged_DL1r_60 = 0;
   jet_isbtagged_DL1r_70 = 0;
   jet_isbtagged_DL1r_77 = 0;
   jet_isbtagged_DL1r_85 = 0;
   jet_tagWeightBin_DL1r_Continuous = 0;
   jet_DL1 = 0;
   jet_DL1r = 0;
   jet_DL1rmu = 0;
   jet_DL1_pu = 0;
   jet_DL1_pc = 0;
   jet_DL1_pb = 0;
   jet_DL1r_pu = 0;
   jet_DL1r_pc = 0;
   jet_DL1r_pb = 0;
   jet_DL1rmu_pu = 0;
   jet_DL1rmu_pc = 0;
   jet_DL1rmu_pb = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e60_lhmedium = 0;
   el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
   el_trigMatch_HLT_e120_lhloose = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   ph_topoetcone20 = 0;
   ph_topoetcone30 = 0;
   ph_topoetcone40 = 0;
   ph_ptcone20 = 0;
   ph_ptcone30 = 0;
   ph_ptcone40 = 0;
   ph_isoFCTCO = 0;
   ph_isoFCT = 0;
   ph_isoFCL = 0;
   ph_isTight = 0;
   ph_isLoose = 0;
   ph_isTight_daod = 0;
   ph_isHFake = 0;
   ph_rhad1 = 0;
   ph_rhad = 0;
   ph_reta = 0;
   ph_weta2 = 0;
   ph_rphi = 0;
   ph_ws3 = 0;
   ph_wstot = 0;
   ph_fracm = 0;
   ph_deltaE = 0;
   ph_eratio = 0;
   ph_emaxs1 = 0;
   ph_f1 = 0;
   ph_e277 = 0;
   ph_OQ = 0;
   ph_author = 0;
   ph_conversionType = 0;
   ph_caloEta = 0;
   ph_isEM_Tight = 0;
   ph_SF_ID = 0;
   ph_SF_ID_unc = 0;
   ph_SF_iso = 0;
   ph_SF_iso_unc = 0;
   ph_truthType = 0;
   ph_truthOrigin = 0;
   ph_mc_pid = 0;
   ph_mc_pt = 0;
   ph_mc_eta = 0;
   ph_mc_phi = 0;
   ph_mcel_dr = 0;
   ph_mcel_pt = 0;
   ph_mcel_eta = 0;
   ph_mcel_phi = 0;
   ph_mc_parent_pid = 0;
   ph_mc_production = 0;
   el_isoGradient = 0;
   el_mc_pid = 0;
   el_mc_charge = 0;
   el_mc_pt = 0;
   el_mc_eta = 0;
   el_mc_phi = 0;
   mu_isTight = 0;
   el_isTight = 0;

   if(!isData) {
     if(isNominal) { fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights); }
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_photonSF", &weight_photonSF, &b_weight_photonSF);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
   fChain->SetBranchAddress("weight_oldTriggerSF", &weight_oldTriggerSF, &b_weight_oldTriggerSF);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60", &weight_bTagSF_DL1r_60, &b_weight_bTagSF_DL1r_60);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70", &weight_bTagSF_DL1r_70, &b_weight_bTagSF_DL1r_70);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85", &weight_bTagSF_DL1r_85, &b_weight_bTagSF_DL1r_85);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   if(isNominal) {
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_UP", &weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_weight_globalLeptonTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_UP", &weight_oldTriggerSF_EL_Trigger_UP, &b_weight_oldTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_DOWN", &weight_oldTriggerSF_EL_Trigger_DOWN, &b_weight_oldTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_UP", &weight_oldTriggerSF_MU_Trigger_STAT_UP, &b_weight_oldTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &weight_oldTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_UP", &weight_oldTriggerSF_MU_Trigger_SYST_UP, &b_weight_oldTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &weight_oldTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_photonSF_ID_UP", &weight_photonSF_ID_UP, &b_weight_photonSF_ID_UP);
   fChain->SetBranchAddress("weight_photonSF_ID_DOWN", &weight_photonSF_ID_DOWN, &b_weight_photonSF_ID_DOWN);
   fChain->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso, &b_weight_photonSF_effIso);
   fChain->SetBranchAddress("weight_photonSF_effIso_UP", &weight_photonSF_effIso_UP, &b_weight_photonSF_effIso_UP);
   fChain->SetBranchAddress("weight_photonSF_effIso_DOWN", &weight_photonSF_effIso_DOWN, &b_weight_photonSF_effIso_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_B_up", &weight_bTagSF_DL1r_60_eigenvars_B_up, &b_weight_bTagSF_DL1r_60_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_C_up", &weight_bTagSF_DL1r_60_eigenvars_C_up, &b_weight_bTagSF_DL1r_60_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_Light_up", &weight_bTagSF_DL1r_60_eigenvars_Light_up, &b_weight_bTagSF_DL1r_60_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_B_down", &weight_bTagSF_DL1r_60_eigenvars_B_down, &b_weight_bTagSF_DL1r_60_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_C_down", &weight_bTagSF_DL1r_60_eigenvars_C_down, &b_weight_bTagSF_DL1r_60_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_Light_down", &weight_bTagSF_DL1r_60_eigenvars_Light_down, &b_weight_bTagSF_DL1r_60_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_up", &weight_bTagSF_DL1r_60_extrapolation_up, &b_weight_bTagSF_DL1r_60_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_down", &weight_bTagSF_DL1r_60_extrapolation_down, &b_weight_bTagSF_DL1r_60_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_from_charm_up", &weight_bTagSF_DL1r_60_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_60_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_from_charm_down", &weight_bTagSF_DL1r_60_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_60_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_up", &weight_bTagSF_DL1r_70_eigenvars_B_up, &b_weight_bTagSF_DL1r_70_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_up", &weight_bTagSF_DL1r_70_eigenvars_C_up, &b_weight_bTagSF_DL1r_70_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_up", &weight_bTagSF_DL1r_70_eigenvars_Light_up, &b_weight_bTagSF_DL1r_70_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_down", &weight_bTagSF_DL1r_70_eigenvars_B_down, &b_weight_bTagSF_DL1r_70_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_down", &weight_bTagSF_DL1r_70_eigenvars_C_down, &b_weight_bTagSF_DL1r_70_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_down", &weight_bTagSF_DL1r_70_eigenvars_Light_down, &b_weight_bTagSF_DL1r_70_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_up", &weight_bTagSF_DL1r_70_extrapolation_up, &b_weight_bTagSF_DL1r_70_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_down", &weight_bTagSF_DL1r_70_extrapolation_down, &b_weight_bTagSF_DL1r_70_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_up", &weight_bTagSF_DL1r_70_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_down", &weight_bTagSF_DL1r_70_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_up", &weight_bTagSF_DL1r_77_eigenvars_B_up, &b_weight_bTagSF_DL1r_77_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_up", &weight_bTagSF_DL1r_77_eigenvars_C_up, &b_weight_bTagSF_DL1r_77_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_up", &weight_bTagSF_DL1r_77_eigenvars_Light_up, &b_weight_bTagSF_DL1r_77_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_down", &weight_bTagSF_DL1r_77_eigenvars_B_down, &b_weight_bTagSF_DL1r_77_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_down", &weight_bTagSF_DL1r_77_eigenvars_C_down, &b_weight_bTagSF_DL1r_77_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_down", &weight_bTagSF_DL1r_77_eigenvars_Light_down, &b_weight_bTagSF_DL1r_77_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_up", &weight_bTagSF_DL1r_77_extrapolation_up, &b_weight_bTagSF_DL1r_77_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_down", &weight_bTagSF_DL1r_77_extrapolation_down, &b_weight_bTagSF_DL1r_77_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_up", &weight_bTagSF_DL1r_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_down", &weight_bTagSF_DL1r_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_B_up", &weight_bTagSF_DL1r_85_eigenvars_B_up, &b_weight_bTagSF_DL1r_85_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_C_up", &weight_bTagSF_DL1r_85_eigenvars_C_up, &b_weight_bTagSF_DL1r_85_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_Light_up", &weight_bTagSF_DL1r_85_eigenvars_Light_up, &b_weight_bTagSF_DL1r_85_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_B_down", &weight_bTagSF_DL1r_85_eigenvars_B_down, &b_weight_bTagSF_DL1r_85_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_C_down", &weight_bTagSF_DL1r_85_eigenvars_C_down, &b_weight_bTagSF_DL1r_85_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_Light_down", &weight_bTagSF_DL1r_85_eigenvars_Light_down, &b_weight_bTagSF_DL1r_85_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_up", &weight_bTagSF_DL1r_85_extrapolation_up, &b_weight_bTagSF_DL1r_85_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_down", &weight_bTagSF_DL1r_85_extrapolation_down, &b_weight_bTagSF_DL1r_85_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_from_charm_up", &weight_bTagSF_DL1r_85_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_85_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_from_charm_down", &weight_bTagSF_DL1r_85_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_85_extrapolation_from_charm_down);

   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &weight_bTagSF_DL1r_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &weight_bTagSF_DL1r_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &weight_bTagSF_DL1r_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &weight_bTagSF_DL1r_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);

   } //isNominal
   } // isData
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   if(!isData) { fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber); }
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   if(!isData) { fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
     fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
   fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl); }
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   if(!isData) { fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt); }
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);
   fChain->SetBranchAddress("ph_iso", &ph_iso, &b_ph_iso);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
   if(!isData) {   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
   fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended); }
   fChain->SetBranchAddress("jet_isbtagged_DL1r_60", &jet_isbtagged_DL1r_60, &b_jet_isbtagged_DL1r_60);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_70", &jet_isbtagged_DL1r_70, &b_jet_isbtagged_DL1r_70);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_77", &jet_isbtagged_DL1r_77, &b_jet_isbtagged_DL1r_77);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_85", &jet_isbtagged_DL1r_85, &b_jet_isbtagged_DL1r_85);
   fChain->SetBranchAddress("jet_tagWeightBin_DL1r_Continuous", &jet_tagWeightBin_DL1r_Continuous, &b_jet_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("jet_DL1", &jet_DL1, &b_jet_DL1);
   fChain->SetBranchAddress("jet_DL1r", &jet_DL1r, &b_jet_DL1r);
   fChain->SetBranchAddress("jet_DL1rmu", &jet_DL1rmu, &b_jet_DL1rmu);
   fChain->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
   fChain->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
   fChain->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
   fChain->SetBranchAddress("jet_DL1r_pu", &jet_DL1r_pu, &b_jet_DL1r_pu);
   fChain->SetBranchAddress("jet_DL1r_pc", &jet_DL1r_pc, &b_jet_DL1r_pc);
   fChain->SetBranchAddress("jet_DL1r_pb", &jet_DL1r_pb, &b_jet_DL1r_pb);
   fChain->SetBranchAddress("jet_DL1rmu_pu", &jet_DL1rmu_pu, &b_jet_DL1rmu_pu);
   fChain->SetBranchAddress("jet_DL1rmu_pc", &jet_DL1rmu_pc, &b_jet_DL1rmu_pc);
   fChain->SetBranchAddress("jet_DL1rmu_pb", &jet_DL1rmu_pb, &b_jet_DL1rmu_pb);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("triggermatch_el_no_jet", &triggermatch_el_no_jet, &b_triggermatch_el_no_jet);
   fChain->SetBranchAddress("triggermatch_el_gamma", &triggermatch_el_gamma, &b_triggermatch_el_gamma);
   fChain->SetBranchAddress("triggermatch_mu_gamma", &triggermatch_mu_gamma, &b_triggermatch_mu_gamma);
   fChain->SetBranchAddress("sel_ejets", &sel_ejets, &b_sel_ejets);
   fChain->SetBranchAddress("sel_mujets", &sel_mujets, &b_sel_mujets);
   fChain->SetBranchAddress("sel_zee_OS", &sel_zee_OS, &b_sel_zee_OS);
   fChain->SetBranchAddress("sel_zeg", &sel_zeg, &b_sel_zeg);
   fChain->SetBranchAddress("sel_radZ_eeg_OS", &sel_radZ_eeg_OS, &b_sel_radZ_eeg_OS);
   fChain->SetBranchAddress("sel_radZ_mumug_OS", &sel_radZ_mumug_OS, &b_sel_radZ_mumug_OS);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
   fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
   fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
   fChain->SetBranchAddress("ph_isoFCTCO", &ph_isoFCTCO, &b_ph_isoFCTCO);
   fChain->SetBranchAddress("ph_isoFCT", &ph_isoFCT, &b_ph_isoFCT);
   fChain->SetBranchAddress("ph_isoFCL", &ph_isoFCL, &b_ph_isoFCL);
   fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
   fChain->SetBranchAddress("ph_isLoose", &ph_isLoose, &b_ph_isLoose);
   fChain->SetBranchAddress("ph_isTight_daod", &ph_isTight_daod, &b_ph_isTight_daod);
   fChain->SetBranchAddress("ph_isHFake", &ph_isHFake, &b_ph_isHFake);
   fChain->SetBranchAddress("ph_rhad1", &ph_rhad1, &b_ph_rhad1);
   fChain->SetBranchAddress("ph_rhad", &ph_rhad, &b_ph_rhad);
   fChain->SetBranchAddress("ph_reta", &ph_reta, &b_ph_reta);
   fChain->SetBranchAddress("ph_weta2", &ph_weta2, &b_ph_weta2);
   fChain->SetBranchAddress("ph_rphi", &ph_rphi, &b_ph_rphi);
   fChain->SetBranchAddress("ph_ws3", &ph_ws3, &b_ph_ws3);
   fChain->SetBranchAddress("ph_wstot", &ph_wstot, &b_ph_wstot);
   fChain->SetBranchAddress("ph_fracm", &ph_fracm, &b_ph_fracm);
   fChain->SetBranchAddress("ph_deltaE", &ph_deltaE, &b_ph_deltaE);
   fChain->SetBranchAddress("ph_eratio", &ph_eratio, &b_ph_eratio);
   fChain->SetBranchAddress("ph_emaxs1", &ph_emaxs1, &b_ph_emaxs1);
   fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
   fChain->SetBranchAddress("ph_e277", &ph_e277, &b_ph_e277);
   fChain->SetBranchAddress("ph_OQ", &ph_OQ, &b_ph_OQ);
   fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
   fChain->SetBranchAddress("ph_conversionType", &ph_conversionType, &b_ph_conversionType);
   fChain->SetBranchAddress("ph_caloEta", &ph_caloEta, &b_ph_caloEta);
   fChain->SetBranchAddress("ph_isEM_Tight", &ph_isEM_Tight, &b_ph_isEM_Tight);
   fChain->SetBranchAddress("ph_SF_ID", &ph_SF_ID, &b_ph_SF_ID);
   fChain->SetBranchAddress("ph_SF_ID_unc", &ph_SF_ID_unc, &b_ph_SF_ID_unc);
   fChain->SetBranchAddress("ph_SF_iso", &ph_SF_iso, &b_ph_SF_iso);
   fChain->SetBranchAddress("ph_SF_iso_unc", &ph_SF_iso_unc, &b_ph_SF_iso_unc);
   fChain->SetBranchAddress("ph_truthType", &ph_truthType, &b_ph_truthType);
   fChain->SetBranchAddress("ph_truthOrigin", &ph_truthOrigin, &b_ph_truthOrigin);
   fChain->SetBranchAddress("ph_mc_pid", &ph_mc_pid, &b_ph_mc_pid);
   fChain->SetBranchAddress("ph_mc_pt", &ph_mc_pt, &b_ph_mc_pt);
   fChain->SetBranchAddress("ph_mc_eta", &ph_mc_eta, &b_ph_mc_eta);
   fChain->SetBranchAddress("ph_mc_phi", &ph_mc_phi, &b_ph_mc_phi);
   fChain->SetBranchAddress("ph_mcel_dr", &ph_mcel_dr, &b_ph_mcel_dr);
   fChain->SetBranchAddress("ph_mcel_pt", &ph_mcel_pt, &b_ph_mcel_pt);
   fChain->SetBranchAddress("ph_mcel_eta", &ph_mcel_eta, &b_ph_mcel_eta);
   fChain->SetBranchAddress("ph_mcel_phi", &ph_mcel_phi, &b_ph_mcel_phi);
   fChain->SetBranchAddress("ph_mc_parent_pid", &ph_mc_parent_pid, &b_ph_mc_parent_pid);
   fChain->SetBranchAddress("ph_mc_production", &ph_mc_production, &b_ph_mc_production);
   fChain->SetBranchAddress("el_isoGradient", &el_isoGradient, &b_el_isoGradient);
   fChain->SetBranchAddress("el_mc_pid", &el_mc_pid, &b_el_mc_pid);
   fChain->SetBranchAddress("el_mc_charge", &el_mc_charge, &b_el_mc_charge);
   fChain->SetBranchAddress("el_mc_pt", &el_mc_pt, &b_el_mc_pt);
   fChain->SetBranchAddress("el_mc_eta", &el_mc_eta, &b_el_mc_eta);
   fChain->SetBranchAddress("el_mc_phi", &el_mc_phi, &b_el_mc_phi);
   fChain->SetBranchAddress("event_in_overlap", &event_in_overlap, &b_event_in_overlap);
   fChain->SetBranchAddress("truthJetMatchedB", &truthJetMatchedB, &b_truthJetMatchedB);
   fChain->SetBranchAddress("truthJetMatchedD", &truthJetMatchedD, &b_truthJetMatchedD);

   if(doFake) {
     fChain->SetBranchAddress("ASM_weight", &ASM_weight, &b_ASM_weight);
     fChain->SetBranchAddress("ASM_weight_Syst_0", &ASM_weight_Syst_0, &b_ASM_weight_Syst_0);
     fChain->SetBranchAddress("ASM_weight_Systname_0", &ASM_weight_Systname_0, &b_ASM_weight_Systname_0);
     fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
     fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   }

}

}; //end class declaration
#endif
