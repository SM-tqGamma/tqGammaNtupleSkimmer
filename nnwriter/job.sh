#!/bin/bash
source setup.sh
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8
echo "python NNout_ur.py --ifile $1 --ofile $2 --treename $3"
python NNout_ur.py --ifile $1 --ofile $2 --treename $3
