import os,sys,click,time

def getVariations():

    variations = [
        "CategoryReduction_JET_BJES_Response__1down",
        "CategoryReduction_JET_BJES_Response__1up",
        "CategoryReduction_JET_EffectiveNP_Detector1__1down",
        "CategoryReduction_JET_EffectiveNP_Detector1__1up",
        "CategoryReduction_JET_EffectiveNP_Detector2__1down",
        "CategoryReduction_JET_EffectiveNP_Detector2__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed1__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed1__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed2__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed2__1up",
        "CategoryReduction_JET_EffectiveNP_Mixed3__1down",
        "CategoryReduction_JET_EffectiveNP_Mixed3__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling1__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling1__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling2__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling2__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling3__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling3__1up",
        "CategoryReduction_JET_EffectiveNP_Modelling4__1down",
        "CategoryReduction_JET_EffectiveNP_Modelling4__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical1__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical1__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical2__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical2__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical3__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical3__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical4__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical4__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical5__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical5__1up",
        "CategoryReduction_JET_EffectiveNP_Statistical6__1down",
        "CategoryReduction_JET_EffectiveNP_Statistical6__1up",
        "CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
        "CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
        "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
        "CategoryReduction_JET_EtaIntercalibration_TotalStat__1down",
        "CategoryReduction_JET_EtaIntercalibration_TotalStat__1up",
        "CategoryReduction_JET_Flavor_Composition__1down",
        "CategoryReduction_JET_Flavor_Composition__1up",
        "CategoryReduction_JET_Flavor_Response__1down",
        "CategoryReduction_JET_Flavor_Response__1up",
        "CategoryReduction_JET_JER_DataVsMC_MC16__1down",
        "CategoryReduction_JET_JER_DataVsMC_MC16__1up",
        "CategoryReduction_JET_JER_EffectiveNP_1__1down",
        "CategoryReduction_JET_JER_EffectiveNP_1__1up",
        "CategoryReduction_JET_JER_EffectiveNP_2__1down",
        "CategoryReduction_JET_JER_EffectiveNP_2__1up",
        "CategoryReduction_JET_JER_EffectiveNP_3__1down",
        "CategoryReduction_JET_JER_EffectiveNP_3__1up",
        "CategoryReduction_JET_JER_EffectiveNP_4__1down",
        "CategoryReduction_JET_JER_EffectiveNP_4__1up",
        "CategoryReduction_JET_JER_EffectiveNP_5__1down",
        "CategoryReduction_JET_JER_EffectiveNP_5__1up",
        "CategoryReduction_JET_JER_EffectiveNP_6__1down",
        "CategoryReduction_JET_JER_EffectiveNP_6__1up",
        "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",
        "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",
        "CategoryReduction_JET_Pileup_OffsetMu__1down",
        "CategoryReduction_JET_Pileup_OffsetMu__1up",
        "CategoryReduction_JET_Pileup_OffsetNPV__1down",
        "CategoryReduction_JET_Pileup_OffsetNPV__1up",
        "CategoryReduction_JET_Pileup_PtTerm__1down",
        "CategoryReduction_JET_Pileup_PtTerm__1up",
        "CategoryReduction_JET_Pileup_RhoTopology__1down",
        "CategoryReduction_JET_Pileup_RhoTopology__1up",
        "CategoryReduction_JET_PunchThrough_MC16__1down",
        "CategoryReduction_JET_PunchThrough_MC16__1up",
        "CategoryReduction_JET_SingleParticle_HighPt__1down",
        "CategoryReduction_JET_SingleParticle_HighPt__1up",
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_AF2__1down",
        "EG_SCALE_AF2__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up",
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_ScaleDown",
        "MET_SoftTrk_ScaleUp",
        "MUON_ID__1down",
        "MUON_ID__1up",
        "MUON_MS__1down",
        "MUON_MS__1up",
        "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RESBIAS__1up",
        "MUON_SAGITTA_RHO__1down",
        "MUON_SAGITTA_RHO__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up",
        "nominal",
        "CategoryReduction_JET_JER_DataVsMC_AFII__1down",
        "CategoryReduction_JET_JER_DataVsMC_AFII__1up",
        "CategoryReduction_JET_PunchThrough_AFII__1down",
        "CategoryReduction_JET_PunchThrough_AFII__1up",
        "CategoryReduction_JET_RelativeNonClosure_AFII__1down",
        "CategoryReduction_JET_RelativeNonClosure_AFII__1up",]
    return variations

def sDict():
    sampleDict = {}
    sampleDict["tqGamma"] = [412147]
    sampleDict["ttbar"] = [410470]
    sampleDict["ttGamma"] = [410389]
    sampleDict["ZJets"] = list(range(364114,364128))+list(range(364100,364114))+list(range(364128,364142))
    sampleDict["ZeeJets"] = list(range(364114,364128))
    sampleDict["ZmmJets"] = list(range(364100,364114))
    sampleDict["ZttJets"] = list(range(364128,364142))

    sampleDict["WJets"] = list(range(364170,364184))+list(range(364156,364170))+list(range(364184,364198))
    sampleDict["WenJets"] = list(range(364170,364184))
    sampleDict["WmnJets"] = list(range(364156,364170))
    sampleDict["WtnJets"] = list(range(364184,364198))

    sampleDict["ZJetsGamma"] = list(range(366140,366145))+list(range(366145,366150))+list(range(366150,366155))
    sampleDict["WJetsGamma"] = list(range(364522,364526))+list(range(364527,364531))+list(range(364532,364536))
    sampleDict["WJetsGammaMissing"] = [364527, 364528]
    sampleDict["tchan"] = [410658,410659]
    sampleDict["tW"] = [410646,410647]
    sampleDict["schan"] = [410644,410645]
    sampleDict["diboson"] = [364250,364253,364254,364255,363355,363356,363357,363358,363359,363360,363489]
    sampleDict["ttGammaNew"] = [500800, 504554]

    sampleDict["tqGammaShower"] = [500537]
    sampleDict["ttGammaVar"] = [410395,410404,410405]
    sampleDict["ttbarVar"] = [410480,410482]
    sampleDict["SingleTopVar"] = [411032, 411033, 411034, 411035, 411036, 411037]
    sampleDict["tqGammaLO"] = [412089]
    sampleDict["ttbarHW71"] = [411233, 411234]
    sampleDict["ttbarHW"] = [410557, 410558]
    sampleDict["tWDR"] = [410655, 410654]

    sampleDict["allSyst"] = sampleDict["tqGamma"]+sampleDict["ttbar"]+sampleDict["ttGamma"]+sampleDict["ZJets"]+sampleDict["ZJetsGamma"]+sampleDict["WJetsGamma"]+sampleDict["WJets"]+ \
    sampleDict["tchan"]+sampleDict["tW"]+sampleDict["schan"]+sampleDict["diboson"]+sampleDict["ttGammaNew"]

    sampleDict["allNom"] = sampleDict["tqGammaShower"]+sampleDict["ttGammaVar"]+sampleDict["ttbarVar"]+sampleDict["SingleTopVar"]+sampleDict["tqGammaLO"]+sampleDict["ttbarHW71"]+sampleDict["ttbarHW"]
    return sampleDict

def jobSubmitter(jobConfigs):
    import htcondor, classad
    batch_name = jobConfigs[0]["sample"]+jobConfigs[0]["af2"]+"_"+jobConfigs[0]["camp"]+"_"+jobConfigs[0]["sysname"]
    if jobConfigs[0]["sample"] == "": batch_name = jobConfigs[0]["dsid"]+jobConfigs[0]["af2"]+"_"+jobConfigs[0]["camp"]+"_"+jobConfigs[0]["sysname"]
    condorLogDir = f'condorLogs/{jobConfigs[0]["sample"]+jobConfigs[0]["af2"]}/{jobConfigs[0]["sysname"]}'
    os.system(f"mkdir -p {condorLogDir}")
    job = htcondor.Submit({
        "executable": "job.sh",   # the program to run on the execute node
        "arguments": "$(ifile) $(ofile) $(sysname)", # arguments passed to the executable
        "output": f"{condorLogDir}/$(dsid)$(af2)_$(sysname).out", # anything the job prints to standard output will end up in this file
        "error": f"{condorLogDir}//$(dsid)$(af2)_$(sysname).err",  # anything the job prints to standard error will end up in this file
        "log": f"{condorLogDir}//$(dsid)$(af2)_$(sysname).log", # this file will contain a record of what happened to the job
        "request_cpus": "1", # how many CPU cores we want
        "request_memory": "4GB",  # how much memory we want
        "batch_name": batch_name,
        "requirements": "has_avx",
    })
    collector = htcondor.Collector()
    WaitForNextBatch = True
    while WaitForNextBatch:
        jn = 0
        for schedd_ad in collector.locateAll(htcondor.DaemonTypes.Schedd):
            schedd = htcondor.Schedd(schedd_ad)
            ads = schedd.query("JobStatus==1", ['ClusterId', 'OWNER'])
            jn += len(ads)
        if jn < 1000: WaitForNextBatch=False
        if WaitForNextBatch:
            print(f"Waiting for queue to empty below 1000 idle jobs! Currently {jn} jobs are idle")
            time.sleep(5)
    schedd = htcondor.Schedd()
    successful =False
    while not successful:
        try:
            with schedd.transaction() as txn:
                submit_result = job.queue_with_itemdata(txn, itemdata = iter(jobConfigs))
            successful = True
            print("Submission successful")
        except:
            print("Submission not successful")
    return

@click.command()
@click.option("--campaign","-c", multiple=True,required=True, type=click.Choice(["mc16a", "mc16d", "mc16e"]), help="Specify which campaign you want to run on.")
@click.option("--dofake", is_flag=True)
@click.option("--isdata", is_flag=True)
@click.option("--dsid", "-d", multiple=True,default=[0])
@click.option("--isaf2", is_flag=True)
@click.option("--sysname", default="nominal")
@click.option("--samples", "-s",multiple=True ,default=[""])
@click.option("--debug", is_flag=True)
def main(campaign, dofake, isdata, dsid, isaf2, sysname, samples, debug):
    mntdir = os.environ["MNTDIR"]
    odir = os.environ["NNDIR"]
    if not os.path.exists(odir):
        print(f"Generating output directory: {odir}")
        os.system(f"mkdir -p {odir}")
    variations = getVariations()
    if not sysname in variations and not sysname == "all":
        print(f"{sysname} not in list of variations. Exiting.")
        sys.exit(1)
    sampleDict = sDict()
    if "all" in samples: samples = sampleDict.keys()
    for sample in samples:
        if not sample == "" and not sample in sampleDict.keys():
            print(f"{sample} not specified in the sample dictionary. Exiting")
            sys.exit(1)
        elif not sample == "":
            dsid = sampleDict[sample]
        if not isdata and dsid == (0,):
            print("Specify at least one dsid! Exiting")
            sys.exit(1)
        systs = [sysname]
        if sysname == "all": systs = variations
        for syst in systs:
            jobConfigs = []
            for camp in campaign:
                subdir = [camp]
                if isdata:
                    if "mc16a" == camp: subdir = ["data15", "data16"]
                    elif "mc16d" == camp: subdir = ["data17"]
                    elif "mc16e" == camp: subdir = ["data18"]
                for sub in subdir:
                    if dofake: sub = sub.replace("data", "lfake")
                    if debug: print(f"Searching for input in {mntdir}/{sub}")
                    if debug: print("########################################")
                    for cursample in os.listdir(f"{mntdir}/{sub}"):
                        if not os.path.isdir(f"{mntdir}/{sub}/{cursample}"): continue
                        if not isaf2 and "af2" in cursample: continue
                        elif isaf2 and not "af2" in cursample: continue
                        for cdsid in dsid:
                            if not isdata and not str(cdsid) in cursample: continue
                            idir = f"{mntdir}/{sub}/{cursample}/{syst}"
                            if isdata: idir = f"{mntdir}/{sub}/{cursample}"
                            if not os.path.exists(f"{idir}") and not isdata:
                                if debug: print(f"Did not find {syst} for {cursample}")
                                continue
                            ofile = f"{odir}/{sub}/{cursample}/{syst}"
                            if isdata: ofile = f"{odir}/{sub}/{cursample}"
                            if debug: print(f"Found input directory: {idir}")
                            if debug: print("----------------------------------------")
                            if os.path.exists(ofile) and len(os.listdir(ofile)) > 0:
                                print(f"rm {ofile}/*.root")
                                os.system(f"rm {ofile}/*.root")
                            if not os.path.exists(ofile):
                                if debug: print(f"Generating output directory: {ofile}")
                                os.system(f"mkdir -p {ofile}")
                            for rfile in os.listdir(f"{idir}"):
                                ofile = f"{odir}/{sub}/{cursample}/{syst}"
                                if isdata: ofile = f"{odir}/{sub}/{cursample}"
                                jobConfig = {}
                                jobConfig["ifile"] = ""
                                jobConfig["ofile"] = ""
                                jobConfig["sysname"] = ""
                                jobConfig["dsid"] = str(cdsid)
                                jobConfig["af2"] = ""
                                if not rfile.endswith(".root"): continue
                                ifile = f"{idir}/{rfile}"
                                if debug: print(f"Found inputfile: {ifile}")
                                ofile += f"/{rfile}"
                                if debug: print(f"Output will be saved at {ofile}")
                                jobConfig["ifile"] = ifile
                                jobConfig["ofile"] = ofile
                                jobConfig["sysname"] = syst
                                jobConfig["sample"] = sample
                                jobConfig["camp"] = camp
                                if isdata:
                                    jobConfig["dsid"] = sub
                                if isaf2: jobConfig["af2"] = "_af2"
                                jobConfigs.append(jobConfig)
                            if debug: print("########################################")
            print(f"Submitting jobs {sample} for {syst}")
            jobSubmitter(jobConfigs)

if __name__ == "__main__":
    main()
