import os,sys, ROOT
from tqdm import tqdm
import click
from tensorflow import keras
import numpy as np
import pandas as pd
import multiprocessing as mp
from functools import partial
import uproot


def provFlist():
	featurelist_1fj = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', \
    'ph_pt', 'ph_eta', 'ph_phi', 'met_met', "HT",\
    'met_phi', 'lbj_pt','lbj_eta', 'lbj_phi', 'njets', \
    'fj_pt', 'fj_phi', 'fj_eta', 'nfjets','transMass', "fjph_ctheta",\
    'fjet_flag', "bfj_m", "blep_m","bph_m", 'transMassWb', \
    "transMassWph", "lepph_dr", "lfj_dr", "blep_dr", "fjph_dr", "fjph_m", "top_m", "Wbsn_e", "topph_pt","topph_ctheta", "fjph_e", "fjph_deta", "bph_deta", "blep_deta",\
    "ph_conversionType", "lbj_tagWeightBin_DL1r_Continuous", "lepph_m", "bph_pt"]

	featurelist_0fj = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', \
    'ph_pt', 'ph_eta', 'ph_phi', 'met_met', "HT",\
    'met_phi', 'lbj_pt','lbj_eta', 'lbj_phi', 'njets', \
    'transMass', "blep_m","bph_m", 'transMassWb', \
    "transMassWph", "lepph_dr",  "blep_dr", "top_m", "Wbsn_e", "topph_pt","topph_ctheta","bph_deta", "blep_deta","ph_conversionType", "lbj_tagWeightBin_DL1r_Continuous", "lepph_m", "bph_pt"]

	remvars_1fj_nom = ["ph_conversionType","topph_pt", "topph_ctheta", "lepph_m", "bph_deta", "bph_m", "ph_eta", "fj_phi", "lbj_phi", "lfj_dr", "bph_pt", "lbj_tagWeightBin_DL1r_Continuous", "transMassWph", "lbj_pt", "ph_pt", "HT", "blep_deta"]
	remvars_0fj_nom = ["ph_conversionType", "lbj_phi", "bph_pt", "lepph_m", "lep1_id", "blep_deta", "Wbsn_e", "transMass", "topph_ctheta", "bph_deta", "lep1_phi", "lbj_pt", "met_phi", "ph_phi", "met_met"]

	remvars_1fj_mix = ["njets", "nfjets","ph_conversionType", "blep_deta", "lepph_m", "topph_pt", "lep1_phi", "transMass", "ph_eta", "lep1_id", "bph_pt", "fj_pt", "bph_deta", "transMassWph", "lfj_dr", "lepph_dr"]
	remvars_0fj_mix = ["njets","ph_conversionType", "lep1_pt", "transMass", "blep_deta", "lep1_phi", "ph_phi", "bph_deta", "Wbsn_e", "topph_ctheta", "lepph_m", "blep_m", "met_phi", "lbj_phi", "bph_m"]

	remvars_1fj_nomwonj = ["njets", "nfjets", "ph_conversionType", 'bph_deta', 'bph_pt', 'topph_ctheta', 'bph_m', 'lep1_phi', 'lepph_m', 'met_phi', 'blep_deta', 'ph_eta', 'lbj_phi', 'fjph_m', 'fjet_flag', 'met_met', 'fj_eta', 'fj_phi', 'transMassWb', 'fjph_ctheta', 'ph_phi', 'lbj_eta', 'topph_pt', 'ph_pt', 'Wbsn_e', 'fjph_dr', 'lep1_eta', 'lep1_pt']    #for 1fj
	remvars_0fj_nomwonj = ["njets", "ph_conversionType",'bph_pt', 'lep1_id', 'met_phi', 'lepph_m', 'blep_deta', 'ph_pt', 'bph_deta', 'lbj_phi', 'Wbsn_e', 'bph_m', 'transMass', 'topph_ctheta', 'lep1_phi', 'ph_phi', 'transMassWb', 'blep_m', 'met_met']  #for 0fj

	featurelist_1fj_nom = [feature for feature in featurelist_1fj if not feature in remvars_1fj_nom]
	featurelist_0fj_nom = [feature for feature in featurelist_0fj if not feature in remvars_0fj_nom]

	featurelist_1fj_mix = [feature for feature in featurelist_1fj if not feature in remvars_1fj_mix]
	featurelist_0fj_mix = [feature for feature in featurelist_0fj if not feature in remvars_0fj_mix]

	featurelist_1fj_nomwonj = [feature for feature in featurelist_1fj if not feature in remvars_1fj_nomwonj]
	featurelist_0fj_nomwonj = [feature for feature in featurelist_0fj if not feature in remvars_0fj_nomwonj]

	return [featurelist_1fj_nom, featurelist_1fj_mix, featurelist_1fj_nomwonj],[featurelist_0fj_nom, featurelist_0fj_mix, featurelist_0fj_nomwonj]


def NNout(params, models_1fj, models_0fj, treename):
	ifile = params[0]
	ofile = params[1]
	MinMaxDF_1fj_nom = pd.read_csv("input/chosen/1fj_nom/MinMax.csv")
	MinMaxDF_0fj_nom = pd.read_csv("input/chosen/0fj_nom/MinMax.csv")

	MinMaxDF_1fj_mix = pd.read_csv("input/chosen/1fj_mix/MinMax.csv")
	MinMaxDF_0fj_mix = pd.read_csv("input/chosen/0fj_mix/MinMax.csv")

	MinMaxDF_1fj_nomwonj = pd.read_csv("input/chosen/1fj_nomwonj/MinMax.csv")
	MinMaxDF_0fj_nomwonj = pd.read_csv("input/chosen/0fj_nomwonj/MinMax.csv")

	fl_1fj, fl_0fj = provFlist()
	uniqueFl = []
	for fl in fl_1fj:
		for feature in fl:
			if not feature in uniqueFl: uniqueFl.append(feature)

	for fl in fl_0fj:
		for feature in fl:
			if not feature in uniqueFl: uniqueFl.append(feature)
	if not "nbjets_DL1r_70" in uniqueFl: uniqueFl.append("nbjets_DL1r_70")

	uptree = uproot.open(f"{ifile}:{treename}")
	events = uptree.arrays(uniqueFl, library="pd")
	res_nom = []
	res_mix = []

	itfile = ROOT.TFile(ifile)
	itree = itfile.Get(treename)
	#print(f"itree has {itree.GetEntries()} Entries")
	#print("---------------------------------------------")
	ListOfBranches = itree.GetListOfBranches()
	for branch in ListOfBranches:
		#if "NN_out" in branch.GetName():
		itree.SetBranchStatus(branch.GetName(), 0)
		#	break
	otfile = ROOT.TFile(ofile,"RECREATE")
	#otree = itree.CopyTree("1<2")
	otree = ROOT.TTree(treename, treename)
	ListOfKeys = [key.GetName() for key in itfile.GetListOfKeys()]
	#if "genWeightNames" in ListOfKeys:
	#	wtree = itfile.Get("genWeightNames")
	#	wotree = wtree.CopyTree("1<2")
	#	wotree.Write()
	#if "SFWeightNames" in ListOfKeys:
	#	SFtree = itfile.Get("SFWeightNames")
	#	SFotree = SFtree.CopyTree("1<2")
	#	SFotree.Write()

	itfile.Close()
	NN_out_nom = np.zeros(1, dtype=float)
	NN_out_mix = np.zeros(1, dtype=float)
	NN_out_nomwonj = np.zeros(1, dtype=float)
	NN_branch_nom = otree.Branch("NN_out_nom", NN_out_nom, "NN_out_nom/D")
	NN_branch_mix = otree.Branch("NN_out_mix", NN_out_mix, "NN_out_mix/D")
	NN_branch_nomwonj = otree.Branch("NN_out_nomwonj", NN_out_nomwonj, "NN_out_nomwonj/D")

	for index, event in tqdm(events.iterrows()):
		if not (event["nbjets_DL1r_70"] == 1):
			NN_out_nomwonj[0] = 0.
			NN_out_nom[0] = 0.
			NN_out_mix[0] = 0.
		elif event["nfjets"] == 0:
			event_nom = event[fl_0fj[0]]
			event_mix = event[fl_0fj[1]]
			event_nomwonj = event[fl_0fj[2]]

			for feature in event_nom.keys():
				scale = 1.0
				#if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_nom[feature] = (event_nom[feature]*scale - MinMaxDF_0fj_nom[feature][0]) / (MinMaxDF_0fj_nom[feature][1] - MinMaxDF_0fj_nom[feature][0])
			for feature in event_mix.keys():
				scale = 1.0
				#if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_mix[feature] = (event_mix[feature]*scale - MinMaxDF_0fj_mix[feature][0]) / (MinMaxDF_0fj_mix[feature][1] - MinMaxDF_0fj_mix[feature][0])
			for feature in event_nomwonj.keys():
				scale = 1.0
				#if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_nomwonj[feature] = (event_nomwonj[feature]*scale - MinMaxDF_0fj_nomwonj[feature][0]) / (MinMaxDF_0fj_nomwonj[feature][1] - MinMaxDF_0fj_nomwonj[feature][0])

			NN_out_nom[0] = models_0fj[0].predict(np.asarray([event_nom]))[0][0]
			NN_out_mix[0] = models_0fj[1].predict(np.asarray([event_mix]))[0][0]
			NN_out_nomwonj[0] = models_0fj[2].predict(np.asarray([event_nomwonj]))[0][0]
		else:
			event_nom = event[fl_1fj[0]]
			event_mix = event[fl_1fj[1]]
			event_nomwonj = event[fl_1fj[2]]
			for feature in event_nom.keys():
				scale = 1.0
			#	if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_nom[feature] = (event_nom[feature]*scale - MinMaxDF_1fj_nom[feature][0]) / (MinMaxDF_1fj_nom[feature][1] - MinMaxDF_1fj_nom[feature][0])
			for feature in event_mix.keys():
				scale = 1.0
			#	if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_mix[feature] = (event_mix[feature]*scale - MinMaxDF_1fj_mix[feature][0]) / (MinMaxDF_1fj_mix[feature][1] - MinMaxDF_1fj_mix[feature][0])
			for feature in event_nomwonj.keys():
				scale = 1.0
				#if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000.
				event_nomwonj[feature] = (event_nomwonj[feature]*scale - MinMaxDF_1fj_nomwonj[feature][0]) / (MinMaxDF_1fj_nomwonj[feature][1] - MinMaxDF_1fj_nomwonj[feature][0])

			NN_out_nom[0] = models_1fj[0].predict(np.asarray([event_nom]))[0][0]
			NN_out_mix[0] = models_1fj[1].predict(np.asarray([event_mix]))[0][0]
			NN_out_nomwonj[0] = models_1fj[2].predict(np.asarray([event_nomwonj]))[0][0]
		#NN_branch_nom.Fill()
		#NN_branch_mix.Fill()
		#NN_branch_nomwonj.Fill()
		otree.Fill()

	otree.Write()
	otfile.Close()

@click.command()
@click.option("--ifile", required=True)
@click.option("--ofile", required=True)
@click.option("--treename", required=True)
def main(ifile, ofile, treename):

	model_1fj_nom = keras.models.load_model("input/chosen/1fj_nom/NNnet_kFold2.h5")
	model_0fj_nom = keras.models.load_model("input/chosen/0fj_nom/NNnet_kFold2.h5")

	model_1fj_mix = keras.models.load_model("input/chosen/1fj_mix/NNnet_kFold2.h5")
	model_0fj_mix = keras.models.load_model("input/chosen/0fj_mix/NNnet_kFold2.h5")

	model_1fj_nomwonj = keras.models.load_model("input/chosen/1fj_nomwonj/NNnet_kFold3.h5")
	model_0fj_nomwonj = keras.models.load_model("input/chosen/0fj_nomwonj/NNnet_kFold2.h5")

	NNout([ifile,ofile],[model_1fj_nom,model_1fj_mix,model_1fj_nomwonj], [model_0fj_nom,model_0fj_mix,model_0fj_nomwonj], treename)

if __name__ == "__main__":
    main()
