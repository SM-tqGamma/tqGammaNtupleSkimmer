import os,sys


def main():
    # variations = [
    #     "CategoryReduction_JET_BJES_Response__1down",
    #     "CategoryReduction_JET_BJES_Response__1up",
    #     "CategoryReduction_JET_EffectiveNP_Detector1__1down",
    #     "CategoryReduction_JET_EffectiveNP_Detector1__1up",
    #     "CategoryReduction_JET_EffectiveNP_Detector2__1down",
    #     "CategoryReduction_JET_EffectiveNP_Detector2__1up",
    #     "CategoryReduction_JET_EffectiveNP_Mixed1__1down",
    #     "CategoryReduction_JET_EffectiveNP_Mixed1__1up",
    #     "CategoryReduction_JET_EffectiveNP_Mixed2__1down",
    #     "CategoryReduction_JET_EffectiveNP_Mixed2__1up",
    #     "CategoryReduction_JET_EffectiveNP_Mixed3__1down",
    #     "CategoryReduction_JET_EffectiveNP_Mixed3__1up",
    #     "CategoryReduction_JET_EffectiveNP_Modelling1__1down",
    #     "CategoryReduction_JET_EffectiveNP_Modelling1__1up",
    #     "CategoryReduction_JET_EffectiveNP_Modelling2__1down",
    #     "CategoryReduction_JET_EffectiveNP_Modelling2__1up",
    #     "CategoryReduction_JET_EffectiveNP_Modelling3__1down",
    #     "CategoryReduction_JET_EffectiveNP_Modelling3__1up",
    #     "CategoryReduction_JET_EffectiveNP_Modelling4__1down",
    #     "CategoryReduction_JET_EffectiveNP_Modelling4__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical1__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical1__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical2__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical2__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical3__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical3__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical4__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical4__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical5__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical5__1up",
    #     "CategoryReduction_JET_EffectiveNP_Statistical6__1down",
    #     "CategoryReduction_JET_EffectiveNP_Statistical6__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
    #     "CategoryReduction_JET_EtaIntercalibration_TotalStat__1down",
    #     "CategoryReduction_JET_EtaIntercalibration_TotalStat__1up",
    #     #"CategoryReduction_JET_Flavor_Composition__1down",
    #     #"CategoryReduction_JET_Flavor_Composition__1up",
    #     "CategoryReduction_JET_Flavor_Response__1down",
    #     "CategoryReduction_JET_Flavor_Response__1up",
    #     "CategoryReduction_JET_JER_DataVsMC_MC16__1down",
    #     "CategoryReduction_JET_JER_DataVsMC_MC16__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_1__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_1__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_2__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_2__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_3__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_3__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_4__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_4__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_5__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_5__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_6__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_6__1up",
    #     "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",
    #     "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",
    #     "CategoryReduction_JET_Pileup_OffsetMu__1down",
    #     "CategoryReduction_JET_Pileup_OffsetMu__1up",
    #     "CategoryReduction_JET_Pileup_OffsetNPV__1down",
    #     "CategoryReduction_JET_Pileup_OffsetNPV__1up",
    #     "CategoryReduction_JET_Pileup_PtTerm__1down",
    #     "CategoryReduction_JET_Pileup_PtTerm__1up",
    #     "CategoryReduction_JET_Pileup_RhoTopology__1down",
    #     "CategoryReduction_JET_Pileup_RhoTopology__1up",
    #     "CategoryReduction_JET_PunchThrough_MC16__1down",
    #     "CategoryReduction_JET_PunchThrough_MC16__1up",
    #     "CategoryReduction_JET_SingleParticle_HighPt__1down",
    #     "CategoryReduction_JET_SingleParticle_HighPt__1up",
    #     "EG_RESOLUTION_ALL__1down",
    #     "EG_RESOLUTION_ALL__1up",
    #     "EG_SCALE_AF2__1down",
    #     "EG_SCALE_AF2__1up",
    #     "EG_SCALE_ALL__1down",
    #     "EG_SCALE_ALL__1up",
    #     "MET_SoftTrk_ResoPara",
    #     "MET_SoftTrk_ResoPerp",
    #     "MET_SoftTrk_ScaleDown",
    #     "MET_SoftTrk_ScaleUp",
    #     "MUON_ID__1down",
    #     "MUON_ID__1up",
    #     "MUON_MS__1down",
    #     "MUON_MS__1up",
    #     "MUON_SAGITTA_RESBIAS__1down",
    #     "MUON_SAGITTA_RESBIAS__1up",
    #     "MUON_SAGITTA_RHO__1down",
    #     "MUON_SAGITTA_RHO__1up",
    #     "MUON_SCALE__1down",
    #     "MUON_SCALE__1up",
    #     "nominal",
    #     "CategoryReduction_JET_JER_DataVsMC_AFII__1down",
    #     "CategoryReduction_JET_JER_DataVsMC_AFII__1up",
    #     "CategoryReduction_JET_PunchThrough_AFII__1down",
    #     "CategoryReduction_JET_PunchThrough_AFII__1up",
    #     "CategoryReduction_JET_RelativeNonClosure_AFII__1down",
    #     "CategoryReduction_JET_RelativeNonClosure_AFII__1up",]

    variations = [
        ["CategoryReduction_JET_BJES_Response__1down", "b-jet energy scale response" ],
        ["CategoryReduction_JET_BJES_Response__1up", "b-jet energy scale response" ],
        ["CategoryReduction_JET_EffectiveNP_Detector1__1down", "CatRed eff. NP det1"],
        ["CategoryReduction_JET_EffectiveNP_Detector1__1up","CatRed eff. NP det1"],
        ["CategoryReduction_JET_EffectiveNP_Detector2__1down","CatRed eff. NP det2"],
        ["CategoryReduction_JET_EffectiveNP_Detector2__1up","CatRed eff. NP det2"],
        ["CategoryReduction_JET_EffectiveNP_Mixed1__1down","CatRed eff. NP mix1"],
        ["CategoryReduction_JET_EffectiveNP_Mixed1__1up","CatRed eff. NP mix1"],
        ["CategoryReduction_JET_EffectiveNP_Mixed2__1down","CatRed eff. NP mix2"],
        ["CategoryReduction_JET_EffectiveNP_Mixed2__1up","CatRed eff. NP mix2"],
        ["CategoryReduction_JET_EffectiveNP_Mixed3__1down","CatRed eff. NP mix3"],
        ["CategoryReduction_JET_EffectiveNP_Mixed3__1up","CatRed eff. NP mix3"],
        ["CategoryReduction_JET_EffectiveNP_Modelling1__1down","CatRed eff. NP model1"],
        ["CategoryReduction_JET_EffectiveNP_Modelling1__1up","CatRed eff. NP model1"],
        ["CategoryReduction_JET_EffectiveNP_Modelling2__1down","CatRed eff. NP model2"],
        ["CategoryReduction_JET_EffectiveNP_Modelling2__1up","CatRed eff. NP model2"],
        ["CategoryReduction_JET_EffectiveNP_Modelling3__1down", "CatRed eff. NP model3"],
        ["CategoryReduction_JET_EffectiveNP_Modelling3__1up","CatRed eff. NP model3"],
        ["CategoryReduction_JET_EffectiveNP_Modelling4__1down","CatRed eff. NP model4"],
        ["CategoryReduction_JET_EffectiveNP_Modelling4__1up","CatRed eff. NP model4"],
        ["CategoryReduction_JET_EffectiveNP_Statistical1__1down","CatRed eff. NP stat1"],
        ["CategoryReduction_JET_EffectiveNP_Statistical1__1up","CatRed eff. NP stat1"],
        ["CategoryReduction_JET_EffectiveNP_Statistical2__1down","CatRed eff. NP stat2"],
        ["CategoryReduction_JET_EffectiveNP_Statistical2__1up","CatRed eff. NP stat2"],
        ["CategoryReduction_JET_EffectiveNP_Statistical3__1down","CatRed eff. NP stat3"],
        ["CategoryReduction_JET_EffectiveNP_Statistical3__1up","CatRed eff. NP stat3"],
        ["CategoryReduction_JET_EffectiveNP_Statistical4__1down","CatRed eff. NP stat4"],
        ["CategoryReduction_JET_EffectiveNP_Statistical4__1up","CatRed eff. NP stat4"],
        ["CategoryReduction_JET_EffectiveNP_Statistical5__1down","CatRed eff. NP stat5"],
        ["CategoryReduction_JET_EffectiveNP_Statistical5__1up","CatRed eff. NP stat5"],
        ["CategoryReduction_JET_EffectiveNP_Statistical6__1down","CatRed eff. NP stat6"],
        ["CategoryReduction_JET_EffectiveNP_Statistical6__1up","CatRed eff. NP stat6"],
        ["CategoryReduction_JET_EtaIntercalibration_Modelling__1down","CatRed eta intercal. modelling"],
        ["CategoryReduction_JET_EtaIntercalibration_Modelling__1up","CatRed eta intercal. modelling"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down","CatRed eta intercal. non-clos. 2018 data"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up","CatRed eta intercal. non-clos. 2018 data"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down","CatRed eta intercal. non-clos. high-E"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up","CatRed eta intercal. non-clos. high-E"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down","CatRed eta intercal. non-clos. neg-eta"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up","CatRed eta intercal. non-clos. neg-eta"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down","CatRed eta intercal. non-clos. pos-eta"],
        ["CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up","CatRed eta intercal. non-clos. pos-eta"],
        ["CategoryReduction_JET_EtaIntercalibration_TotalStat__1down", "CatRed eta intercal. tot. stat."],
        ["CategoryReduction_JET_EtaIntercalibration_TotalStat__1up","CatRed eta intercal. tot. stat."],
        #"CategoryReduction_JET_Flavor_Composition__1down",
        #"CategoryReduction_JET_Flavor_Composition__1up",
        ["CategoryReduction_JET_Flavor_Response__1down", "CatRed flavor response"],
        ["CategoryReduction_JET_Flavor_Response__1up","CatRed flavor response"],
        ["CategoryReduction_JET_JER_DataVsMC_MC16__1down","CatRed JER data vs. MC"],
        ["CategoryReduction_JET_JER_DataVsMC_MC16__1up","CatRed JER data vs. MC"],
        ["CategoryReduction_JET_JER_EffectiveNP_1__1down","CatRed JER eff. NP1"],
        ["CategoryReduction_JET_JER_EffectiveNP_1__1up","CatRed JER eff. NP1"],
        ["CategoryReduction_JET_JER_EffectiveNP_2__1down","CatRed JER eff. NP2"],
        ["CategoryReduction_JET_JER_EffectiveNP_2__1up","CatRed JER eff. NP2"],
        ["CategoryReduction_JET_JER_EffectiveNP_3__1down","CatRed JER eff. NP3"],
        ["CategoryReduction_JET_JER_EffectiveNP_3__1up","CatRed JER eff. NP3"],
        ["CategoryReduction_JET_JER_EffectiveNP_4__1down","CatRed JER eff. NP4"],
        ["CategoryReduction_JET_JER_EffectiveNP_4__1up","CatRed JER eff. NP4"],
        ["CategoryReduction_JET_JER_EffectiveNP_5__1down","CatRed JER eff. NP5"],
        ["CategoryReduction_JET_JER_EffectiveNP_5__1up","CatRed JER eff. NP5"],
        ["CategoryReduction_JET_JER_EffectiveNP_6__1down","CatRed JER eff. NP6"],
        ["CategoryReduction_JET_JER_EffectiveNP_6__1up","CatRed JER eff. NP6"],
        ["CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down","CatRed JER eff. NP rest"],
        ["CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up","CatRed JER eff. NP rest"],
        ["CategoryReduction_JET_Pileup_OffsetMu__1down","CatRed pile-up offset mu"],
        ["CategoryReduction_JET_Pileup_OffsetMu__1up","CatRed pile-up offset mu"],
        ["CategoryReduction_JET_Pileup_OffsetNPV__1down","CatRed pile-up offset NPV"],
        ["CategoryReduction_JET_Pileup_OffsetNPV__1up","CatRed pile-up offset NPV"],
        ["CategoryReduction_JET_Pileup_PtTerm__1down","CatRed pile-up pT term"],
        ["CategoryReduction_JET_Pileup_PtTerm__1up","CatRed pile-up pT term"],
        ["CategoryReduction_JET_Pileup_RhoTopology__1down",'"CatRed pile-up #rho topology"'],
        ["CategoryReduction_JET_Pileup_RhoTopology__1up",'"CatRed pile-up #rho topology"'],
        ["CategoryReduction_JET_PunchThrough_MC16__1down","CatRed punch-through"],
        ["CategoryReduction_JET_PunchThrough_MC16__1up","CatRed punch-through"],
        ["CategoryReduction_JET_SingleParticle_HighPt__1down","CatRed single-part. high pT"],
        ["CategoryReduction_JET_SingleParticle_HighPt__1up","CatRed single-part. high pT"],
        ["EG_RESOLUTION_ALL__1down",'"e/#gamma resolution"'],
        ["EG_RESOLUTION_ALL__1up",'"e/#gamma resolution"'],
        ["EG_SCALE_AF2__1down",'"e/#gamma scale AFII"'],
        ["EG_SCALE_AF2__1up",'"e/#gamma scale AFII"'],
        ["EG_SCALE_ALL__1down",'"e/#gamma scale"'],
        ["EG_SCALE_ALL__1up",'"e/#gamma scale"'],
        ["MET_SoftTrk_ResoPara","MET soft-track reso. par."],
        ["MET_SoftTrk_ResoPerp","MET soft-track reso. perp."],
        ["MET_SoftTrk_ScaleDown","MET soft-track scale"],
        ["MET_SoftTrk_ScaleUp","MET soft-track scale"],
        ["MUON_ID__1down","Muon ID"],
        ["MUON_ID__1up","Muon ID"],
        ["MUON_MS__1down","Muon MS"],
        ["MUON_MS__1up","Muon MS"],
        ["MUON_SAGITTA_RESBIAS__1down","Muon sagitta res. bias"],
        ["MUON_SAGITTA_RESBIAS__1up","Muon sagitta res. bias"],
        ["MUON_SAGITTA_RHO__1down","Muon sagitta rho"],
        ["MUON_SAGITTA_RHO__1up","Muon sagitta rho"],
        ["MUON_SCALE__1down","Muon scale"],
        ["MUON_SCALE__1up","Muon scale"],
        ["nominal","nominal"],
        ["CategoryReduction_JET_JER_DataVsMC_AFII__1down","CatRed JER data vs. MC AFII"],
        ["CategoryReduction_JET_JER_DataVsMC_AFII__1up","CatRed JER data vs. MC AFII"],
        ["CategoryReduction_JET_PunchThrough_AFII__1down","CatRed punch-through AFII"],
        ["CategoryReduction_JET_PunchThrough_AFII__1up","CatRed punch-through AFII"],
        ["CategoryReduction_JET_RelativeNonClosure_AFII__1down","CatRed rel. non-closure AFII"],
        ["CategoryReduction_JET_RelativeNonClosure_AFII__1up","CatRed rel. non-closure AFII"],]

    SFvariations = [
        ["leptonSF_EL_SF_Trigger_UP", "electron trigger SF"],
        ["leptonSF_EL_SF_Reco_UP","electron reconstruction SF"],
        ["leptonSF_EL_SF_ID_UP","electron identification SF"],
        ["leptonSF_EL_SF_Isol_UP","Electron isol. SF"],
        ["leptonSF_MU_SF_Trigger_STAT_UP", "muon trigger stat. SF"],
        ["leptonSF_MU_SF_Trigger_SYST_UP","muon trigger syst. SF"],
        ["leptonSF_MU_SF_ID_STAT_UP","muon identification stat. SF"],
        ["leptonSF_MU_SF_ID_SYST_UP","muon identification syst. SF"],
        ["leptonSF_MU_SF_ID_STAT_LOWPT_UP","muon identification stat. low pT SF"],
        ["leptonSF_MU_SF_ID_SYST_LOWPT_UP","muon identification syst. low pT SF"],
        ["leptonSF_MU_SF_Isol_STAT_UP", "muon isolation stat. SF"],
        ["leptonSF_MU_SF_Isol_SYST_UP","muon isolation syst. SF"],
        ["leptonSF_MU_SF_TTVA_STAT_UP", "muon TTVA stat. SF"],
        ["leptonSF_MU_SF_TTVA_SYST_UP", "muon TTVA syst. SF"],
        ["photonSFown_ID_up", "photon identification SF"],
        ["photonSFown_ISO_up","photon efficiency isolation SF"],
        ["hfakeweight_UP", "Hfakes SF unc."],
        ["efakeweight_UP", "Efakes SF unc."],
        ["jvt_UP", "jet vertex tagger SF"],
        ["pileup_UP", "pile-up SF"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_1_UP", "b-tag eigenvar. B00"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_2_UP", "b-tag eigenvar. B01"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_3_UP","b-tag eigenvar. B02"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_4_UP","b-tag eigenvar. B03"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_5_UP","b-tag eigenvar. B04"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_6_UP","b-tag eigenvar. B05"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_7_UP","b-tag eigenvar. B06"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_8_UP","b-tag eigenvar. B07"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_9_UP","b-tag eigenvar. B08"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_10_UP","b-tag eigenvar. B09"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_11_UP","b-tag eigenvar. B10"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_12_UP","b-tag eigenvar. B11"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_13_UP","b-tag eigenvar. B12"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_14_UP","b-tag eigenvar. B13"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_15_UP","b-tag eigenvar. B14"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_16_UP","b-tag eigenvar. B15"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_17_UP","b-tag eigenvar. B16"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_18_UP","b-tag eigenvar. B17"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_19_UP","b-tag eigenvar. B18"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_20_UP","b-tag eigenvar. B19"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_21_UP","b-tag eigenvar. B20"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_22_UP","b-tag eigenvar. B21"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_23_UP","b-tag eigenvar. B22"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_24_UP","b-tag eigenvar. B23"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_25_UP","b-tag eigenvar. B24"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_26_UP","b-tag eigenvar. B25"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_27_UP","b-tag eigenvar. B26"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_28_UP","b-tag eigenvar. B27"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_29_UP","b-tag eigenvar. B28"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_30_UP","b-tag eigenvar. B29"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_31_UP","b-tag eigenvar. B30"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_32_UP","b-tag eigenvar. B31"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_33_UP","b-tag eigenvar. B32"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_34_UP","b-tag eigenvar. B33"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_35_UP","b-tag eigenvar. B34"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_36_UP","b-tag eigenvar. B35"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_37_UP","b-tag eigenvar. B36"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_38_UP","b-tag eigenvar. B37"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_39_UP","b-tag eigenvar. B38"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_40_UP","b-tag eigenvar. B39"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_41_UP","b-tag eigenvar. B40"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_42_UP","b-tag eigenvar. B41"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_43_UP","b-tag eigenvar. B42"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_44_UP","b-tag eigenvar. B43"],
        ["bTagSF_DL1r_Continuous_eigenvars_B_45_UP","b-tag eigenvar. B44"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_1_UP","b-tag eigenvar. C00"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_2_UP", "b-tag eigenvar. C01"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_3_UP","b-tag eigenvar. C02"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_4_UP","b-tag eigenvar. C03"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_5_UP","b-tag eigenvar. C04"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_6_UP","b-tag eigenvar. C05"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_7_UP","b-tag eigenvar. C06"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_8_UP","b-tag eigenvar. C07"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_9_UP","b-tag eigenvar. C08"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_10_UP","b-tag eigenvar. C09"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_11_UP","b-tag eigenvar. C10"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_12_UP","b-tag eigenvar. C11"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_13_UP","b-tag eigenvar. C12"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_14_UP","b-tag eigenvar. C13"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_15_UP","b-tag eigenvar. C14"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_16_UP","b-tag eigenvar. C15"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_17_UP","b-tag eigenvar. C16"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_18_UP","b-tag eigenvar. C17"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_19_UP","b-tag eigenvar. C18"],
        ["bTagSF_DL1r_Continuous_eigenvars_C_20_UP","b-tag eigenvar. C19"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_1_UP","b-tag eigenvar. L00"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_2_UP","b-tag eigenvar. L01"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_3_UP","b-tag eigenvar. L02"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_4_UP","b-tag eigenvar. L03"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_5_UP","b-tag eigenvar. L04"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_6_UP","b-tag eigenvar. L05"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_7_UP","b-tag eigenvar. L06"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_8_UP","b-tag eigenvar. L07"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_9_UP","b-tag eigenvar. L08"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_10_UP","b-tag eigenvar. L09"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_11_UP","b-tag eigenvar. L10"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_12_UP","b-tag eigenvar. L11"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_13_UP","b-tag eigenvar. L12"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_14_UP","b-tag eigenvar. L13"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_15_UP","b-tag eigenvar. L14"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_16_UP","b-tag eigenvar. L15"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_17_UP","b-tag eigenvar. L16"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_18_UP","b-tag eigenvar. L17"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_19_UP","b-tag eigenvar. L18"],
        ["bTagSF_DL1r_Continuous_eigenvars_Light_20_UP","b-tag eigenvar. L19"],
    ]


    origfile = open("tqGamma_crosscheck.config", "r")
    config =  open("tqGamma_fullcrosscheck.config", "w")
    for line in origfile:
        config.write(line)
    config.write("\n")
    # tqy PDF variations
    # for pdfvar in range(1,101):
    #     config.write(f"Systematic: ty_PDF_var{pdfvar} \n")
    #     config.write(f"  CombineName: ty_PDF \n")
    #     config.write(f"  CombineType: STANDARDDEVIATIONNODDOF \n")
    #     config.write(f"  Symmetrisation: ONESIDED \n")
    #     config.write(f"  Samples: ty \n")
    #     config.write(f"  HistoFilesUp: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights \n")
    #     config.write(f"  HistoNameSufUp: _PDF={260400+pdfvar}NNPDF30_nlo_as_0118_nf_4 \n \n")
    #
    # for pdfvar in range(1,101):
    #     config.write(f"Systematic: tty_PDF_var{pdfvar} \n")
    #     config.write(f"  CombineName: tty_PDF \n")
    #     config.write(f"  CombineType: STANDARDDEVIATIONNODDOF \n")
    #     config.write(f"  Symmetrisation: ONESIDED \n")
    #     config.write(f"  Samples: tty \n")
    #     config.write(f"  HistoFilesUp: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights \n")
    #     config.write(f"  HistoNameSufUp: _Member{pdfvar} \n \n")
    #
    # for pdfvar in range(2,101):
    #     config.write(f"Systematic: tt_PDF_var{pdfvar} \n")
    #     config.write(f"  CombineName: tt_PDF \n")
    #     config.write(f"  CombineType: STANDARDDEVIATIONNODDOF \n")
    #     config.write(f"  Symmetrisation: ONESIDED \n")
    #     config.write(f"  Samples: ttbar \n")
    #     config.write(f"  HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights \n")
    #     config.write(f"  HistoNameSufUp: _PDFset={260000+pdfvar} \n \n")

    for objVar in variations:
        if objVar[0] in ["nominal"] or "__1down" in objVar[0] or "Down" in objVar[0]: continue
        if "AFII" in objVar[0]: continue
        config.write(f"Systematic: {objVar[0].split('__')[0]} \n")
        config.write(f"  Title: {objVar[1]} \n")
        #config.write(f"  HistoFileSufUp: _{objVar} \n")
        config.write(f"  HistoNameSufUp: _{objVar[0]} \n")
        if "JET_JER" in objVar[0] or "MET_SoftTrk_Reso" in objVar[0]:
            config.write(f"  Symmetrisation: ONESIDED \n")
            #config.write(f"  HistoNameSufDown: _{objVar[0].replace('UP', 'DOWN').replace('Up','Down').replace('1up', '1down')} \n")
            #config.write(f"  Symmetrisation: TWOSIDED \n")
        else:
            #config.write(f"  HistoFileSufDown: _{objVar.replace('UP', 'DOWN').replace('Up','Down').replace('1up', '1down')} \n")
            config.write(f"  HistoNameSufDown: _{objVar[0].replace('UP', 'DOWN').replace('Up','Down').replace('1up', '1down')} \n")
            config.write(f"  Symmetrisation: TWOSIDED \n")
        #config.write(f"  Samples: Signal, WJetsGamma, ZJetsGamma, Diboson, ttbar, ttGamma, SingleTop, tq_dec,tq_fake, WJets, ZJets \n \n")
        if not objVar[0].split("__")[0] in ["CategoryReduction_JET_PunchThrough_MC16", "CategoryReduction_JET_JER_DataVsMC_MC16", "CategoryReduction_JET_JER_DataVsMC_AFII", "CategoryReduction_JET_PunchThrough_AFII", "CategoryReduction_JET_RelativeNonClosure_AFII"]:
            config.write(f"  Exclude: FakeLeptons \n \n")
        elif "AFII" in objVar[0]: config.write(f"  Samples: ty \n \n")
        else: config.write(f"  Exclude: FakeLeptons,ty \n \n")


    for SFVar in SFvariations:
        #if Var in ["nominal"] or "__1down" in objVar: continue
        config.write(f"Systematic: {SFVar[0].replace('_UP', '').replace('_up', '')} \n")
        config.write(f"  Title: {SFVar[1]} \n")
        #config.write(f"  HistoFileSufUp: _{objVar} \n")
        if not "bTagSF" in SFVar[0]: config.write(f"  HistoFileSufUp: _ObjSys \n")
        if not "efake" in SFVar[0] and not "hfake" in SFVar[0]:
            config.write(f"  HistoNameSufUp: _weight_{SFVar[0]} \n")
            config.write(f"  HistoNameSufDown: _weight_{SFVar[0].replace('_UP', '_DOWN').replace('_up', '_down')} \n")
        else:
            config.write(f"  HistoNameSufUp: _{SFVar[0]} \n")
            config.write(f"  HistoNameSufDown: _{SFVar[0].replace('_UP', '_DOWN').replace('_up', '_down')} \n")
        if not "bTagSF" in SFVar[0]: config.write(f"  HistoFileSufDown: _ObjSys \n")
        config.write(f"  Symmetrisation: TWOSIDED \n")
        config.write(f"  Exclude: FakeLeptons \n \n")
        #config.write(f"  Samples: Signal, WJetsGamma, ZJetsGamma, Diboson, ttbar, ttGamma, SingleTop, tq_dec ,tq_fake, WJets, ZJets \n \n")
        #config.write(f"  Samples: Signal \n \n")

    config.close()

if __name__ == "__main__":
    main()
