Job: "tqGamma_pdf"
  Label: "#it{t}#it{q}#gamma"
  CmeLabel: "13 TeV"
  LumiLabel: "139 fb^{-1}"
  AtlasLabel: "Internal"
  POI: "SigXsecOverSM"
  ReadFrom: HIST
  HistoPath: "/ceph/groups/e4/users/bwendland/tqgammaHIST_Aug21/"
  DebugLevel: 1
  SystControlPlots: TRUE
  StatOnly: False
  ImageFormat: pdf
  SystPruningShape: 0.01
  SystPruningNorm: 0.01
  SuppressNegativeBinWarnings: True
  CorrelationThreshold: 0.02
  BlindingThreshold: 0.1
  RankingMaxNP: 20
  KeepPruning: TRUE


Fit: "myFit"
  FitType: SPLUSB
  FitRegion: CRSR
  FitBlind: TRUE
  POIAsimov: 1

Significance: "Significance"
  SignificanceBlind: TRUE
  POIAsimov: 1

Region: "SR_1fj"
  Type: SIGNAL
  VariableTitle: "NN_{out}"
  Label: "SR >1fj"
  ShortLabel: "SR >1fj"
  #Rebinning: 0, 0.113, 0.210, 0.306, 0.402, 0.494, 0.585, 0.672, 0.767, 0.867, 1
  Rebin: 20
  Ymin: 10E-1
  LogScale: False
  DataType: DATA
  HistoNames: NN_out_nomwonj_fwd_elmu_inc_


Region: "SR_0fj"
  Type: SIGNAL
  VariableTitle: "NN_{out}"
  Label: "SR 0fj"
  ShortLabel: "SR 0fj"
  #Rebinning: 0, 0.113, 0.210, 0.306, 0.402, 0.494, 0.585, 0.672, 0.767, 0.867, 1
  Rebin: 20
  Ymin: 10E-1
  LogScale: False
  DataType: DATA
  HistoNames: NN_out_nomwonj_nofwd_elmu_inc_

Region: "ttyCR"
  Type: SIGNAL
  VariableTitle: "NN_{out}"
  Label: "t#bar{t}#gamma CR"
  ShortLabel: "ttyCR"
  #Rebinning: 0, 0.113, 0.210, 0.306, 0.402, 0.494, 0.585, 0.672, 0.767, 0.867, 1
  Rebin: 20
  Ymin: 10E-1
  LogScale: False
  DataType: DATA
  HistoNames: NN_out_nomwonj_tty_elmu_inc_

Region: "WyCR"
  Type: SIGNAL
  VariableTitle: "NN_{out}"
  Label: "W#gamma CR"
  ShortLabel: "WyCR"
  #Rebinning: 0, 0.113, 0.210, 0.306, 0.402, 0.494, 0.585, 0.672, 0.767, 0.867, 1
  Rebin: 100
  Ymin: 10E-1
  LogScale: False
  DataType: DATA
  HistoNames: NN_out_nomwonj_Wy_elmu_inc_

Sample: Signal_AFII
  Type: GHOST
  #HistoPathSuff: nominal/
  HistoNameSuff: inc
  HistoFiles: mc16a/412147_af2/412147_nominal,mc16d/412147_af2/412147_nominal,mc16e/412147_af2/412147_nominal


Sample: ttGamma_AFII
  Type: GHOST
  #HistoPathSuff: nominal/
  HistoNameSuff: inc
  HistoFiles: mc16a/410389_af2/410389_nominal,mc16d/410389_af2/410389_nominal,mc16e/410389_af2/410389_nominal

Sample: ttbar_AFII
  Type: GHOST
  #HistoPathSuff: nominal/
  HistoNameSuff: inc
  HistoFiles: mc16a/410470_af2/410470_nominal,mc16d/410470_af2/410470_nominal,mc16e/410470_af2/410470_nominal

Sample: tq_dec_AFII
  Type: GHOST
  HistoNameSuff: prompt
  HistoFiles: mc16a/410658_af2/410658_nominal,mc16d/410658_af2/410658_nominal,mc16e/410658_af2/410658_nominal,mc16a/410659_af2/410659_nominal,mc16d/410659_af2/410659_nominal,mc16e/410659_af2/410659_nominal

Sample: tq_fake_AFII
  Type: GHOST
  HistoNameSuff: prompt
  HistoFiles: mc16a/410658_af2/410658_nominal,mc16d/410658_af2/410658_nominal,mc16e/410658_af2/410658_nominal,mc16a/410659_af2/410659_nominal,mc16d/410659_af2/410659_nominal,mc16e/410659_af2/410659_nominal

Sample: "data"
  Type: DATA
  Title: "data"
  #HistoPathSuffs:
  HistoNameSuff: inc
  HistoFiles: data15/data15,data16/data16,data17/data17,data18/data18

Sample: "Signal"
  Type: SIGNAL
  Title: "#it{t}#it{q}#gamma"
  FillColorRGB: 248,151,31
  LineColor: 1
  NormFactor: "SigXsecOverSM",1,0,10
  #HistoPathSuff: nominal/merged/
  HistoNameSuff: inc
  HistoFiles: mc16a/412147/412147,mc16d/412147/412147,mc16e/412147/412147
  #HistoFileSuff: _nominal

Sample: ttGamma
  Type: BACKGROUND
  Title: "#it{t}#bar{#it{t}}#gamma"
  FillColorRGB: 87,157,66
  LineColor: 1
  HistoNameSuff: inc
  NormFactor: "ttGamma",1,0,10
  HistoFiles: mc16a/410389/410389,mc16d/410389/410389,mc16e/410389/410389
  #HistoFileSuff: _nominal

Sample: tq_dec
  Type: BACKGROUND
  Title: "#it{t}q#gamma (dec)"
  FillColorRGB: 214,210,196
  LineColor: 1
  HistoNameSuff: prompt
  HistoFiles: mc16a/410658/410658,mc16d/410658/410658,mc16e/410658/410658,mc16a/410659/410659,mc16d/410659/410659,mc16e/410659/410659

Sample: tq_fake
  Type: BACKGROUND
  Title: "#it{t}q (fake)"
  FillColorRGB: 214,180,196
  LineColor: 1
  HistoNameSuffs: efake,hfake
  HistoFiles: mc16a/410658/410658,mc16d/410658/410658,mc16e/410658/410658,mc16a/410659/410659,mc16d/410659/410659,mc16e/410659/410659


Sample: ttbar
  Type: BACKGROUND
  Title: "#it{t}#bar{#it{t}}"
  FillColorRGB: 255,0,0
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/410470/410470,mc16d/410470/410470,mc16e/410470/410470

Sample: Diboson
  Type: BACKGROUND
  Title: "#it{V}#it{V}"
  FillColorRGB: 238,31,96
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/364250/364250, mc16d/364250/364250, mc16e/364250/364250, mc16a/364253/364253, mc16d/364253/364253, mc16e/364253/364253, mc16a/364254/364254, mc16d/364254/364254, mc16e/364254/364254, mc16a/364255/364255, mc16d/364255/364255, mc16e/364255/364255, mc16a/363355/363355, mc16d/363355/363355, mc16e/363355/363355, mc16a/363356/363356, mc16d/363356/363356, mc16e/363356/363356, mc16a/363357/363357, mc16d/363357/363357, mc16e/363357/363357, mc16a/363358/363358, mc16d/363358/363358, mc16e/363358/363358, mc16a/363359/363359, mc16d/363359/363359, mc16e/363359/363359, mc16a/363360/363360, mc16d/363360/363360, mc16e/363360/363360, mc16a/363489/363489, mc16d/363489/363489, mc16e/363489/363489

Sample: ZJets
  Type: BACKGROUND
  Title: "#it{Z}+jets"
  FillColorRGB: 204,102,255
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/364114/364114, mc16d/364114/364114, mc16e/364114/364114, mc16a/364115/364115, mc16d/364115/364115, mc16e/364115/364115, mc16a/364116/364116, mc16d/364116/364116, mc16e/364116/364116, mc16a/364117/364117, mc16d/364117/364117, mc16e/364117/364117, mc16a/364118/364118, mc16d/364118/364118, mc16e/364118/364118, mc16a/364119/364119, mc16d/364119/364119, mc16e/364119/364119, mc16a/364120/364120, mc16d/364120/364120, mc16e/364120/364120, mc16a/364121/364121, mc16d/364121/364121, mc16e/364121/364121, mc16a/364122/364122, mc16d/364122/364122, mc16e/364122/364122, mc16a/364123/364123, mc16d/364123/364123, mc16e/364123/364123, mc16a/364124/364124, mc16d/364124/364124, mc16e/364124/364124, mc16a/364125/364125, mc16d/364125/364125, mc16e/364125/364125, mc16a/364126/364126, mc16d/364126/364126, mc16e/364126/364126, mc16a/364127/364127, mc16d/364127/364127, mc16e/364127/364127, mc16a/364100/364100, mc16d/364100/364100, mc16e/364100/364100, mc16a/364101/364101, mc16d/364101/364101, mc16e/364101/364101, mc16a/364102/364102, mc16d/364102/364102, mc16e/364102/364102, mc16a/364103/364103, mc16d/364103/364103, mc16e/364103/364103, mc16a/364104/364104, mc16d/364104/364104, mc16e/364104/364104, mc16a/364105/364105, mc16d/364105/364105, mc16e/364105/364105, mc16a/364106/364106, mc16d/364106/364106, mc16e/364106/364106, mc16a/364107/364107, mc16d/364107/364107, mc16e/364107/364107, mc16a/364108/364108, mc16d/364108/364108, mc16e/364108/364108, mc16a/364109/364109, mc16d/364109/364109, mc16e/364109/364109, mc16a/364110/364110, mc16d/364110/364110, mc16e/364110/364110, mc16a/364111/364111, mc16d/364111/364111, mc16e/364111/364111, mc16a/364112/364112, mc16d/364112/364112, mc16e/364112/364112, mc16a/364113/364113, mc16d/364113/364113, mc16e/364113/364113, mc16a/364128/364128, mc16d/364128/364128, mc16e/364128/364128, mc16a/364129/364129, mc16d/364129/364129, mc16e/364129/364129, mc16a/364130/364130, mc16d/364130/364130, mc16e/364130/364130, mc16a/364131/364131, mc16d/364131/364131, mc16e/364131/364131, mc16a/364132/364132, mc16d/364132/364132, mc16e/364132/364132, mc16a/364133/364133, mc16d/364133/364133, mc16e/364133/364133, mc16a/364134/364134, mc16d/364134/364134, mc16e/364134/364134, mc16a/364135/364135, mc16d/364135/364135, mc16e/364135/364135, mc16a/364136/364136, mc16d/364136/364136, mc16e/364136/364136, mc16a/364137/364137, mc16d/364137/364137, mc16e/364137/364137, mc16a/364138/364138, mc16d/364138/364138, mc16e/364138/364138, mc16a/364139/364139, mc16d/364139/364139, mc16e/364139/364139, mc16a/364140/364140, mc16d/364140/364140, mc16e/364140/364140, mc16a/364141/364141, mc16d/364141/364141, mc16e/364141/364141

Sample: ZJetsGamma
  Type: BACKGROUND
  Title: "#it{Z}#gamma+jets"
  FillColorRGB: 0,169,183
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/366140/366140, mc16d/366140/366140, mc16e/366140/366140, mc16a/366141/366141, mc16d/366141/366141, mc16e/366141/366141, mc16a/366142/366142, mc16d/366142/366142, mc16e/366142/366142, mc16a/366143/366143, mc16d/366143/366143, mc16e/366143/366143, mc16a/366144/366144, mc16d/366144/366144, mc16e/366144/366144, mc16a/366145/366145, mc16d/366145/366145, mc16e/366145/366145, mc16a/366146/366146, mc16d/366146/366146, mc16e/366146/366146, mc16a/366147/366147, mc16d/366147/366147, mc16e/366147/366147, mc16a/366148/366148, mc16d/366148/366148, mc16e/366148/366148, mc16a/366149/366149, mc16d/366149/366149, mc16e/366149/366149, mc16a/366150/366150, mc16d/366150/366150, mc16e/366150/366150, mc16a/366151/366151, mc16d/366151/366151, mc16e/366151/366151, mc16a/366152/366152, mc16d/366152/366152, mc16e/366152/366152, mc16a/366153/366153, mc16d/366153/366153, mc16e/366153/366153, mc16a/366154/366154, mc16d/366154/366154, mc16e/366154/366154,

Sample: SingleTop
  Type: BACKGROUND
  Title: "#it{t}"
  FillColorRGB: 156,173,183
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/410644/410644, mc16d/410644/410644, mc16e/410644/410644, mc16a/410645/410645, mc16d/410645/410645, mc16e/410645/410645,mc16a/410646/410646, mc16d/410646/410646, mc16e/410646/410646, mc16a/410647/410647, mc16d/410647/410647, mc16e/410647/410647

Sample: WJetsGamma
  Type: BACKGROUND
  Title: "#it{W}#gamma+jets"
  FillColorRGB: 255,214,0
  NormFactor: "WJetsGamma",1,0,10
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/364522/364522, mc16d/364522/364522, mc16e/364522/364522, mc16a/364523/364523, mc16d/364523/364523, mc16e/364523/364523, mc16a/364524/364524, mc16d/364524/364524, mc16e/364524/364524, mc16a/364525/364525, mc16d/364525/364525, mc16e/364525/364525, mc16a/364527/364527, mc16d/364527/364527, mc16e/364527/364527, mc16a/364528/364528, mc16d/364528/364528, mc16e/364528/364528, mc16a/364529/364529, mc16d/364529/364529, mc16e/364529/364529, mc16a/364530/364530, mc16d/364530/364530, mc16e/364530/364530, mc16a/364532/364532, mc16d/364532/364532, mc16e/364532/364532, mc16a/364533/364533, mc16d/364533/364533, mc16e/364533/364533, mc16a/364534/364534, mc16d/364534/364534, mc16e/364534/364534, mc16a/364535/364535, mc16d/364535/364535, mc16e/364535/364535

Sample: WJets
  Type: BACKGROUND
  Title: "#it{W}+jets"
  FillColorRGB: 0,95,134
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: mc16a/364170/364170, mc16d/364170/364170, mc16e/364170/364170, mc16a/364171/364171, mc16d/364171/364171, mc16e/364171/364171, mc16a/364172/364172, mc16d/364172/364172, mc16e/364172/364172, mc16a/364173/364173, mc16d/364173/364173, mc16e/364173/364173, mc16a/364174/364174, mc16d/364174/364174, mc16e/364174/364174, mc16a/364175/364175, mc16d/364175/364175, mc16e/364175/364175, mc16a/364176/364176, mc16d/364176/364176, mc16e/364176/364176, mc16a/364177/364177, mc16d/364177/364177, mc16e/364177/364177, mc16a/364178/364178, mc16d/364178/364178, mc16e/364178/364178, mc16a/364179/364179, mc16d/364179/364179, mc16e/364179/364179, mc16a/364180/364180, mc16d/364180/364180, mc16e/364180/364180, mc16a/364181/364181, mc16d/364181/364181, mc16e/364181/364181, mc16a/364182/364182, mc16d/364182/364182, mc16e/364182/364182, mc16a/364183/364183, mc16d/364183/364183, mc16e/364183/364183, mc16a/364156/364156, mc16d/364156/364156, mc16e/364156/364156, mc16a/364157/364157, mc16d/364157/364157, mc16e/364157/364157, mc16a/364158/364158, mc16d/364158/364158, mc16e/364158/364158, mc16a/364159/364159, mc16d/364159/364159, mc16e/364159/364159, mc16a/364160/364160, mc16d/364160/364160, mc16e/364160/364160, mc16a/364161/364161, mc16d/364161/364161, mc16e/364161/364161, mc16a/364162/364162, mc16d/364162/364162, mc16e/364162/364162, mc16a/364163/364163, mc16d/364163/364163, mc16e/364163/364163, mc16a/364164/364164, mc16d/364164/364164, mc16e/364164/364164, mc16a/364165/364165, mc16d/364165/364165, mc16e/364165/364165, mc16a/364166/364166, mc16d/364166/364166, mc16e/364166/364166, mc16a/364167/364167, mc16d/364167/364167, mc16e/364167/364167, mc16a/364168/364168, mc16d/364168/364168, mc16e/364168/364168, mc16a/364169/364169, mc16d/364169/364169, mc16e/364169/364169, mc16a/364184/364184, mc16d/364184/364184, mc16e/364184/364184, mc16a/364185/364185, mc16d/364185/364185, mc16e/364185/364185, mc16a/364186/364186, mc16d/364186/364186, mc16e/364186/364186, mc16a/364187/364187, mc16d/364187/364187, mc16e/364187/364187, mc16a/364188/364188, mc16d/364188/364188, mc16e/364188/364188, mc16a/364189/364189, mc16d/364189/364189, mc16e/364189/364189, mc16a/364190/364190, mc16d/364190/364190, mc16e/364190/364190, mc16a/364191/364191, mc16d/364191/364191, mc16e/364191/364191, mc16a/364192/364192, mc16d/364192/364192, mc16e/364192/364192, mc16a/364193/364193, mc16d/364193/364193, mc16e/364193/364193, mc16a/364194/364194, mc16d/364194/364194, mc16e/364194/364194, mc16a/364195/364195, mc16d/364195/364195, mc16e/364195/364195, mc16a/364196/364196, mc16d/364196/364196, mc16e/364196/364196, mc16a/364197/364197, mc16d/364197/364197, mc16e/364197/364197

Sample: LFake
  Type: BACKGROUND
  Title: "fake lepton"
  FillColorRGB: 3,252,232
  LineColor: 1
  HistoNameSuff: inc
  HistoFiles: lfake15/lfake15,lfake16/lfake16,lfake17/lfake17,lfake18/lfake18

Systematic: ShowerSignal
  Title: tqGamma_shower
  Type: HISTO
  Symmetrisation: ONESIDED
#  HistoPathSufUp: ../
  HistoFilesUp: mc16a/500537_af2/500537_nominal,mc16d/500537_af2/500537_nominal,mc16e/500537_af2/500537_nominal
  Samples: Signal
  Smoothing: 40
  ReferenceSample: Signal_AFII

Systematic: LFakeVar
 Title: LFakeVar
 Type: HISTO
 Symmetrisation: ONESIDED
 HistoNameSufUp: _lfakevar
 Samples: LFake
 Smoothing: 40
 DropNorm: all

Systematic: lumi
 Title: "luminosity"
 Type: OVERALL
 OverallUp: +0.017
 OverallDown: -0.017
 Exclude: LFake

Systematic: LFakeNorm
 Title: "LFakeNorm"
 Type: OVERALL
 OverallUp: +0.50
 OverallDown: -0.50
 Samples: LFake

Systematic: ZGammaJetsNorm
 Title: "ZGammaJetsNorm"
 Type: OVERALL
 OverallUp: +0.30
 OverallDown: -0.30
 Samples: ZJetsGamma

Systematic: ttGamma_shower
 Title: ttGamma_shower
 Type: HISTO
 Symmetrisation: ONESIDED
#  HistoPathSufUp: ../
 HistoFilesUp: mc16a/410395_af2/410395_nominal,mc16d/410395_af2/410395_nominal,mc16e/410395_af2/410395_nominal
 Samples: ttGamma
 Smoothing: 40
 ReferenceSample: ttGamma_AFII

Systematic: ttGamma_var3c
 Title: ttGamma_Var3c
 Type: HISTO
 Symmetrisation: TWOSIDED
#  HistoPathSufUp: ../
 HistoFilesUp: mc16a/410404_af2/410404_nominal,mc16d/410404_af2/410404_nominal,mc16e/410404_af2/410404_nominal
 HistoFilesDown: mc16a/410405_af2/410405_nominal,mc16d/410405_af2/410405_nominal,mc16e/410405_af2/410405_nominal
 Samples: ttGamma
 Smoothing: 40
 ReferenceSample: ttGamma_AFII

Systematic: ttbar_hdamp
 Title: tt_hdamp
 Type: HISTO
 Symmetrisation: ONESIDED
#  HistoPathSufU
 HistoFilesUp: mc16a/410480_af2/410480_nominal,mc16d/410480_af2/410480_nominal,mc16e/410480_af2/410480_nominal,mc16a/410482_af2/410482_nominal,mc16d/410482_af2/410482_nominal,mc16e/410482_af2/410482_nominal
 #HistoFilesDown: mc16a/410405_af2/410405_nominal,mc16d/410405_af2/410405_nominal,mc16e/410405_af2/410405_nominal
 Samples: ttbar
 Smoothing: 40
 ReferenceSample: ttbar_AFII

Systematic: tq_dec_shower
 Title: tq_dec_shower
 Type: HISTO
 Symmetrisation: ONESIDED
 Samples: tq_dec
 HistoFilesUp: mc16a/411032_af2/411032_nominal,mc16d/411032_af2/411032_nominal,mc16e/411032_af2/411032_nominal,mc16a/411033_af2/411033_nominal,mc16d/411033_af2/411033_nominal,mc16e/411033_af2/411033_nominal
 Smoothing: 40
 ReferenceSample: tq_dec_AFII

#Systematic: tq_fake_shower
# Title: tq_fake_shower
# Type: HISTO
# Symmetrisation: ONESIDED
# Samples: tq_fake
# HistoFilesUp: mc16a/411032_af2/411032_nominal,mc16d/411032_af2/411032_nominal,mc16e/411032_af2/411032_nominal,mc16a/411033_af2/411033_nominal,mc16d/411033_af2/411033_nominal,mc16e/411033_af2/411033_nominal
# Smoothing: 40
# ReferenceSample: tq_fake_AFII
#
Systematic: tq_LOvsNLO
 Title: tq_LOvsNLO
 Type: HISTO
 Symmetrisation: ONESIDED
 Samples: tq_dec
 HistoFilesUp: mc16a/412089_af2/412089_nominal,mc16d/412089_af2/412089_nominal,mc16e/412089_af2/412089_nominal
 Smoothing: 40
 ReferenceSample: tq_dec_AFII
 DropNorm: all

Systematic: tqy_muR
 Title: tqy_muR
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: Signal
 HistoFilesUp: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoFilesDown: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoNameSufUp: _dyn=3muR=020000E+01muF=010000E+01
 HistoNameSufDown: _dyn=3muR=050000E+00muF=010000E+01

Systematic: tqy_muF
 Title: tqy_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: Signal
 HistoFilesUp: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoFilesDown: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoNameSufUp: _dyn=3muR=010000E+01muF=020000E+01
 HistoNameSufDown: _dyn=3muR=010000E+01muF=050000E+00

Systematic: tqy_muR_muF
 Title: tqy_muR_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: Signal
 HistoFilesUp: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoFilesDown: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoNameSufUp: _dyn=3muR=020000E+01muF=020000E+01
 HistoNameSufDown: _dyn=3muR=050000E+00muF=050000E+00

Systematic: tty_muR
 Title: tty_muR
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttGamma
 HistoFilesUp: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoFilesDown: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoNameSufUp: _mur=2muf=1
 HistoNameSufDown: _mur=05muf=1

Systematic: tty_muF
 Title: tty_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttGamma
 HistoFilesUp: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoFilesDown: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoNameSufUp: _mur=1muf=2
 HistoNameSufDown: _mur=1muf=05

Systematic: tty_muR_muF
 Title: tty_muR_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttGamma
 HistoFilesUp: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoFilesDown: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoNameSufUp: _mur=2muf=2
 HistoNameSufDown: _mur=05muf=05

Systematic: tt_muR
 Title: tt_muR
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttbar
 HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoFilesDown: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoNameSufUp: _muR=20,muF=10
 HistoNameSufDown: _muR=05,muF=10

Systematic: tt_muF
 Title: tt_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttbar
 HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoFilesDown: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoNameSufUp: _muR=10,muF=20
 HistoNameSufDown: _muR=10,muF=05

Systematic: tt_muR_muF
 Title: tt_muR_muF
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttbar
 HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoFilesDown: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoNameSufUp: _muR=20,muF=20
 HistoNameSufDown: _muR=05,muF=05

Systematic: tt_var3c
 Title: tt_var3c
 Type: HISTO
 #Symmetrisation: TWOSIDED
 Samples: ttbar
 HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoFilesDown: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoNameSufUp: _Var3cUp
 HistoNameSufDown: _Var3cDown

Systematic: tqy_PDF
 Title: "#it{tq#gamma} PDF"
 CombineName: tqy_PDF
 CombineType: STANDARDDEVIATION
 Symmetrisation: ONESIDED
 Samples: Signal
 HistoFilesUp: mc16a/412147/412147_MCweights,mc16d/412147/412147_MCweights,mc16e/412147/412147_MCweights
 HistoNameSufUp: _PDF=260400NNPDF30_nlo_as_0118_nf_4

Systematic: tty_PDF
 Title: "#it{t#bar{t}#gamma} PDF"
 CombineName: tty_PDF
 CombineType: STANDARDDEVIATION
 Symmetrisation: ONESIDED
 Samples: ttGamma
 HistoFilesUp: mc16a/410389/410389_MCweights,mc16d/410389/410389_MCweights,mc16e/410389/410389_MCweights
 HistoNameSufUp: _Member0

Systematic: tt_PDF
 Title: "#it{t#bar{t}} PDF"
 CombineName: tt_PDF
 CombineType: STANDARDDEVIATION
 Symmetrisation: ONESIDED
 Samples: ttbar
 HistoFilesUp: mc16a/410470/410470_MCweights,mc16d/410470/410470_MCweights,mc16e/410470/410470_MCweights
 HistoNameSufUp: _PDFset=260001


