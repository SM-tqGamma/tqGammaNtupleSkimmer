# tqGamma Ntuple Skimmer

## Author: Harish Potti

## Description: 

Ntuple Skimmer for tqGamma analysis.

## Build the software 


Setup latest version of AnalysisBase. 

You can find the latest release here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes21_2

```
lsetup "asetup AnalysisBase,21.2.116,here"
cmake ../source
cmake --build ./
source x86*/setup.sh

```

## Usage:

```
ntuple-skimmer --in /data_ceph/harish/v1_tqgamma/mergedv2/mc16a/412089.root --out output.root --isData 0 --dosys 1 --dofake 0

```